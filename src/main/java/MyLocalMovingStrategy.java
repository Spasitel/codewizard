import java.awt.Color;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;

import model.LivingUnit;
import model.Wizard;

public class MyLocalMovingStrategy {

	public static void moveToWayPoint(MyDataPoint2D point, boolean attackMove, MyLocalMoveType moveType) {
		if (!attackMove && moveType.equals(MyLocalMoveType.WAYPOINT)) {
			MyAttackStrategy.attackTree();
		}

		if (MyStrategy.globalState.equals(MyGlobalState.GO_TO_RUNE)) {
			if (makeAcurateMoveNearRune(point, attackMove, moveType))
				return;
		}
		moveTo(point, attackMove, moveType, MyMagicNumbers.DISTANCE_FOR_WAYPIONT_MOVE);

	}

	private static void moveTo(MyDataPoint2D point, boolean attackMove, MyLocalMoveType moveType, double moveDistance) {
		double coefficient = 1.0;
		moveTo(point, attackMove, moveType, moveDistance, coefficient);
	}

	private static void moveTo(MyDataPoint2D point, boolean attackMove, MyLocalMoveType moveType, double moveDistance,
			double coefficient) {
		double angle = MyStrategy.self.getAngleTo(point.getX(), point.getY());
		tryMoveToAngle(attackMove, angle, moveType, moveDistance, coefficient);
	}

	private static boolean makeAcurateMoveNearRune(MyDataPoint2D point, boolean attackMove, MyLocalMoveType moveType) {
		if (moveType.equals(MyLocalMoveType.DODGE) || moveType.equals(MyLocalMoveType.SAFE))
			return false;
		assert moveType.equals(MyLocalMoveType.WAYPOINT);
		MyGlobalMoveStrategy.RuneType runeType;
		if (!point.equals(MyGlobalMoveStrategy.RuneType.BOT.waypoint)) {
			if (!point.equals(MyGlobalMoveStrategy.RuneType.TOP.waypoint))
				return false;
			else
				runeType = MyGlobalMoveStrategy.RuneType.TOP;
		} else {
			runeType = MyGlobalMoveStrategy.RuneType.BOT;
		}

		Wizard self = MyStrategy.self;
		double distance = MyUtils.getDistance(self, point.getX(), point.getY());
		double moveDistance = distance - self.getRadius() - MyStrategy.game.getBonusRadius();
		double maxWizardSpeed = MyUtils.getMaxWizardSpeed(self);
		if (moveDistance > maxWizardSpeed + MyMagicNumbers.DELTA) {
			if (moveDistance < MyMagicNumbers.DISTANCE_FOR_WAYPIONT_MOVE) {
				moveTo(point, attackMove, moveType, moveDistance);
				return true;
			}
			return false;
		}

		if (!MyGlobalMoveStrategy.isRuneGot.get(runeType) || MyGlobalMoveStrategy.getTimeToNextRune() == -1) {
			moveTo(point, attackMove, moveType, maxWizardSpeed);
			return true;
		}

		double coefficient = (moveDistance - maxWizardSpeed / 2) / maxWizardSpeed;
		moveTo(point, attackMove, moveType, maxWizardSpeed, coefficient);

		return true;
	}

	/**
	 * 
	 * @param distance
	 *            - moving distance
	 */
	public static void tryMoveToAngle(boolean attackMove, double angle, MyLocalMoveType moveType, double distance) {
		double coefficient = 1.0;
		tryMoveToAngle(attackMove, angle, moveType, distance, coefficient);
	}

	private static void tryMoveToAngle(boolean attackMove, double angle, MyLocalMoveType moveType, double distance,
			double coefficient) {
		double angleToMove = getBestAngelToMove(angle, moveType, distance);
		makeMoveToAngle(attackMove, angleToMove, coefficient);
	}

	private static double getBestAngelToMove(double angle, MyLocalMoveType moveType, double distance) {
		AngleRange combine = getRestrict(angle, moveType, distance);

		if (combine.equals(AngleRange.NONE))
			return angle;

		if (combine.equals(AngleRange.ALL)) {
			if (distance > MyUtils.getMaxWizardSpeed(MyStrategy.self)) {
				return getBestAngelToMove(angle, moveType, distance / 2);
			} else {
				return angle;
			}
		}

		double shiftToLeft = MyUtils.normalizeAngle(angle - combine.left);
		double shiftToRight = MyUtils.normalizeAngle(angle - combine.right);

		if (Math.abs(shiftToRight) < Math.abs(shiftToLeft))
			return combine.right;
		else
			return combine.left;
	}

	private static AngleRange getRestrict(double angle, MyLocalMoveType moveType, double distance) {
		Set<LivingUnit> objects = new LinkedHashSet<>();
		objects.addAll(MyStrategy.world.getBuildingsSet());
		objects.addAll(Arrays.asList(MyStrategy.world.getMinions()));
		objects.addAll(Arrays.asList(MyStrategy.world.getTrees()));
		objects.addAll(Arrays.asList(MyStrategy.world.getWizards()));

		Set<Set<AngleRange>> clustersRestricts = getUnitsRestricts(objects, distance, angle);

		return combineRestricts(angle, clustersRestricts);
	}

	private static AngleRange combineRestricts(double angle, Set<Set<AngleRange>> clustersRestricts) {
		double closestRadius = 0;
		Set<AngleRange> closestCluster = null;

		// choose main cluster
		for (Set<AngleRange> cluster : clustersRestricts) {
			for (AngleRange range : cluster) {
				if (range.contain(angle)) {
					if (closestCluster == null || range.raduis < closestRadius) {
						closestRadius = range.raduis;
						closestCluster = cluster;
					}
				}
			}
		}

		if (closestCluster == null)
			return AngleRange.NONE;

		AngleRange closestClusterRange = combineClusterRestricts(angle, closestCluster);
		assert !closestClusterRange.equals(AngleRange.NONE);

		if (closestClusterRange.equals(AngleRange.ALL))
			return AngleRange.ALL;

		Set<Set<AngleRange>> intersectsClusters = new LinkedHashSet<>();

		// find intersects clusters
		loop: for (Set<AngleRange> cluster : clustersRestricts) {
			if (cluster.equals(closestCluster))
				continue;
			for (AngleRange range : cluster) {
				if (closestClusterRange.contain(range.left) || closestClusterRange.contain(range.right)) {
					intersectsClusters.add(cluster);
					continue loop;
				}
			}
		}

		if (intersectsClusters.isEmpty())
			return closestClusterRange;

		// find left and right in main cluster
		double leftWithShift = MyUtils.normalizeAngle(closestClusterRange.left + MyMagicNumbers.DELTA * 2);
		double rightWithShift = MyUtils.normalizeAngle(closestClusterRange.right - MyMagicNumbers.DELTA * 2);

		double leftRaduis = MyStrategy.game.getMapSize() * 2;
		double rightRaduis = MyStrategy.game.getMapSize() * 2;

		for (AngleRange range : closestCluster) {
			if (range.contain(rightWithShift) && range.raduis < rightRaduis) {
				rightRaduis = range.raduis;
			}
			if (range.contain(leftWithShift) && range.raduis < leftRaduis) {
				leftRaduis = range.raduis;
			}
		}

		assert leftRaduis < MyStrategy.game.getMapSize() * 1.5;
		assert rightRaduis < MyStrategy.game.getMapSize() * 1.5;

		double left = MyUtils.normalizeAngle(closestClusterRange.left - 2 * MyMagicNumbers.DELTA);
		double right = MyUtils.normalizeAngle(closestClusterRange.right + 2 * MyMagicNumbers.DELTA);

		// find left and right for all clusters
		search: while (true) {
			if (MyStrategy.world.getTickIndex() == MyStrategy.moveToDebug) {
				MyStrategy.world.toString();
			}
			for (Set<AngleRange> intersectCluster : intersectsClusters) {
				for (AngleRange range : intersectCluster) {
					if (range.contain(left) && range.raduis < leftRaduis - MyMagicNumbers.DELTA) {
						left = MyUtils.normalizeAngle(range.right + 2 * MyMagicNumbers.DELTA);
						leftRaduis = range.raduis;
						if (MyStrategy.world.getTickIndex() == MyStrategy.moveToDebug) {
							MyStrategy.world.toString();
						}
						continue search;
					}
					if (range.contain(right) && range.raduis < rightRaduis - MyMagicNumbers.DELTA) {
						right = MyUtils.normalizeAngle(range.left - 2 * MyMagicNumbers.DELTA);
						rightRaduis = range.raduis;
						if (MyStrategy.world.getTickIndex() == MyStrategy.moveToDebug) {
							MyStrategy.world.toString();
						}
						continue search;
					}
				}
			}

			for (AngleRange range : closestCluster) {
				if (range.contain(left) && range.raduis < leftRaduis - MyMagicNumbers.DELTA) {
					left = MyUtils.normalizeAngle(range.left - 2 * MyMagicNumbers.DELTA);
					leftRaduis = range.raduis;
					if (MyStrategy.world.getTickIndex() == MyStrategy.moveToDebug) {
						MyStrategy.world.toString();
					}
					continue search;
				}
				if (range.contain(right) && range.raduis < rightRaduis - MyMagicNumbers.DELTA) {
					right = MyUtils.normalizeAngle(range.right + 2 * MyMagicNumbers.DELTA);
					rightRaduis = range.raduis;
					if (MyStrategy.world.getTickIndex() == MyStrategy.moveToDebug) {
						MyStrategy.world.toString();
					}
					continue search;
				}

			}
			break;
		}
		if (MyStrategy.world.getTickIndex() == MyStrategy.moveToDebug) {
			MyStrategy.world.toString();
		}

		return new AngleRange(left, right, closestRadius);
	}

	private static AngleRange combineClusterRestricts(double angle, Set<AngleRange> clustersRestricts) {
		double left = 0;
		double right = 0;
		boolean changed = false;

		for (AngleRange range : clustersRestricts) {
			if (range.equals(AngleRange.ALL))
				return AngleRange.ALL;
			if (range.equals(AngleRange.NONE))
				continue;
			if (range.contain(angle)) {
				changed = true;
				left = range.left;
				right = range.right;
			}
		}

		if (!changed)
			return AngleRange.NONE;

		double sumRange = 0;
		double rangeNumber = 1.0;
		while (changed) {
			changed = false;

			double leftWithShift = MyUtils.normalizeAngle(left - MyMagicNumbers.DELTA);
			double rightWithShift = MyUtils.normalizeAngle(right + MyMagicNumbers.DELTA);

			for (AngleRange range : clustersRestricts) {
				if (range.equals(AngleRange.NONE))
					continue;
				boolean containRight = range.contain(rightWithShift);
				boolean containLeft = range.contain(leftWithShift);

				if (containLeft && containRight) {
					double shift = MyUtils.normalizeAngle(-range.left - Math.PI);
					double shiftedRight = MyUtils.normalizeAngle(rightWithShift + shift);
					double shiftedLeft = MyUtils.normalizeAngle(leftWithShift + shift);
					if (shiftedRight < shiftedLeft) {
						assert new AngleRange(leftWithShift, rightWithShift, range.raduis).contain(range.left);
						assert new AngleRange(leftWithShift, rightWithShift, range.raduis).contain(range.right);
						return AngleRange.ALL;
					}
				}
				if (containRight) {
					right = range.right;
					changed = true;
				}
				if (containLeft) {
					left = range.left;
					changed = true;
				}
				if (changed) {
					sumRange += range.raduis;
					rangeNumber++;
					break;
				}
			}
		}
		left = MyUtils.normalizeAngle(left - MyMagicNumbers.DELTA);
		right = MyUtils.normalizeAngle(right + MyMagicNumbers.DELTA);
		return new AngleRange(left, right, sumRange / rangeNumber);

	}

	private static Set<Set<AngleRange>> getUnitsRestricts(Set<LivingUnit> objects, double movingDistance,
			double angle) {
		Set<LivingUnit> unitsInRange = new LinkedHashSet<>();
		Wizard self = MyStrategy.self;
		for (LivingUnit unit : objects) {
			double distance = MyUtils.getDistance(unit, self);
			if (distance > self.getRadius()) {
				if (distance - self.getRadius() - unit.getRadius() < movingDistance + MyMagicNumbers.DELTA)
					unitsInRange.add(unit);
			}
		}

		Set<Set<AngleRange>> result = new LinkedHashSet<>();
		boolean isBorderAdded = false;

		double diametr = self.getRadius() * 2;
		while (!unitsInRange.isEmpty()) {
			LivingUnit start = unitsInRange.iterator().next();
			Set<LivingUnit> cluster = new LinkedHashSet<>();
			cluster.add(start);
			Set<LivingUnit> currents = new LinkedHashSet<>();
			currents.add(start);
			while (!currents.isEmpty()) {
				Set<LivingUnit> toWatch = new LinkedHashSet<>();
				for (LivingUnit unit : currents) {
					for (LivingUnit pretendent : unitsInRange) {
						if (cluster.contains(pretendent))
							continue;
						double distance = MyUtils.getDistance(unit, pretendent);
						double radiusUnit = unit.getRadius();
						double radiusPretendent = pretendent.getRadius();
						double min = Math.min(radiusUnit, radiusPretendent) + self.getRadius();
						double maxSpeed = MyUtils.getMaxWizardSpeed(self);
						double delta = Math.sqrt(min * min + maxSpeed * maxSpeed) - min + MyMagicNumbers.DELTA;
						double d = radiusUnit + radiusPretendent + diametr + delta - distance;
						if (MyStrategy.world.getTickIndex() == MyStrategy.moveToDebug && d > 0 && d < 5.0) {
							MyStrategy.world.toString();
						}
						if (distance < radiusUnit + radiusPretendent + diametr + delta) {
							if (MyStrategy.RENDER) {
								MyStrategy.visualClient.line(unit.getX(), unit.getY(), pretendent.getX(),
										pretendent.getY(), new Color(0.5f, 0.5f, 0.5f));
							}
							toWatch.add(pretendent);
							cluster.add(pretendent);
						}
					}
				}

				currents = toWatch;
			}

			Set<AngleRange> clusterRestricts = new LinkedHashSet<>();
			for (LivingUnit unit : cluster) {
				unitsInRange.remove(unit);
				clusterRestricts.add(getAngleRangeFromUnit(unit));
				AngleRange borderRestrict = getBorderRestrictForUnit(unit);
				if (borderRestrict != null) {
					clusterRestricts.add(borderRestrict);
					if (!isBorderAdded) {
						AngleRange borderSelfRestrict = getBorderRestrictSelf(movingDistance);
						if (borderSelfRestrict != null) {
							clusterRestricts.add(borderSelfRestrict);
						}
					}
				}
			}

			if (MyStrategy.world.getTickIndex() == MyStrategy.moveToDebug) {
				AngleRange combine = combineClusterRestricts(angle, clusterRestricts);
				MyTesting.drowRestricts(clusterRestricts, combine, angle);
			}

			result.add(clusterRestricts);
		}

		if (!isBorderAdded) {
			AngleRange borderRestrict = getBorderRestrictSelf(movingDistance);
			if (borderRestrict != null) {
				Set<AngleRange> borderCluster = new LinkedHashSet<>();
				borderCluster.add(borderRestrict);
				result.add(borderCluster);
			}
		}

		return result;
	}

	private static AngleRange getBorderRestrictSelf(double movingDistance) {
		double radius = MyStrategy.self.getRadius() + MyUtils.getMaxWizardSpeed(MyStrategy.self);
		double mapSize = MyStrategy.game.getMapSize();
		double distance = movingDistance + MyMagicNumbers.DELTA;

		double x = MyStrategy.self.getX();
		double selfAngle = MyStrategy.self.getAngle();
		AngleRange xResult = null;

		if (x - radius < distance) {
			xResult = getAngleRangeForBorder(x - radius, distance, MyUtils.normalizeAngle(-selfAngle + Math.PI));
		} else if (mapSize - x - radius < distance) {
			xResult = getAngleRangeForBorder(mapSize - x - radius, distance, MyUtils.normalizeAngle(-selfAngle + 0));
		}

		double y = MyStrategy.self.getY();
		AngleRange yResult = null;
		if (y - radius < distance) {
			yResult = getAngleRangeForBorder(y - radius, distance, MyUtils.normalizeAngle(-selfAngle - Math.PI / 2.0));
		} else if (mapSize - y - radius < distance) {
			yResult = getAngleRangeForBorder(mapSize - y - radius, distance,
					MyUtils.normalizeAngle(-selfAngle + Math.PI / 2.0));
		}

		if (xResult == null && yResult == null) {
			return null;
		} else if (xResult != null && yResult != null) {
			// corner
			return combineCornerResults(xResult, yResult);
		} else if (xResult != null)
			return xResult;
		else
			return yResult;

	}

	private static AngleRange combineCornerResults(AngleRange xResult, AngleRange yResult) {
		double selfAngle = MyStrategy.self.getAngle();

		if ((xResult.contain(MyUtils.normalizeAngle(-selfAngle + 0))
				&& yResult.contain(MyUtils.normalizeAngle(-selfAngle + Math.PI / 2)))
				|| (xResult.contain(MyUtils.normalizeAngle(-selfAngle + Math.PI))
						&& yResult.contain(MyUtils.normalizeAngle(-selfAngle + -Math.PI / 2)))) {
			double left = xResult.left;
			double right = yResult.right;
			AngleRange result = new AngleRange(left, right, xResult.raduis);
			assert result.contain(xResult.right);
			assert result.contain(yResult.left);
			return result;
		} else {
			double left = yResult.left;
			double right = xResult.right;
			AngleRange result = new AngleRange(left, right, xResult.raduis);
			assert result.contain(xResult.left);
			assert result.contain(yResult.right);
			return result;
		}
	}

	private static AngleRange getAngleRangeForBorder(double toBorder, double distance, double center) {
		double delta = Math.acos(toBorder / distance);
		assert delta + MyMagicNumbers.DELTA > 0;
		double left = MyUtils.normalizeAngle(center - delta - MyMagicNumbers.DELTA);
		double right = MyUtils.normalizeAngle(center + delta + MyMagicNumbers.DELTA);
		AngleRange result = new AngleRange(left, right, MyStrategy.game.getMapSize() / 2);
		assert result.contain(center);
		return result;
	}

	private static AngleRange getBorderRestrictForUnit(LivingUnit unit) {
		Wizard self = MyStrategy.self;
		double distance = self.getRadius() * 2 + unit.getRadius();

		double x = unit.getX();
		double y = unit.getY();

		double angle = self.getAngleTo(unit);
		double selfAngle = MyStrategy.self.getAngle();

		double mapSize = MyStrategy.game.getMapSize();
		if (x < distance) {
			if (y < self.getY())
				return new AngleRange(MyUtils.normalizeAngle(-selfAngle + Math.PI), angle, mapSize / 2);
			else
				return new AngleRange(angle, MyUtils.normalizeAngle(-selfAngle + Math.PI), mapSize / 2);
		} else if (mapSize - x < distance) {
			if (y < self.getY())
				return new AngleRange(angle, MyUtils.normalizeAngle(-selfAngle + 0), mapSize / 2);
			else
				return new AngleRange(MyUtils.normalizeAngle(-selfAngle + 0), angle, mapSize / 2);
		}

		if (y < distance) {
			if (x < self.getX())
				return new AngleRange(angle, MyUtils.normalizeAngle(-selfAngle + -Math.PI / 2), mapSize / 2);
			else
				return new AngleRange(MyUtils.normalizeAngle(-selfAngle + -Math.PI / 2), angle, mapSize / 2);
		} else if (mapSize - y < distance) {
			if (x < self.getX())
				return new AngleRange(MyUtils.normalizeAngle(-selfAngle + Math.PI / 2), angle, mapSize / 2);
			else
				return new AngleRange(angle, MyUtils.normalizeAngle(-selfAngle + Math.PI / 2), mapSize / 2);
		}

		return null;
	}

	private static AngleRange getAngleRangeFromUnit(LivingUnit unit) {
		Wizard self = MyStrategy.self;
		double unitRadius = unit.getRadius();
		double Rr = self.getRadius() + unitRadius;

		double distance = MyUtils.getDistance(self, unit);
		double radius = Math.sqrt(distance * distance - Rr * Rr);
		assert distance > Rr;
		if (unit instanceof Wizard) {
			Rr += MyUtils.getMaxWizardSpeed((Wizard) unit);
		}
		double angle = self.getAngleTo(unit);
		double deltaAngle;
		if (distance > Rr) {
			deltaAngle = Math.asin(Rr / distance);
		} else {
			double minMove = MyUtils.getMinWizardSpeed(self);
			if (distance + minMove < Rr)
				deltaAngle = Math.PI - 2 * MyMagicNumbers.DELTA;
			else {
				deltaAngle = MyUtils.getAngleFromTriangle(Rr, minMove, distance);
			}

		}
		assert deltaAngle > 0;
		double left = MyUtils.normalizeAngle(angle - deltaAngle - MyMagicNumbers.DELTA);

		double right = MyUtils.normalizeAngle(angle + deltaAngle + MyMagicNumbers.DELTA);

		return new AngleRange(left, right, radius);

	}

	private static void makeMoveToAngle(boolean attackMove, double angle, double coefficient) {
		double maxWizardSpeed = MyUtils.getMaxWizardSpeed(MyStrategy.self);
		double minWizardSpeed = MyUtils.getMinWizardSpeed(MyStrategy.self);

		MoveCoordinates coordinates = getCoordinatesToMove(angle, maxWizardSpeed, minWizardSpeed);

		MyStrategy.move.setSpeed(coordinates.forward * coefficient);
		MyStrategy.move.setStrafeSpeed(coordinates.strafe * coefficient);

		assert (!isGoThroughBorder());
		assert (MyStrategy.checkMoving());

		if (!attackMove)
			MyStrategy.move.setTurn(angle);
	}

	public static MoveCoordinates getCoordinatesToMove(double angle, double maxWizardSpeed, double minWizardSpeed) {
		double tg = Math.tan(angle);
		double maxForwardSpeed;
		if (Math.abs(angle) < Math.PI / 2) {
			maxForwardSpeed = maxWizardSpeed;
		} else
			maxForwardSpeed = minWizardSpeed;
		double maxStrafeSpeed = minWizardSpeed;

		double speedForward = Math
				.sqrt(1 / (1 / (maxForwardSpeed * maxForwardSpeed) + tg * tg / (maxStrafeSpeed * maxStrafeSpeed)));

		assert (speedForward <= maxForwardSpeed + MyMagicNumbers.DELTA) : speedForward + " " + maxForwardSpeed + " "
				+ tg;

		double speedStrafe = Math.abs(speedForward * tg);

		assert (speedStrafe < maxStrafeSpeed + MyMagicNumbers.DELTA) : speedStrafe + " " + maxStrafeSpeed + " "
				+ speedForward + " " + tg;

		if (Math.abs(angle) > Math.PI / 2) {
			speedForward = -speedForward;
		}

		if (angle < 0) {
			speedStrafe = -speedStrafe;
		}

		MoveCoordinates coordinates = new MoveCoordinates(speedForward, speedStrafe);
		assert (speedForward * speedForward / (maxForwardSpeed * maxForwardSpeed)
				+ speedStrafe * speedStrafe / (maxStrafeSpeed * maxStrafeSpeed) > 0.99);
		return coordinates;
	}

	private static boolean isGoThroughBorder() {
		Wizard self = MyStrategy.self;
		double angle = self.getAngle();
		double x = self.getX();
		double deltaX = MyStrategy.move.getSpeed() * Math.cos(angle)
				- MyStrategy.move.getStrafeSpeed() * Math.sin(angle);
		if (deltaX + x < self.getRadius() - MyMagicNumbers.DELTA)
			return true;
		if (deltaX + x > MyStrategy.game.getMapSize() - self.getRadius() + MyMagicNumbers.DELTA)
			return true;

		double y = self.getY();
		double deltaY = MyStrategy.move.getSpeed() * Math.sin(angle)
				+ MyStrategy.move.getStrafeSpeed() * Math.cos(angle);
		if (deltaY + y < self.getRadius() - MyMagicNumbers.DELTA)
			return true;
		if (deltaY + y > MyStrategy.game.getMapSize() - self.getRadius() + MyMagicNumbers.DELTA)
			return true;

		return false;
	}

	public static class MoveCoordinates {
		final double forward;
		final double strafe;

		public MoveCoordinates(double forward, double strafe) {
			super();
			this.forward = forward;
			this.strafe = strafe;
		}

	}

	public static class AngleRange {
		public static final AngleRange NONE = new AngleRange(0, MyMagicNumbers.DELTA, 100.0);
		public static final AngleRange ALL = new AngleRange(0, -MyMagicNumbers.DELTA, 100.0);
		public final double left;
		public final double right;
		public final double raduis;

		public boolean contain(double angle) {
			assert (angle < Math.PI + MyMagicNumbers.DELTA);
			assert (angle > -Math.PI - MyMagicNumbers.DELTA);

			if (this.equals(ALL))
				return true;
			if (this.equals(NONE))
				return false;

			if (left < right) {
				return angle > left && angle < right;
			} else {
				return angle < right || angle > left;
			}
		}

		public AngleRange(double left, double right, double raduis) {
			super();
			this.left = left;
			this.right = right;
			this.raduis = raduis;
		}

		@Override
		public String toString() {
			return "AngleRange [left=" + left + ", right=" + right + ", raduis=" + raduis + "]";
		}

	}

}
