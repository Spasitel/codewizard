import java.awt.Color;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import model.Building;
import model.Game;
import model.LivingUnit;
import model.Minion;
import model.Move;
import model.Tree;
import model.Unit;
import model.Wizard;
import model.World;

public final class MyStrategy implements Strategy {
	public static final int moveToDebug = -1;
	private static boolean TEST_RUN = false;
	public static boolean RENDER = true;

	public static MyVisualClientIterface visualClient = null;

	static Wizard self;
	static MyWorld world = null;
	static Game game;
	static Move move;
	static MyGlobalState globalState = MyGlobalState.GO_TO_LANE;
	static boolean finalMode;
	static Boolean isFastPush = null;

	@Override
	public void move(Wizard self, World world, Game game, Move move) {
		finalMode = game.isRawMessagesEnabled();

		boolean repeaterMode = false;
		try {
			repeaterMode = System.getProperty("TurnOffRender") != null;
		} catch (Throwable e) {
		}
		if (finalMode || repeaterMode)
			RENDER = false;
		if (world.getTickIndex() >= moveToDebug) {
			world.toString();
		}

		if (RENDER) {
			if (visualClient == null) {
				try {
					visualClient = (MyVisualClientIterface) Class.forName("VisualClient").newInstance();
				} catch (ClassNotFoundException e) {
					RENDER = false;
				} catch (InstantiationException e) {
					RENDER = false;
				} catch (IllegalAccessException e) {
					RENDER = false;
				}
			}
		}
		if (RENDER) {
			MyStrategy.visualClient.beginPost();
			MyStrategy.visualClient.circle(self.getX(), self.getY(), self.getCastRange(), new Color(0.5f, 0.5f, 0.5f));

		}

		initializeTick(self, world, game, move);

		if (TEST_RUN) {
			if (MyTesting.testRun())
				return;
		}

		try {
			makeMove(self, world, game, move);
		} catch (AssertionError e) {
			assert printExeptionToFile(e);
		}

		if (RENDER) {
			MyStrategy.visualClient.endPost();
			// visualClient.sync(world.getTickIndex());
		}

	}

	private static boolean printExeptionToFile(Throwable e) {
		FileWriter fw = null;
		try {
			fw = new FileWriter("exeptions.txt", true);
			BufferedWriter bw = new BufferedWriter(fw);
			PrintWriter out = new PrintWriter(bw);
			e.printStackTrace(out);
		} catch (IOException a) {
			// exception handling left as an exercise for the reader
		} finally {
			try {
				if (fw != null)
					fw.close();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}

		return true;
	}

	private void makeMove(Wizard self, World world, Game game, Move move) {
		MyDodgeEnemyStrategy.updateInfo();

		LivingUnit bestTarget = MyAttackStrategy.getBestTarget();

		MyGlobalMoveStrategy.makeGlobalStrategy(bestTarget);

		boolean attackMove = MyAttackStrategy.attackMove(bestTarget);

		if (!attackMove)
			MyLocalMovingStrategy.moveToWayPoint(MyGlobalMoveStrategy.getNextWaypoint(), false,
					MyLocalMoveType.WAYPOINT);

		if (game.isSkillsEnabled()) {
			MySkillStartegy.learnSkill(bestTarget);
			MySkillStartegy.makeHasteMove(bestTarget);
		}

	}

	private static int prevTick = -10;
	private static double speedX = -10;
	private static double speedY = -10;
	private static double speedFactor = 1.0;
	private static double angle;
	private static double speed;
	private static double strafe;

	public static boolean checkMoving() {
		assert prevTick < world.getTickIndex();
		double multiplySpeedFactor = MyUtils.getMultiplySpeedFactor(self);
		if (world.getTickIndex() == prevTick + 1
				&& Math.abs(multiplySpeedFactor - speedFactor) < MyMagicNumbers.DELTA) {
			double maxSpeed = MyUtils.getMaxWizardSpeed(self);
			Unit closest = null;
			double minDistance = maxSpeed + MyMagicNumbers.DELTA;
			for (Tree tree : world.getTrees()) {
				if (MyUtils.getDistance(self, tree) - self.getRadius() - tree.getRadius() < minDistance) {
					closest = tree;
					minDistance = MyUtils.getDistance(self, tree) - self.getRadius() - tree.getRadius();
				}
			}
			for (Building building : world.getBuildings()) {
				if (MyUtils.getDistance(self, building) - self.getRadius() - building.getRadius() < minDistance) {
					closest = building;
					minDistance = MyUtils.getDistance(self, building) - self.getRadius() - building.getRadius();
				}
			}
			if (closest != null) {
				boolean isCheckNeeded = true;
				for (Wizard wizard : world.getWizards()) {
					if (wizard.isMe())
						continue;
					if (MyUtils.getDistance(self, wizard) < self.getRadius() + wizard.getRadius() + maxSpeed
							+ MyMagicNumbers.DELTA)
						isCheckNeeded = false;
				}
				for (Minion minion : world.getMinions()) {
					if (MyUtils.getDistance(self, minion) < self.getRadius() + minion.getRadius() + maxSpeed
							+ MyMagicNumbers.DELTA)
						isCheckNeeded = false;
				}

				if (isCheckNeeded) {
					// assert Math.abs(self.getSpeedX() - speedX) < MyMagicNumbers.DELTA : self.getSpeedX() + " : " +
					// speedX;
					// assert Math.abs(self.getSpeedY() - speedY) < MyMagicNumbers.DELTA : self.getSpeedY() + " : " +
					// speedY;
					if ((Math.abs(speedX) > 0.1 && Math.abs(self.getSpeedX()) < MyMagicNumbers.DELTA)
							|| (Math.abs(speedY) > 0.1 && Math.abs(self.getSpeedY()) < MyMagicNumbers.DELTA)) {
						System.out.println("Unit: " + closest.getX() + " " + closest.getY());
						System.out.println("Self: " + self.getX() + " " + self.getY());
						System.out.println("Try: " + speedX + " " + speedY);
						System.out.println("After try: " + (self.getX() + speedX) + " " + (speedY + self.getY()));
						System.out.println("Distance after try: " + minDistance);
						System.out.println("Speed: " + speed);
						System.out.println("Strafe: " + strafe);
						System.out.println("Angle: " + angle);
						System.out.println("Factor: " + speedFactor);
						System.out.println("SpeedAbs: " + Math.sqrt(strafe * strafe + speed * speed));
						assert false;
					}
				}
			}
		}

		double angle2 = self.getAngle();
		double speed2 = move.getSpeed();
		double strafeSpeed = move.getStrafeSpeed();
		double spX = speed2 * Math.cos(angle2) - strafeSpeed * Math.sin(angle2);
		double spY = speed2 * Math.sin(angle2) + strafeSpeed * Math.cos(angle2);
		double newX = spX + self.getX();
		double newY = spY + self.getY();
		for (Tree tree : world.getTrees()) {
			double distance = MyUtils.getDistance(tree, newX, newY) - self.getRadius() - tree.getRadius();
			if (distance < MyMagicNumbers.DELTA) {
				assert false : tree.getX() + " : " + tree.getY() + " : " + spX + " : " + spY + " : " + distance;
			}

		}
		for (Building building : world.getBuildings()) {
			double delta = MyUtils.getDistance(building, newX, newY) - self.getRadius() - building.getRadius();
			if (delta < MyMagicNumbers.DELTA) {
				assert false : building.getX() + " : " + building.getY() + " : " + spX + " : " + spY + " : " + delta;
				// MyLocalMovingStrategy.moveToWayPoint(MyGlobalMoveStrategy.getNextWaypoint(), true,
				// MyLocalMoveType.WAYPOINT);
			}

		}

		prevTick = world.getTickIndex();
		angle = angle2;
		speed = speed2;
		strafe = strafeSpeed;
		speedX = spX;
		speedY = spY;
		speedFactor = multiplySpeedFactor;

		return true;

	}

	static void initializeTick(Wizard self, World world, Game game, Move move) {
		MyStrategy.self = self;
		MyStrategy.game = game;
		MyStrategy.move = move;
		MyMagicNumbers.init(game);
		MyStrategy.world = MyWorld.create(MyStrategy.world, world);
	}
}