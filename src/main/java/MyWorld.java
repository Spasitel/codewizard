import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import model.Building;
import model.Faction;
import model.Minion;
import model.Projectile;
import model.ProjectileType;
import model.Wizard;
import model.World;

public class MyWorld extends World {

	public MyWorld(World world, Set<Building> buildings, Set<MyProjectile> projectiles) {
		super(world.getTickIndex(), world.getTickCount(), world.getWidth(), world.getHeight(), world.getPlayers(),
				world.getWizards(), world.getMinions(), world.getProjectiles(), world.getBonuses(),
				world.getBuildings(), world.getTrees());
		this.myBuildings = buildings;
		this.myProjectiles = projectiles;
	}

	public static MyWorld create(MyWorld prev, World world) {
		Set<Building> buildings = getBuildingsSet(prev, world);
		Set<MyProjectile> myProjectiles = getMyProjectiles(prev, world);
		return new MyWorld(world, buildings, myProjectiles);

	}

	private static Set<MyProjectile> getMyProjectiles(MyWorld prev, World world) {
		Set<MyProjectile> result = new LinkedHashSet<>();
		for (Projectile projectile : Arrays.asList(world.getProjectiles())) {
			MyProjectile prevProjectile = getPrevProjectile(projectile, prev.getMyProjectiles());
			MyProjectile newProjectile;
			if (prevProjectile == null) {
				newProjectile = getProjectileNew(projectile, world);
			} else {
				newProjectile = getProjectileFromPrev(projectile, prevProjectile, world);
			}
			result.add(newProjectile);
		}

		return result;
	}

	private static MyProjectile getProjectileFromPrev(Projectile projectile, MyProjectile prevProjectile, World world) {
		if (prevProjectile.isCertan()) {
			return new MyProjectile(projectile, prevProjectile.getEndX(), prevProjectile.getEndY(),
					prevProjectile.isCertan());
		} else {
			assert (!projectile.getType().equals(ProjectileType.DART));

			MyProjectile certain = createCertainProjetile(projectile, world);
			if (certain != null)
				return certain;
			return new MyProjectile(projectile, prevProjectile.getEndX(), prevProjectile.getEndY(), false);
		}
	}

	private static MyProjectile createCertainProjetile(Projectile projectile, World world) {
		MyProjectile create = null;
		for (Wizard wizard : Arrays.asList(world.getWizards())) {
			if (wizard.getId() == projectile.getOwnerUnitId()) {
				double maxDistance = wizard.getCastRange();
				double distance = MyUtils.getDistance(wizard, projectile);
				long tickDoneAlready = Math.round(distance / MyUtils.getProjetileSpeed(projectile.getType()));
				double distanceRemain = maxDistance - tickDoneAlready * MyUtils.getProjetileSpeed(projectile.getType());

				create = MyProjectile.create(projectile, distanceRemain, true);
				break;
			}
		}
		return create;
	}

	private static MyProjectile getProjectileNew(Projectile projectile, World world) {
		double maxDistance;
		boolean isCertain = false;
		if (!projectile.getType().equals(ProjectileType.DART)) {
			MyProjectile certain = createCertainProjetile(projectile, world);
			if (certain != null)
				return certain;
			maxDistance = MyMagicNumbers.WIZARD_MAX_CAST_RANGE;
		} else {
			isCertain = true;
			maxDistance = MyStrategy.game.getFetishBlowdartAttackRange();
		}

		double distance = maxDistance - MyUtils.getProjetileSpeed(projectile.getType());
		return MyProjectile.create(projectile, distance, isCertain);
	}

	private static MyProjectile getPrevProjectile(Projectile projectile, Set<MyProjectile> prevMyProjectiles) {
		for (MyProjectile myProjectile : prevMyProjectiles) {
			if (projectile.getId() == myProjectile.getId())
				return myProjectile;
		}
		return null;
	}

	private static Set<Building> getBuildingsSet(MyWorld prev, World world) {
		Set<Building> buildings = new LinkedHashSet<>();
		List<Building> asList = Arrays.asList(world.getBuildings());
		Faction myFaction = world.getMyPlayer().getFaction();
		if (prev == null) {
			for (Building building : asList) {
				assert (building.getFaction().equals(myFaction));

				buildings.add(building);
				Building building2 = new MyBuilding(10000 - building.getId(),
						MyStrategy.game.getMapSize() - building.getX(), MyStrategy.game.getMapSize() - building.getY(),
						building.getSpeedX(), building.getSpeedY(), building.getAngle(),
						MyUtils.getOppositeFaction(building.getFaction()), building.getRadius(), building.getLife(),
						building.getMaxLife(), building.getStatuses(), building.getType(), building.getVisionRange(),
						building.getAttackRange(), building.getDamage(), building.getCooldownTicks(),
						building.getRemainingActionCooldownTicks());
				buildings.add(building2);
			}
		} else {
			Set<Building> prevBuildingsSet = new LinkedHashSet<>(prev.getBuildingsSet());
			for (Building building : asList) {
				buildings.add(building);
				Iterator<Building> iterator = prevBuildingsSet.iterator();
				while (iterator.hasNext()) {
					Building prevBuilding = iterator.next();
					if (Math.abs(prevBuilding.getX() - building.getX()) < MyMagicNumbers.DELTA
							&& Math.abs(prevBuilding.getY() - building.getY()) < MyMagicNumbers.DELTA) {
						iterator.remove();
						break;
					}
				}
			}

			for (Building prevBuilding : prevBuildingsSet) {
				List<Minion> minions = Arrays.asList(world.getMinions());
				List<Wizard> wizards = Arrays.asList(world.getWizards());
				if (prevBuilding.getFaction().equals(myFaction)) {
					// my tower down.
				} else {
					boolean remove = false;
					for (Minion minion : minions) {
						if (minion.getFaction().equals(myFaction)) {
							if (MyUtils.getDistance(minion, prevBuilding) < minion.getVisionRange()
									- MyMagicNumbers.DELTA) {
								remove = true;
								break;
							}
						}
					}
					if (!remove) {
						for (Wizard wizard : wizards) {
							if (wizard.getFaction().equals(myFaction)) {
								if (MyUtils.getDistance(wizard, prevBuilding) < wizard.getVisionRange()
										- MyMagicNumbers.DELTA) {
									remove = true;
									break;
								}
							}
						}
					}
					if (!remove) {
						if (prevBuilding instanceof MyBuilding) {
							((MyBuilding) prevBuilding).decrementRemainingActionCooldownTicks();
							buildings.add(prevBuilding);
						} else {
							if (prevBuilding.getRemainingActionCooldownTicks() == 0) {
								buildings.add(prevBuilding);
							} else {
								MyBuilding myBuilding = new MyBuilding(prevBuilding);
								myBuilding.decrementRemainingActionCooldownTicks();
								buildings.add(myBuilding);
							}

						}
					}
				}
			}
		}
		return buildings;
	}

	private final Set<Building> myBuildings;

	public Set<Building> getBuildingsSet() {
		return myBuildings;
	}

	private final Set<MyProjectile> myProjectiles;

	public Set<MyProjectile> getMyProjectiles() {
		return myProjectiles;
	}

}
