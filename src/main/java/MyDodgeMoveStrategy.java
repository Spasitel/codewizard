import java.awt.Color;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import model.Projectile;
import model.ProjectileType;
import model.Wizard;

public class MyDodgeMoveStrategy {

	private static final double shift = Math.PI / 30;// TODO

	private static final Map<Integer, List<Position>> positionsForDodgeWithAngle;
	private static final Map<Integer, List<Position>> positionsForDodgeWithoutAngle;

	static {
		positionsForDodgeWithAngle = calculatePositionsForDodge(true, true);
		positionsForDodgeWithoutAngle = calculatePositionsForDodge(false, false);
	}

	public static Map<Integer, List<Position>> calculatePositionsForDodge(boolean optimal, boolean withAngle) {
		Map<Integer, List<Position>> result = new LinkedHashMap<>();

		List<Position> start = new LinkedList<>();
		for (int i = 0; i < Math.PI / shift; i++) {
			Position position = new Position(0, 0, 0, 0, i * shift, 0);
			start.add(position);
			Position position2 = new Position(0, 0, 0, 0, MyUtils.normalizeAngle(i * shift + Math.PI), 0);
			start.add(position2);
		}
		int step = 0;
		result.put(step, start);
		while (step < 20) {
			List<Position> current = new LinkedList<>();
			for (Position position : result.get(step)) {
				Position next = getNextPosition(position, optimal, withAngle);
				current.add(next);
			}
			step++;
			result.put(step, current);
		}
		return result;
	}

	private static Position getNextPosition(Position position, boolean optimal, boolean withAngle) {
		double deltaAngle = position.angleToMove - position.currentAngle;
		double moveAngle;
		if (optimal && withAngle)
			moveAngle = MyDodgeMoveStrategy.getBestAngeleToDodge(deltaAngle);
		else
			moveAngle = deltaAngle;

		MyLocalMovingStrategy.MoveCoordinates coordinates = MyLocalMovingStrategy.getCoordinatesToMove(moveAngle, 4.0,
				3.0);// FIXME

		int moves = position.moves + 1;
		double startAngle;
		if (moves == 1)
			startAngle = moveAngle;
		else
			startAngle = position.startAngle;

		double angleToTurn;
		if (withAngle)
			angleToTurn = position.angleToMove;
		else
			angleToTurn = MyUtils.normalizeAngle(position.angleToMove + Math.PI);
		double maxAngleChange = Math.PI / 30;// FIXME: haste
		double currentAngle;

		if (Math.abs(MyUtils.normalizeAngle(angleToTurn - position.currentAngle)) < maxAngleChange) {
			currentAngle = angleToTurn;
		} else {
			double deltaAngleToTurn = MyUtils.normalizeAngle(angleToTurn - position.currentAngle);
			if (deltaAngleToTurn > 0)
				currentAngle = MyUtils.normalizeAngle(position.currentAngle + maxAngleChange);
			else
				currentAngle = MyUtils.normalizeAngle(position.currentAngle - maxAngleChange);
		}

		double sin = Math.sin(position.currentAngle);
		double cos = Math.cos(position.currentAngle);
		double x = position.forward + coordinates.forward * cos - coordinates.strafe * sin;
		double y = position.strafe + coordinates.forward * sin + coordinates.strafe * cos;

		return new Position(x, y, currentAngle, startAngle, position.angleToMove, moves);
	}

	public static double getBestAngeleToDodge(double angle) {
		double cB = 3.0;
		double cF = 4.0;
		if (Math.abs(angle) > Math.PI / 2)
			return angle;

		if (Math.abs(angle) < MyMagicNumbers.DELTA)
			return angle;

		double tgB = Math.tan(Math.PI / 2 - Math.abs(angle));
		double x = cB * cB / Math.sqrt(cF * cF * tgB * tgB + cB * cB);
		double y = cF * Math.sqrt(1 - x * x / (cB * cB));
		double result = Math.atan(x / y);
		if (angle < 0)
			return -result;
		else
			return result;
	}

	/**
	 * @return null - no dodge move<br>
	 *         true - dodge with angle<br>
	 *         false - dodge without angle
	 * 
	 */
	public static Boolean makeDodgeMove() {

		Wizard self = MyStrategy.self;
		Set<DodgeMove> moves = new LinkedHashSet<>();
		for (MyProjectile myProjectile : MyStrategy.world.getMyProjectiles()) {
			if (myProjectile.getFaction().equals(self.getFaction()))
				continue;
			if (myProjectile.getType().equals(ProjectileType.FIREBALL)) {
				// TODO
			}

			double distanceToProjectile = MyUtils.getDistance(self, myProjectile);
			double distanceToHit = self.getRadius() + myProjectile.getRadius() + MyUtils.getMaxWizardSpeed(self);
			if (distanceToProjectile > MyMagicNumbers.WIZARD_MAX_CAST_RANGE + distanceToHit)
				continue;// too far

			double angleFromProjectileToMe = myProjectile.getAngleTo(self);
			if (Math.abs(angleFromProjectileToMe) > Math.PI / 2) {
				if (distanceToProjectile > distanceToHit)
					continue;// go in other direction

				if (myProjectile.getOwnerUnitId() == self.getId())
					continue;
				double angleToMove = MyUtils.normalizeAngle(self.getAngleTo(myProjectile) + Math.PI);
				moves.add(
						new DodgeMove(angleToMove, myProjectile, false, MyUtils.getMaxWizardSpeed(self), angleToMove));
			}

			double projectileStrafeDistance = distanceToProjectile * Math.sin(angleFromProjectileToMe);
			if (Math.abs(projectileStrafeDistance) > distanceToHit)
				continue; // miss

			double projectileForwardDistance = distanceToProjectile * Math.cos(angleFromProjectileToMe);
			double projectileDistanceToEnd = MyUtils.getDistance(myProjectile, myProjectile.getEndX(),
					myProjectile.getEndY());

			if (projectileDistanceToEnd < projectileForwardDistance - distanceToHit)
				continue; // not come close enough

			if (projectileDistanceToEnd < projectileForwardDistance) {
				double distanceToEnd = MyUtils.getDistance(self, myProjectile.getEndX(), myProjectile.getEndY());
				if (distanceToEnd > distanceToHit)
					continue; // not come close enough
			}

			double lastPositionDistance;
			double prevLastPositionDistance;
			double projetileSpeed = MyUtils.getProjetileSpeed(myProjectile.getType());
			if (projectileForwardDistance > projectileDistanceToEnd) {
				lastPositionDistance = projectileDistanceToEnd;
			} else {
				int steps = (int) (projectileForwardDistance / projetileSpeed) + 1;
				if (((double) steps) * projetileSpeed > projectileDistanceToEnd) {
					lastPositionDistance = projectileDistanceToEnd;
				} else {
					lastPositionDistance = ((double) steps) * projetileSpeed;
				}
			}

			int lastSteps = (int) ((lastPositionDistance - MyMagicNumbers.DELTA) / projetileSpeed) + 1;
			if (lastSteps == 1) {
				prevLastPositionDistance = 0;
			} else {
				prevLastPositionDistance = ((double) (lastSteps - 1)) * projetileSpeed;
			}

			DodgeMove dodgeMove = getBestDodgeMove(myProjectile, lastPositionDistance, prevLastPositionDistance,
					lastSteps, self);

			if (dodgeMove != null)
				moves.add(dodgeMove);

		}

		if (moves.isEmpty())
			return null;
		DodgeMove toMove = null;

		if (moves.size() == 1) {
			toMove = moves.iterator().next();
		} else {
			for (DodgeMove move : moves) {
				if (toMove == null) {
					toMove = move;
					continue;
				}

				if (!toMove.isAngle && move.isAngle) {
					toMove = move;
					continue;
				}
				if (toMove.isAngle && !move.isAngle)
					continue;

				if (MyUtils.getProjetileDamage(toMove.projectile.getType()) < MyUtils
						.getProjetileDamage(move.projectile.getType()))
					toMove = move;
			}
		}

		assert toMove != null;

		MyLocalMovingStrategy.tryMoveToAngle(true, toMove.angleToMove, MyLocalMoveType.DODGE, toMove.distance);
		if (toMove.isAngle) {
			MyStrategy.move.setTurn(toMove.angleToTurn);

			return true;
		}

		return false;
	}

	public static DodgeMove getBestDodgeMove(MyProjectile myProjectile, double lastPositionDistance,
			double prevLastPositionDistance, int lastSteps, Wizard wizard) {

		List<Position> positionsLast = positionsForDodgeWithoutAngle.get(lastSteps);
		List<Position> positionsPrev = positionsForDodgeWithoutAngle.get(lastSteps - 1);

		if (positionsLast != null) {
			DodgeMove dodgeMove = getMoveFromPositions(myProjectile, lastPositionDistance, prevLastPositionDistance,
					positionsLast, positionsPrev, false, wizard);
			if (dodgeMove != null)
				return dodgeMove;
		}

		positionsLast = positionsForDodgeWithAngle.get(lastSteps);
		positionsPrev = positionsForDodgeWithAngle.get(lastSteps - 1);
		if (positionsLast != null) {
			DodgeMove dodgeMove = getMoveFromPositions(myProjectile, lastPositionDistance, prevLastPositionDistance,
					positionsLast, positionsPrev, true, wizard);
			if (dodgeMove != null)
				return dodgeMove;
		}
		return null;
	}

	private static DodgeMove getMoveFromPositions(MyProjectile myProjectile, double lastPositionDistance,
			double prevLastPositionDistance, List<Position> positionsLast, List<Position> positionsPrev, boolean isTurn,
			Wizard wizard) {
		Position best = null;
		double bestDistance = MyMagicNumbers.DELTA;
		for (Position position : positionsLast) {
			double distanceLast = getDistanceFromProjectile(myProjectile, lastPositionDistance, position, wizard);
			if (distanceLast > bestDistance) {
				if (positionsPrev != null) {
					for (Position positionPrev : positionsPrev) {
						if (Math.abs(position.angleToMove - positionPrev.angleToMove) < MyMagicNumbers.DELTA) {
							double distancePrev = getDistanceFromProjectile(myProjectile, prevLastPositionDistance,
									positionPrev, wizard);
							if (distancePrev > MyMagicNumbers.DELTA) {
								bestDistance = distanceLast;
								best = position;
							}
							break;
						}
					}
				} else {
					bestDistance = distanceLast;
					best = position;
				}
			}
		}

		if (best != null) {
			if (MyStrategy.RENDER && wizard.isMe()) {
				double sin = Math.sin(wizard.getAngle());
				double cos = Math.cos(wizard.getAngle());
				double mul = MyUtils.getMultiplySpeedFactor(wizard);
				double newX = wizard.getX() + best.forward * cos * mul - best.strafe * sin * mul;
				double newY = wizard.getY() + best.forward * sin * mul + best.strafe * cos * mul;

				Color color;
				if (isTurn) {
					color = new Color(1.0f, 0.0f, 0.0f);
				} else {
					color = new Color(0.0f, 1.0f, 0.0f);
				}
				MyStrategy.visualClient.line(wizard.getX(), wizard.getY(), newX, newY, color);
			}

			return new DodgeMove(best.startAngle, myProjectile, isTurn,
					MyUtils.getDistance(0, 0, best.forward, best.strafe) * MyUtils.getMultiplySpeedFactor(wizard),
					best.angleToMove);
		}
		return null;
	}

	private static double getDistanceFromProjectile(MyProjectile projectile, double lastPositionDistance,
			Position position, Wizard wizard) {
		double sin = Math.sin(wizard.getAngle());
		double cos = Math.cos(wizard.getAngle());
		double mul = MyUtils.getMultiplySpeedFactor(wizard);
		double newX = wizard.getX() + position.forward * cos * mul - position.strafe * sin * mul;
		double newY = wizard.getY() + position.forward * sin * mul + position.strafe * cos * mul;

		double angleFromProjectile = projectile.getAngleTo(newX, newY);
		double distanceFromProjectile = MyUtils.getDistance(projectile, newX, newY);
		double positionDistance = distanceFromProjectile * Math.cos(angleFromProjectile);
		double delta;
		if (positionDistance > lastPositionDistance) {
			double sinP = Math.sin(projectile.getAngle());
			double cosP = Math.cos(projectile.getAngle());
			double projectileX = projectile.getX() + lastPositionDistance * cosP;
			double projectileY = projectile.getY() + lastPositionDistance * sinP;
			delta = MyUtils.getDistance(newX, newY, projectileX, projectileY);
		} else {
			delta = Math.abs(distanceFromProjectile * Math.sin(angleFromProjectile));
		}

		delta = delta - wizard.getRadius() - projectile.getRadius();

		return delta;
	}

	static class DodgeMove {
		final double angleToMove;
		final Projectile projectile;
		final boolean isAngle;
		final double distance;
		final double angleToTurn;

		public DodgeMove(double angleToMove, Projectile projectile, boolean isAngle, double distance,
				double angleToTurn) {
			super();
			this.angleToMove = angleToMove;
			this.projectile = projectile;
			this.isAngle = isAngle;
			this.distance = distance;
			this.angleToTurn = angleToTurn;
		}

	}

	static class Position {
		final double forward;
		final double strafe;
		final double currentAngle;
		final double startAngle;
		final double angleToMove;
		final int moves;

		public Position(double x, double y, double currentAngle, double startAngle, double angleToMove, int moves) {
			super();
			this.forward = x;
			this.strafe = y;
			this.currentAngle = currentAngle;
			this.startAngle = startAngle;
			this.angleToMove = angleToMove;
			this.moves = moves;
		}

		@Override
		public String toString() {
			return "Position [x=" + forward + ", y=" + strafe + ", currentAngle=" + currentAngle + ", startAngle="
					+ startAngle + ", angleToMove=" + angleToMove + ", moves=" + moves + "]";
		}

	}
}
