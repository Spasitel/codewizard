import model.Unit;

/**
 * Вспомогательный класс для хранения позиций на карте.
 */
final class MyDataPoint2D {
	private final double x;
	private final double y;

	MyDataPoint2D(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public double getDistanceTo(double x, double y) {
		return StrictMath.hypot(this.x - x, this.y - y);
	}

	public double getDistanceTo(MyDataPoint2D point) {
		return getDistanceTo(point.x, point.y);
	}

	public double getDistanceTo(Unit unit) {
		return getDistanceTo(unit.getX(), unit.getY());
	}

	@Override
	public String toString() {
		return "MyDataPoint2D [x=" + x + ", y=" + y + "]";
	}

	@Override
	public int hashCode() {
		int result = 11;
		result += 17 * (int) (x * 100.0);
		result += 23 * (int) (y * 100.0);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MyDataPoint2D other = (MyDataPoint2D) obj;
		if (Math.abs(x - other.x) > MyMagicNumbers.DELTA)
			return false;
		if (Math.abs(y - other.y) > MyMagicNumbers.DELTA)
			return false;
		return true;
	}

}