import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import model.Projectile;
import model.ProjectileType;
import model.Wizard;

public class MyDodgeEnemyStrategy {

	private static Map<Integer, EvadionStatistic> statistic = new LinkedHashMap<>();

	private static ShootInfo shootInfo = null;
	private static int defaultId = -9898;

	public static void shootTo(Wizard target, boolean isAngle, double minDistance, ProjectileType projectileType) {
		assert shootInfo == null;
		Wizard self = MyStrategy.self;

		int minTick = MyStrategy.world.getTickIndex() + (int) (minDistance / MyUtils.getProjetileSpeed(projectileType));
		int maxTick = MyStrategy.world.getTickIndex()
				+ (int) (self.getCastRange() / MyUtils.getProjetileSpeed(projectileType));
		shootInfo = new ShootInfo(defaultId, target.getId(), isAngle, minTick, maxTick, target.getLife(), self.getXp(),
				projectileType);
	}

	public static void updateInfo() {
		Wizard self = MyStrategy.self;
		if (shootInfo == null)
			return;
		if (shootInfo.idProjectile == defaultId) {
			boolean isFound = false;
			for (Projectile projectile : MyStrategy.world.getProjectiles()) {

				if (projectile.getOwnerUnitId() == self.getId()) {
					shootInfo.idProjectile = projectile.getId();
					isFound = true;
					break;
				}
			}
			if (!isFound) {
				// tree at 1 step
				shootInfo = null;
				return;
			}

		}

		int tickIndex = MyStrategy.world.getTickIndex();
		if (tickIndex > shootInfo.maxTick + 1) {
			// I was dead or frozen
			shootInfo = null;
			return;
		}

		boolean isFound = false;
		for (Projectile projectile : MyStrategy.world.getProjectiles()) {
			if (projectile.getId() == shootInfo.idProjectile) {
				isFound = true;
				break;
			}
		}
		int selfXp = self.getXp();
		int enemyHP = 0;
		for (Wizard wizard : MyStrategy.world.getWizards()) {
			if (wizard.getId() == shootInfo.idTarget)
				enemyHP = wizard.getLife();
		}
		if (!isFound) {
			if (tickIndex <= shootInfo.minTick) {
				// tree or lost vision
				shootInfo = null;
				return;
			}
			int deltaXp = selfXp - shootInfo.selfXp;
			if ((deltaXp > 0 && deltaXp < 5) || (selfXp > shootInfo.selfXp && enemyHP < shootInfo.targetHp
					&& (enemyHP > 0 || shootInfo.targetHp <= 24))) {// FIXME
				// hit
				int key = (int) shootInfo.idTarget;
				if (!statistic.containsKey(key))
					statistic.put(key, new EvadionStatistic());
				if (shootInfo.isAngle)
					statistic.get(key).shootsInTargetWithAngle++;
				else
					statistic.get(key).shootsInTargetWithoutAngle++;
			} else if (deltaXp == 0 && enemyHP > 0) {
				// miss
				int key = (int) shootInfo.idTarget;
				if (!statistic.containsKey(key))
					statistic.put(key, new EvadionStatistic());
				if (shootInfo.isAngle)
					statistic.get(key).shootsDodgeWithAngle++;
				else
					statistic.get(key).shootsDodgeWithoutAngle++;
			}
			shootInfo = null;
			return;
		}

		shootInfo.selfXp = selfXp;
		shootInfo.targetHp = enemyHP;
	}

	public static boolean isWizardWillEvade(Wizard wizard, boolean isAngle) {
		if (isAngle && !isWizardWillEvade(wizard, false))
			return false;
		int hitsInTarget = 0;
		int hitsMissed = 0;
		int minMissForStats = 2;

		if (MyStrategy.finalMode) {
			minMissForStats = 4;
			for (Entry<Integer, EvadionStatistic> entry : statistic.entrySet()) {
				if (isAngle) {
					hitsInTarget += entry.getValue().shootsInTargetWithAngle;
					hitsMissed += entry.getValue().shootsDodgeWithAngle;
				} else {
					hitsInTarget += entry.getValue().shootsInTargetWithoutAngle;
					hitsMissed += entry.getValue().shootsDodgeWithoutAngle;
				}

			}
		} else {
			int key = (int) wizard.getId();
			if (!statistic.containsKey(key))
				return false;
			if (isAngle) {
				hitsInTarget += statistic.get(key).shootsInTargetWithAngle;
				hitsMissed += statistic.get(key).shootsDodgeWithAngle;
			} else {
				hitsInTarget += statistic.get(key).shootsInTargetWithoutAngle;
				hitsMissed += statistic.get(key).shootsDodgeWithoutAngle;
			}
		}

		if (hitsMissed < minMissForStats)
			return false; // Too small stat or not evade

		if (hitsInTarget > hitsMissed * 2)
			return false;

		return true;
	}

	private static class EvadionStatistic {
		int shootsInTargetWithAngle = 0;
		int shootsInTargetWithoutAngle = 0;
		int shootsDodgeWithAngle = 0;
		int shootsDodgeWithoutAngle = 0;

	}

	private static class ShootInfo {
		long idProjectile;
		long idTarget;
		boolean isAngle;

		/**
		 * last tick when projectile should exist
		 */
		int minTick;
		/**
		 * lath tick when projectile can exist
		 */
		int maxTick;

		int targetHp;
		int selfXp;
		@SuppressWarnings("unused")
		ProjectileType projectileType;

		public ShootInfo(long idProjectile, long idTarget, boolean isAngle, int minTick, int maxTick, int targetHp,
				int selfXp, ProjectileType projectileType) {
			super();
			this.idProjectile = idProjectile;
			this.idTarget = idTarget;
			this.isAngle = isAngle;
			this.minTick = minTick;
			this.maxTick = maxTick;
			this.targetHp = targetHp;
			this.selfXp = selfXp;
			this.projectileType = projectileType;
		}

	}
}
