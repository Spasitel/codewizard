import model.ActionType;
import model.LivingUnit;
import model.Minion;
import model.SkillType;
import model.Status;
import model.StatusType;
import model.Wizard;

public class MySkillStartegy {

	static SkillType[] defaultSkills = new SkillType[] { SkillType.RANGE_BONUS_PASSIVE_1, SkillType.RANGE_BONUS_AURA_1,
			SkillType.RANGE_BONUS_PASSIVE_2, SkillType.RANGE_BONUS_AURA_2, SkillType.ADVANCED_MAGIC_MISSILE,
			SkillType.MOVEMENT_BONUS_FACTOR_PASSIVE_1, SkillType.MOVEMENT_BONUS_FACTOR_AURA_1,
			SkillType.MOVEMENT_BONUS_FACTOR_PASSIVE_2, SkillType.MOVEMENT_BONUS_FACTOR_AURA_2, SkillType.HASTE,
			SkillType.MAGICAL_DAMAGE_BONUS_PASSIVE_1, SkillType.MAGICAL_DAMAGE_BONUS_AURA_1,
			SkillType.MAGICAL_DAMAGE_BONUS_PASSIVE_2, SkillType.MAGICAL_DAMAGE_BONUS_AURA_2,
			SkillType.MAGICAL_DAMAGE_ABSORPTION_PASSIVE_1, SkillType.MAGICAL_DAMAGE_ABSORPTION_AURA_1,
			SkillType.MAGICAL_DAMAGE_ABSORPTION_PASSIVE_2, SkillType.MAGICAL_DAMAGE_ABSORPTION_AURA_2 };

	static SkillType[] tripple1Skills = new SkillType[] { SkillType.RANGE_BONUS_PASSIVE_1, SkillType.RANGE_BONUS_AURA_1,
			SkillType.RANGE_BONUS_PASSIVE_2, SkillType.RANGE_BONUS_AURA_2, SkillType.ADVANCED_MAGIC_MISSILE,
			SkillType.MAGICAL_DAMAGE_BONUS_PASSIVE_1, SkillType.MAGICAL_DAMAGE_BONUS_AURA_1,
			SkillType.MAGICAL_DAMAGE_BONUS_PASSIVE_2, SkillType.MAGICAL_DAMAGE_BONUS_AURA_2,
			SkillType.MOVEMENT_BONUS_FACTOR_PASSIVE_1, SkillType.MAGICAL_DAMAGE_ABSORPTION_PASSIVE_1,
			SkillType.STAFF_DAMAGE_BONUS_PASSIVE_1, SkillType.MOVEMENT_BONUS_FACTOR_AURA_1,
			SkillType.MOVEMENT_BONUS_FACTOR_PASSIVE_2, SkillType.MOVEMENT_BONUS_FACTOR_AURA_2, SkillType.HASTE,
			SkillType.MAGICAL_DAMAGE_ABSORPTION_AURA_1, SkillType.MAGICAL_DAMAGE_ABSORPTION_PASSIVE_2 };

	static SkillType[] tripple2Skills = new SkillType[] { SkillType.MOVEMENT_BONUS_FACTOR_PASSIVE_1,
			SkillType.MOVEMENT_BONUS_FACTOR_AURA_1, SkillType.MOVEMENT_BONUS_FACTOR_PASSIVE_2,
			SkillType.MOVEMENT_BONUS_FACTOR_AURA_2, SkillType.HASTE, SkillType.STAFF_DAMAGE_BONUS_PASSIVE_1,
			SkillType.STAFF_DAMAGE_BONUS_AURA_1, SkillType.STAFF_DAMAGE_BONUS_PASSIVE_2,
			SkillType.STAFF_DAMAGE_BONUS_AURA_2, SkillType.FIREBALL, SkillType.RANGE_BONUS_PASSIVE_1,
			SkillType.MAGICAL_DAMAGE_BONUS_PASSIVE_1, SkillType.MAGICAL_DAMAGE_ABSORPTION_PASSIVE_1,
			SkillType.RANGE_BONUS_AURA_1, SkillType.RANGE_BONUS_PASSIVE_2, SkillType.RANGE_BONUS_AURA_2,
			SkillType.MAGICAL_DAMAGE_BONUS_AURA_1, SkillType.MAGICAL_DAMAGE_BONUS_PASSIVE_2,
			SkillType.MAGICAL_DAMAGE_BONUS_AURA_2, SkillType.MAGICAL_DAMAGE_ABSORPTION_AURA_1,
			SkillType.MAGICAL_DAMAGE_ABSORPTION_PASSIVE_2, SkillType.MAGICAL_DAMAGE_ABSORPTION_AURA_2 };

	static SkillType[] tripple3Skills = new SkillType[] { SkillType.STAFF_DAMAGE_BONUS_PASSIVE_1,
			SkillType.STAFF_DAMAGE_BONUS_AURA_1, SkillType.STAFF_DAMAGE_BONUS_PASSIVE_2,
			SkillType.STAFF_DAMAGE_BONUS_AURA_2, SkillType.FIREBALL, SkillType.RANGE_BONUS_PASSIVE_1,
			SkillType.MAGICAL_DAMAGE_ABSORPTION_PASSIVE_1, SkillType.MAGICAL_DAMAGE_ABSORPTION_AURA_1,
			SkillType.MAGICAL_DAMAGE_ABSORPTION_PASSIVE_2, SkillType.MAGICAL_DAMAGE_ABSORPTION_AURA_2,
			SkillType.MOVEMENT_BONUS_FACTOR_PASSIVE_1, SkillType.RANGE_BONUS_AURA_1, SkillType.RANGE_BONUS_PASSIVE_2,
			SkillType.MOVEMENT_BONUS_FACTOR_AURA_1, SkillType.MOVEMENT_BONUS_FACTOR_PASSIVE_2,
			SkillType.MOVEMENT_BONUS_FACTOR_AURA_2, SkillType.HASTE };

	static SkillType[] fastPuchSkills = new SkillType[] { SkillType.STAFF_DAMAGE_BONUS_PASSIVE_1,
			SkillType.STAFF_DAMAGE_BONUS_AURA_1, SkillType.STAFF_DAMAGE_BONUS_PASSIVE_2,
			SkillType.STAFF_DAMAGE_BONUS_AURA_2, SkillType.FIREBALL, SkillType.MOVEMENT_BONUS_FACTOR_PASSIVE_1,
			SkillType.MAGICAL_DAMAGE_BONUS_PASSIVE_1, SkillType.MAGICAL_DAMAGE_BONUS_AURA_1,
			SkillType.MAGICAL_DAMAGE_BONUS_PASSIVE_2, SkillType.MAGICAL_DAMAGE_BONUS_AURA_2,
			SkillType.MAGICAL_DAMAGE_ABSORPTION_PASSIVE_1, SkillType.MAGICAL_DAMAGE_ABSORPTION_AURA_1,
			SkillType.MAGICAL_DAMAGE_ABSORPTION_PASSIVE_2, SkillType.MAGICAL_DAMAGE_ABSORPTION_AURA_2,

			SkillType.MOVEMENT_BONUS_FACTOR_AURA_1, SkillType.MOVEMENT_BONUS_FACTOR_PASSIVE_2,
			SkillType.MOVEMENT_BONUS_FACTOR_AURA_2, SkillType.HASTE, SkillType.RANGE_BONUS_PASSIVE_1,
			SkillType.RANGE_BONUS_AURA_1, SkillType.RANGE_BONUS_PASSIVE_2, SkillType.RANGE_BONUS_AURA_2,
			SkillType.ADVANCED_MAGIC_MISSILE };

	public static void learnSkill(LivingUnit bestTarget) {
		Wizard self = MyStrategy.self;
		int level = self.getLevel();
		int skills = self.getSkills().length;
		SkillType[] arrayAbilitys = null;
		if (skills < level) {
			if (MyStrategy.finalMode) {
				switch ((int) (self.getId() % 5)) {
				case 0:
				case 1:
					if (MyStrategy.isFastPush == null) {
						MyStrategy.isFastPush = MyGlobalMoveStrategy.isFastPushMod();
					} else {
						if (MyStrategy.isFastPush)
							arrayAbilitys = fastPuchSkills;
						else
							arrayAbilitys = defaultSkills;
					}
					break;
				case 4:
					arrayAbilitys = tripple1Skills;
					break;
				case 2:
					arrayAbilitys = tripple2Skills;
					break;
				case 3:
					arrayAbilitys = tripple3Skills;
					break;
				default:
					break;
				}
			} else {
				arrayAbilitys = defaultSkills;
			}
		}
		if (arrayAbilitys != null && arrayAbilitys.length > skills) {
			MyStrategy.move.setSkillToLearn(arrayAbilitys[skills]);
		}
	}

	public static void makeHasteMove(LivingUnit bestTarget) {
		Wizard self = MyStrategy.self;
		int cdHaste = self.getRemainingCooldownTicksByAction()[ActionType.HASTE.ordinal()];
		int cdAction = self.getRemainingActionCooldownTicks();
		boolean isHasHaste = false;
		for (SkillType skillType : self.getSkills()) {
			if (skillType.equals(SkillType.HASTE))
				isHasHaste = true;
		}
		if (isHasHaste && cdHaste <= cdAction) {
			if (self.getMana() >= MyStrategy.game.getHasteManacost()) {
				if (bestTarget == null || bestTarget instanceof Minion) {
					boolean selfHasted = false;
					for (Status status : self.getStatuses()) {
						if (status.getType().equals(StatusType.HASTENED)
								&& status.getRemainingDurationTicks() > cdAction)
							selfHasted = true;
					}
					Wizard bestWizard = null;
					double bestAngle = 3.0;
					wizards: for (Wizard wizard : MyStrategy.world.getWizards()) {
						if (wizard.getFaction().equals(self.getFaction()) && !wizard.isMe()) {
							for (Status status : wizard.getStatuses()) {
								if (status.getType().equals(StatusType.HASTENED)
										&& status.getRemainingDurationTicks() > cdAction)
									continue wizards;
							}
							double distance = MyUtils.getDistance(self, wizard);
							if (distance < self.getCastRange()) {
								double angle = self.getAngleTo(wizard);
								if (bestWizard == null || Math.abs(angle) < bestAngle) {
									bestWizard = wizard;
									bestAngle = Math.abs(angle);
								}
							}
						}
					}
					if (selfHasted && bestTarget == null)
						return;

					if (cdHaste == 0 && cdAction == 0) {
						if (bestWizard == null) {
							MyStrategy.move.setAction(ActionType.HASTE);
							MyStrategy.move.setStatusTargetId(self.getId());
							return;
						} else {
							if (bestAngle < MyStrategy.game.getStaffSector() / 2.0) {
								MyStrategy.move.setAction(ActionType.HASTE);
								MyStrategy.move.setStatusTargetId(bestWizard.getId());
								return;
							}
						}
					}
					if (bestWizard != null)
						MyStrategy.move.setTurn(self.getAngleTo(bestWizard));

				}
			}
		}
	}

}
