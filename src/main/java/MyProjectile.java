import java.awt.Color;

import model.Projectile;

public class MyProjectile extends Projectile {

	private double endX;
	private double endY;
	private boolean isCertan;

	public MyProjectile(Projectile projectile, double endX, double endY, boolean isCertan) {
		super(projectile.getId(), projectile.getX(), projectile.getY(), projectile.getSpeedX(), projectile.getSpeedY(),
				projectile.getAngle(), projectile.getFaction(), projectile.getRadius(), projectile.getType(),
				projectile.getOwnerUnitId(), projectile.getOwnerPlayerId());

		assert (endX >= 0 && endX <= MyStrategy.game.getMapSize());
		assert (endY >= 0 && endY <= MyStrategy.game.getMapSize());
		this.endX = endX;
		this.endY = endY;
		this.isCertan = isCertan;
		if (MyStrategy.RENDER) {
			Color color;
			if (projectile.getFaction().equals(MyStrategy.self.getFaction()))
				color = new Color(0.0F, 1.0F, 0.0F);
			else
				color = new Color(1.0F, 0.0F, 0.0F);
			MyStrategy.visualClient.line(projectile.getX(), projectile.getY(), endX, endY, color);
		}

	}

	public double getEndX() {
		return endX;
	}

	public void setEndX(double endX) {
		this.endX = endX;
	}

	public double getEndY() {
		return endY;
	}

	public void setEndY(double endY) {
		this.endY = endY;
	}

	public boolean isCertan() {
		return isCertan;
	}

	public void setCertan(boolean isCertan) {
		this.isCertan = isCertan;
	}

	public static MyProjectile create(Projectile projectile, double distanceRemain, boolean isCertan) {
		double shiftX = distanceRemain * Math.cos(projectile.getAngle());
		double newX = projectile.getX() + shiftX;
		double shiftY = distanceRemain * Math.sin(projectile.getAngle());
		double newY = projectile.getY() + shiftY;
		if (newX <= 0) {
			double minusRate = Math.abs(newX / shiftX);
			newX = 0;
			newY -= shiftY * minusRate;
		}
		if (newX >= MyStrategy.game.getMapSize()) {
			double minusRate = Math.abs((newX - MyStrategy.game.getMapSize()) / shiftX);
			newX = MyStrategy.game.getMapSize();
			newY -= shiftY * minusRate;
		}
		if (newY <= 0) {
			double minusRate = Math.abs(newY / shiftY);
			newY = 0;
			newX -= shiftX * minusRate;
		}
		if (newY >= MyStrategy.game.getMapSize()) {
			double minusRate = Math.abs((newY - MyStrategy.game.getMapSize()) / shiftY);
			newY = MyStrategy.game.getMapSize();
			newX -= shiftX * minusRate;
		}

		// FIXME: corner
		return new MyProjectile(projectile, newX, newY, isCertan);
	}

}
