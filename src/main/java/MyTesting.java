import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.Arc2D;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.JFrame;
import javax.swing.JPanel;

import model.Minion;
import model.Projectile;
import model.ProjectileType;
import model.Tree;
import model.Wizard;

public class MyTesting {

	static double dx = 1000;
	static double dy = 1000;
	static double dmid = 1000;
	static double driver = 1000;

	public static boolean testRun() {

		boolean change = false;
		int mapSize = 4000;
		for (Tree tree : MyStrategy.world.getTrees()) {

			double x = tree.getX();
			double y = tree.getY();
			double delta;
			if (x < mapSize / 2)
				delta = x - 400;
			else
				delta = mapSize - x - 400;

			if (delta < dx - MyMagicNumbers.DELTA) {
				change = true;
				dx = delta;
			}

			if (y < mapSize / 2)
				delta = y - 400;
			else
				delta = mapSize - y - 400;

			if (delta < dy - MyMagicNumbers.DELTA) {
				change = true;
				dy = delta;
			}

			if (x + y < mapSize)
				delta = mapSize - x - y - 400;
			else
				delta = x + y - mapSize - 400;

			if (delta < dmid - MyMagicNumbers.DELTA) {
				change = true;
				dmid = delta;
			}

			if (x - y < 0)
				delta = -400 - x + y;
			else
				delta = x - y - 400;

			if (delta < driver - MyMagicNumbers.DELTA) {
				change = true;
				driver = delta;
			}

		}

		if (change) {
			System.out.println("x = " + dx);
			System.out.println("y  = " + dy);
			System.out.println("mid = " + dmid);
			System.out.println("river  = " + driver);
			System.out.println();
		}

		/*MyTesting.testCreepsMove();
		return;
		if (MyTesting.dodgeAttack())
			return true;
		testSelfMove();
		return true;*/

		/*Wizard self = MyStrategy.self;
		for (Wizard wizard : MyStrategy.world.getWizards()) {
			if (wizard.getFaction().equals(self.getFaction()))
				continue;
			if (wizard.getCastRange() > 510.0)
				wizard.toString();
		
		}*/

		/*
		if (self.getLevel() > 0) {
			if (self.getSkills().length < self.getLevel()) {
				if (self.getLevel() == 1)
					MyStrategy.move.setSkillToLearn(SkillType.RANGE_BONUS_PASSIVE_1);
				else
					MyStrategy.move.setSkillToLearn(SkillType.RANGE_BONUS_AURA_1);
		
				return true;
			}
			if (self.getLevel() > 1) {
				double castRangeSelf = self.getCastRange();
				for (Wizard wizard : MyStrategy.world.getWizards()) {
					if (!wizard.getFaction().equals(self.getFaction()))
						continue;
					double distance = MyUtils.getDistance(self, wizard);
					if (distance < MyStrategy.game.getAuraSkillRange()
							&& distance > MyStrategy.game.getWizardRadius()) {
						double castRange = wizard.getCastRange();
						wizard.toString();
					}
				}
			}
			return false;
		}*/

		return false;
	}

	@SuppressWarnings("unused")
	private static void testSelfMove() {
		double speed = -3.0;
		double strafe = Math.sqrt(7) * 3 / 4;
		Wizard self = MyStrategy.self;
		double speedPrev = Math.sqrt(self.getSpeedX() * self.getSpeedX() + self.getSpeedY() * self.getSpeedY());
		MyStrategy.move.setSpeed(speed);
		MyStrategy.move.setStrafeSpeed(strafe);
	}

	private static Set<CreepState> prevState = null;
	private static boolean test = true;

	private static class CreepState {
		final double x;
		final double y;
		final double angle;
		final int hp;
		final int cooldown;

		public CreepState(double x, double y, double angle, int hp, int cooldown) {
			super();
			this.x = x;
			this.y = y;
			this.angle = angle;
			this.hp = hp;
			this.cooldown = cooldown;
		}

		@Override
		public String toString() {
			return "CreepState [x=" + x + ", y=" + y + ", angle=" + angle + ", hp=" + hp + ", cooldown=" + cooldown
					+ "]";
		}

	}

	static double maxDistance = 0;
	static double maxAngle = 0;
	static double maxBadDistance = 0;
	static double maxBadAngle = 0;

	public static void testCreepsMove() {
		if (MyStrategy.world.getTickIndex() < 750)
			return;
		Set<CreepState> result = new LinkedHashSet<>();
		for (Minion minion : MyStrategy.world.getMinions()) {
			CreepState state = new CreepState(minion.getX(), minion.getY(), minion.getAngle(), minion.getLife(),
					minion.getRemainingActionCooldownTicks());
			result.add(state);

			if (prevState != null) {
				CreepState prev = null;
				for (CreepState candidate : prevState) {
					if (Math.abs(candidate.x - state.x) < 10) {
						if (Math.abs(candidate.y - state.y) < 10) {
							assert (prev == null);
							prev = candidate;
						}
					}
				}

				if (prev != null) {
					if (Math.abs(prev.x + minion.getSpeedX() - state.x) > MyMagicNumbers.DELTA) {
						System.out.println(prev.x + " " + minion.getSpeedX() + " " + state.x + " "
								+ (prev.x + minion.getSpeedX() - state.x));
						assert false;
					}
					if (Math.abs(prev.y + minion.getSpeedY() - state.y) > MyMagicNumbers.DELTA) {
						System.out.println(prev.y + " " + minion.getSpeedY() + " " + state.y + " "
								+ (prev.y + minion.getSpeedY() - state.y));
						assert false;
					}

					double x = state.x - prev.x;
					double y = state.y - prev.y;
					boolean move = Math.abs(x) > MyMagicNumbers.DELTA || Math.abs(y) > MyMagicNumbers.DELTA;
					double distance = Math.sqrt(x * x + y * y);

					if (test) {
						if (move) {
							double delta1 = Math.tan(prev.angle) * x - y;
							if (Math.abs(delta1) > MyMagicNumbers.DELTA) {
								System.out.println(x + " " + y + " " + prev.angle);
							}

							if (Math.abs(distance - 3.0) > MyMagicNumbers.DELTA) {
								System.out.println(distance);
							}

						}
						continue;
					}
					double angle = state.angle - prev.angle;
					int hp = state.hp - prev.hp;
					int cooldown = state.cooldown - prev.cooldown;

					if (move) {
						if (distance > maxDistance + MyMagicNumbers.DELTA) {
							System.out.println(" DISTANCE: " + distance);
							maxDistance = distance;
						}
					}

					double absAngle = Math.abs(angle);
					if (absAngle > Math.PI)
						absAngle = 2 * Math.PI - absAngle;
					boolean tunr = absAngle > MyMagicNumbers.DELTA;
					if (tunr) {
						if (absAngle > maxAngle + MyMagicNumbers.DELTA) {
							System.out.println(" ANGLE: " + absAngle);
							maxAngle = absAngle;
						}
					}

					boolean shoot = cooldown != 0 && cooldown != -1;

					if (hp != 0 || shoot || tunr || move) {

						if ((move && tunr) || (move && shoot) || (tunr && shoot)) {
							if (move && tunr) {
								if (distance > maxBadDistance + MyMagicNumbers.DELTA) {
									System.out.println(" BAD DISTANCE: " + distance);
									maxBadDistance = distance;
								}
								if (absAngle > maxBadAngle + MyMagicNumbers.DELTA) {
									System.out.println(" BAD ANGLE: " + absAngle);
									maxBadAngle = absAngle;
								}
							}

							// CreepState deltaState = new CreepState(x, y, angle, hp, cooldown);
							// System.out.println(deltaState);
						}
					}
				}

			}
		}
		prevState = result;

	}

	public static boolean dodgeAttack() {

		for (Projectile projectile : MyStrategy.world.getProjectiles()) {
			if (!projectile.getType().equals(ProjectileType.MAGIC_MISSILE))
				continue;
			Wizard self = MyStrategy.self;
			if (projectile.getOwnerPlayerId() == self.getId())
				continue;

			double distance = MyUtils.getDistance(projectile, self);
			if (distance > MyStrategy.self.getCastRange())
				continue;

			double angle = projectile.getAngleTo(self);

			if (Math.abs(angle) > Math.PI / 2)
				continue;

			if (Math.abs(distance * Math.sin(angle)) > self.getRadius() + projectile.getRadius())
				continue;

			MyStrategy.move.setSpeed(-MyUtils.getMinWizardSpeed(MyStrategy.self));
			double strafe = Math.sqrt(7) * 3 / 4;

			if (angle > 0) {
				MyStrategy.move.setStrafeSpeed(-strafe);
			} else {
				MyStrategy.move.setStrafeSpeed(-strafe);
			}
			return true;
		}

		return false;
	}

	public static void main(String[] args) {
		JFrame frame = new JFrame("Test");
		frame.setBounds(0, 0, 1000, 1000);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel contentPane = new JPanel() {
			Graphics2D g2;

			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				g2 = (Graphics2D) g;
				g2.setColor(Color.BLACK);
				drawPaths(true, true);
				g2.setColor(Color.RED);
				drawPaths(false, false);
				g2.setColor(Color.GREEN);
				int r = (int) (20 * 3.5 * 5);
				int shift = (int) (20 * 0.5 * 5);
				g2.drawOval(400 - r + shift, 500 - r, 2 * r, 2 * r);
				// g2.draw(new Arc2D.Double(0, -200, 800, 600, 0, -90, Arc2D.OPEN));
				// g2.draw(new Arc2D.Double(100, -200, 600, 600, -90, -90, Arc2D.OPEN));
				// g2.drawOval(0, -200, 800, 600);

				/*
				g2.setColor(Color.BLACK);
				g2.drawOval(200, 100, 600, 800);
				g2.drawLine(500, 100, 500, 900);
				g2.drawLine(100, 500, 900, 500);
				
				g2.setColor(Color.GREEN);
				double angle = (new Random()).nextDouble() * Math.PI / 2;
				g2.drawLine(500, 500, (int) (500 + 400 * Math.sin(angle)), (int) (500 - 400 * Math.cos(angle)));
				g2.setColor(Color.RED);
				
				double angle2 = MyFightMovingStrategy.getBestAngeleToDodge(angle);
				g2.drawLine(500, 500, (int) (500 + 400 * Math.sin(angle2)), (int) (500 - 400 * Math.cos(angle2)));
				*/

			}

			private void drawPaths(boolean optimal, boolean isTurn) {
				Map<Integer, List<MyDodgeMoveStrategy.Position>> positionsMap = MyDodgeMoveStrategy
						.calculatePositionsForDodge(optimal, isTurn);
				List<MyDodgeMoveStrategy.Position> prev = null;
				for (Entry<Integer, List<MyDodgeMoveStrategy.Position>> entry : positionsMap.entrySet()) {
					double max = 0;
					for (MyDodgeMoveStrategy.Position position : entry.getValue()) {
						double distance = MyUtils.getDistance(entry.getKey() * 0.5, 0, position.forward,
								position.strafe) - 3.5 * entry.getKey();
						if (distance > max)
							max = distance;
						if (prev == null) {
							g2.drawLine(400, 500, 400 + (int) (position.forward * 5),
									500 + (int) (position.strafe * 5));
						} else {
							for (MyDodgeMoveStrategy.Position prevPosition : prev) {
								if (Math.abs(prevPosition.angleToMove - position.angleToMove) < MyMagicNumbers.DELTA) {
									g2.drawLine(400 + (int) (prevPosition.forward * 5),
											500 + (int) (prevPosition.strafe * 5), 400 + (int) (position.forward * 5),
											500 + (int) (position.strafe * 5));
									break;
								}
							}
						}
					}
					if (optimal && isTurn)
						System.out.println(entry.getKey() + " : " + max);

					prev = entry.getValue();
				}
			}
		};
		frame.setContentPane(contentPane);
	}

	public static void drowRestricts(Set<MyLocalMovingStrategy.AngleRange> clusterRestricts,
			MyLocalMovingStrategy.AngleRange combine, double angle) {

		JFrame frame = new JFrame("Test");
		frame.setBounds(0, 0, 1200, 1200);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JPanel contentPane = new JPanel() {
			Graphics2D g2;

			protected void paintComponent(Graphics g) {
				super.paintComponent(g);
				g2 = (Graphics2D) g;

				double angleSelf = MyStrategy.self.getAngle();
				g2.setColor(Color.GREEN);
				g2.drawLine(600, 600, (int) (600 + 300 * Math.cos(angleSelf)), (int) (600 + 300 * Math.sin(angleSelf)));

				g2.setColor(Color.BLACK);
				g2.drawOval(599, 599, 2, 2);
				for (MyLocalMovingStrategy.AngleRange a : clusterRestricts) {
					double raduis = a.raduis / 2;
					double left = 180.0 * a.left / Math.PI;
					double right = 180.0 * a.right / Math.PI;

					double extent = left < right ? right - left : -360 + left - right;
					g2.draw(new Arc2D.Double(600 - raduis, 600 - raduis, raduis * 2, raduis * 2, -left, -extent,
							Arc2D.OPEN));
				}

				g2.setColor(Color.RED);
				g2.drawLine(600, 600, (int) (600 + 300 * Math.cos(angle)), (int) (600 + 300 * Math.sin(angle)));

				double raduis = 350;
				double left = 180.0 * combine.left / Math.PI;
				double right = 180.0 * combine.right / Math.PI;
				double extent = left < right ? right - left : -360 + left - right;
				g2.draw(new Arc2D.Double(600 - raduis, 600 - raduis, raduis * 2, raduis * 2, -left, -extent,
						Arc2D.OPEN));

			}
		};
		frame.setContentPane(contentPane);
	}

}
