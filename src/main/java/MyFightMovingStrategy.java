import java.awt.Color;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import model.ActionType;
import model.Building;
import model.BuildingType;
import model.LivingUnit;
import model.Minion;
import model.MinionType;
import model.ProjectileType;
import model.SkillType;
import model.Wizard;

public class MyFightMovingStrategy {

	public static boolean makeSafeMove() {
		Set<CircleOfDanger> circles = getCirlesOfDanger();
		double panicDangerLevel = MyMagicNumbers.DANGER_LEVEL_TO_PANIC;
		if (circles.isEmpty())
			return false;

		CircleOfDanger mostDangerous = getMostDangerousCircle(circles);
		if (mostDangerous == null)
			return false;
		double dangerLevel = getDangerLevel(mostDangerous);
		Wizard self = MyStrategy.self;
		double angleToDanger = self.getAngleTo(mostDangerous.x, mostDangerous.y);
		double angleSafe;
		MyDataPoint2D previousWaypoint = MyGlobalMoveStrategy.getPreviousWaypoint();
		double angleToWaypoint = self.getAngleTo(previousWaypoint.getX(), previousWaypoint.getY());
		double diff = MyUtils.normalizeAngle(angleToWaypoint - angleToDanger);

		if (dangerLevel < panicDangerLevel) {
			if (Math.abs(diff) < Math.PI / 2) {
				if (diff > 0)
					angleToWaypoint = MyUtils.normalizeAngle(angleToDanger + Math.PI / 2);
				else
					angleToWaypoint = MyUtils.normalizeAngle(angleToDanger - Math.PI / 2);
			}
			angleSafe = angleToWaypoint;

		} else {
			double panicDistance = getPanicDistance(mostDangerous);
			double distance = MyUtils.getDistance(self, mostDangerous.x, mostDangerous.y);
			assert panicDistance + MyMagicNumbers.DELTA > distance;
			double minMove = MyUtils.getMinWizardSpeed(self);
			double angleBack = MyUtils.normalizeAngle(Math.PI + angleToDanger);
			if (distance + minMove < panicDistance + MyMagicNumbers.DELTA) {
				angleSafe = angleBack;
			} else {
				double diffAngle = MyUtils.getAngleFromTriangle(panicDistance, distance, minMove);
				if (diffAngle < Math.abs(diff)) {
					angleSafe = angleToWaypoint;
				} else {
					if (diff > 0)
						angleSafe = MyUtils.normalizeAngle(angleToDanger + diffAngle);
					else
						angleSafe = MyUtils.normalizeAngle(angleToDanger - diffAngle);
				}
			}
		}
		MyLocalMovingStrategy.tryMoveToAngle(true, angleSafe, MyLocalMoveType.SAFE,
				MyMagicNumbers.DISTANCE_FOR_SAFE_MOVE);
		return true;
	}

	private static double getPanicDistance(CircleOfDanger mostDangerous) {
		return mostDangerous.radiusNotEscape + MyMagicNumbers.DANGER_LEVEL_TO_PANIC
				* (mostDangerous.radiusEasyEscape - mostDangerous.radiusNotEscape);
	}

	private static CircleOfDanger getMostDangerousCircle(Set<CircleOfDanger> circles) {
		CircleOfDanger result = circles.iterator().next();
		double levelResult = getDangerLevel(result);
		if (circles.size() > 1) {
			for (CircleOfDanger circleOfDanger : circles) {
				if (circleOfDanger.equals(result))
					continue;

				double level = getDangerLevel(circleOfDanger);

				if (level < MyMagicNumbers.DANGER_LEVEL_TO_CONSIDER)
					continue;

				if (levelResult < MyMagicNumbers.DANGER_LEVEL_TO_CONSIDER) {
					result = circleOfDanger;
					levelResult = level;
					continue;
				}

				if (level > MyMagicNumbers.DANGER_LEVEL_TO_PANIC
						&& levelResult < MyMagicNumbers.DANGER_LEVEL_TO_PANIC) {
					result = circleOfDanger;
					levelResult = level;
					continue;
				}
				if (level < MyMagicNumbers.DANGER_LEVEL_TO_PANIC && levelResult > MyMagicNumbers.DANGER_LEVEL_TO_PANIC)
					continue;

				if (result.damage < circleOfDanger.damage) {
					result = circleOfDanger;
					levelResult = level;
				} else if (result.damage == circleOfDanger.damage) {
					if (circleOfDanger.source instanceof Wizard && !(result.source instanceof Wizard)) {
						result = circleOfDanger;
						levelResult = level;
					} else if (level > 1.0 - MyMagicNumbers.DELTA && levelResult > 1.0 - MyMagicNumbers.DELTA) {
						double distanceResult = MyUtils.getDistance(MyStrategy.self, result.x, result.y);
						double distanceCircle = MyUtils.getDistance(MyStrategy.self, circleOfDanger.x,
								circleOfDanger.y);
						if (distanceCircle < distanceResult) {
							result = circleOfDanger;
							levelResult = level;
						}
					} else if (level > levelResult) {
						result = circleOfDanger;
						levelResult = level;
					}
				}

			}
		}

		if (levelResult < MyMagicNumbers.DANGER_LEVEL_TO_CONSIDER)
			return null;
		else
			return result;
	}

	private static double getDangerLevel(CircleOfDanger circleOfDanger) {
		double distance = MyStrategy.self.getDistanceTo(circleOfDanger.x, circleOfDanger.y);
		if (distance < circleOfDanger.radiusNotEscape)
			return 1.0;
		if (distance > circleOfDanger.radiusEasyEscape)
			return 0;
		return (circleOfDanger.radiusEasyEscape - distance)
				/ (circleOfDanger.radiusEasyEscape - circleOfDanger.radiusNotEscape + MyMagicNumbers.DELTA);
	}

	private static Set<CircleOfDanger> getCirlesOfDanger() {
		Set<CircleOfDanger> circles = new LinkedHashSet<>();
		boolean ignoreFetish = isIgnoreFetish();
		for (Minion minion : Arrays.asList(MyStrategy.world.getMinions())) {
			if (!MyUtils.isUnitEnemy(minion)) {
				continue;
			}

			if (minion.getType().equals(MinionType.ORC_WOODCUTTER)) {
				CircleOfDanger circleOfDanger = getOrcCirle(minion);
				if (circleOfDanger != null)
					circles.add(circleOfDanger);
			} else {
				if (ignoreFetish)
					continue;

				CircleOfDanger circleOfDanger = getFetishCirle(minion);
				if (circleOfDanger != null)
					circles.add(circleOfDanger);
			}

		}

		for (Wizard wizard : Arrays.asList(MyStrategy.world.getWizards())) {
			if (wizard.getFaction().equals(MyStrategy.self.getFaction()))
				continue;
			CircleOfDanger circleOfDanger = getWizardCirle(wizard, ProjectileType.MAGIC_MISSILE);
			if (circleOfDanger != null)
				circles.add(circleOfDanger);

			for (SkillType skill : wizard.getSkills()) {
				if (skill.equals(SkillType.FIREBALL)) {
					if (wizard.getMana() < MyStrategy.game.getFireballManacost()
							- MyStrategy.game.getMagicMissileManacost() * 2)
						continue;

					CircleOfDanger circleFireball = getWizardCirle(wizard, ProjectileType.FIREBALL);
					if (circleFireball != null)
						circles.add(circleFireball);
				}
			}

		}

		for (Building building : MyStrategy.world.getBuildingsSet()) {
			if (building.getFaction().equals(MyStrategy.self.getFaction()))
				continue;

			if (((double) MyStrategy.self.getLife() - building.getDamage())
					/ (double) MyStrategy.self.getMaxLife() > MyMagicNumbers.LIFE_LEVEL_FOR_BUILDING_ATTACK)
				continue;

			CircleOfDanger circleOfDanger = getBuildingCirle(building);
			if (circleOfDanger != null)
				circles.add(circleOfDanger);

		}

		if (MyStrategy.RENDER) {
			for (CircleOfDanger circleOfDanger : circles) {
				MyStrategy.visualClient.circle(circleOfDanger.x, circleOfDanger.y, circleOfDanger.radiusNotEscape,
						new Color(1.0F, 0.0F, 0.0F));
				MyStrategy.visualClient.circle(circleOfDanger.x, circleOfDanger.y, circleOfDanger.radiusEasyEscape,
						new Color(0.0F, 0.0F, 1.0F));
			}
		}

		return circles;
	}

	private static boolean isIgnoreFetish() {
		double hpLevel = MyMagicNumbers.HP_LEVEL_FOR_IGNORE_FETISH;
		if (MyStrategy.isFastPush != null && MyStrategy.isFastPush)
			hpLevel = MyMagicNumbers.HP_LEVEL_FOR_IGNORE_FETISH_FASTPUSH;

		Wizard self = MyStrategy.self;
		if (self.getLife() < self.getMaxLife() * hpLevel)
			return false;

		int staffCD = self.getRemainingCooldownTicksByAction()[ActionType.STAFF.ordinal()];
		int missileCD = self.getRemainingCooldownTicksByAction()[ActionType.MAGIC_MISSILE.ordinal()];
		if (staffCD < missileCD && missileCD > self.getRemainingActionCooldownTicks()) {
			double maxDistance = MyStrategy.game.getFetishBlowdartAttackRange() * 0.7;
			for (Minion minion : MyStrategy.world.getMinions()) {
				if (minion.getType().equals(MinionType.ORC_WOODCUTTER)
						&& (minion.getFaction().equals(self.getFaction()) || MyUtils.isUnitEnemy(minion))) {
					double distance = MyUtils.getDistance(self, minion);
					if (distance < maxDistance)
						return true;
				}

			}
		}

		return false;
	}

	private static CircleOfDanger getBuildingCirle(Building building) {

		double maxDistance;
		if (building.getType().equals(BuildingType.GUARDIAN_TOWER)) {
			maxDistance = MyStrategy.game.getGuardianTowerAttackRange();
		} else {
			maxDistance = MyStrategy.game.getFactionBaseAttackRange();
		}
		maxDistance += MyUtils.getMaxWizardSpeed(MyStrategy.self);
		double distance = MyUtils.getDistance(building, MyStrategy.self);
		if (MyStrategy.RENDER) {
			MyStrategy.visualClient.circle(building.getX(), building.getY(), maxDistance, new Color(0.0F, 1.0F, 0.0F));
		}
		if (distance - MyMagicNumbers.DELTA > maxDistance)
			return null;

		double cd = building.getRemainingActionCooldownTicks();

		double minDistance = MyStrategy.self.getRadius() + building.getRadius();
		double radiusEasyEscape = maxDistance
				- (cd - MyMagicNumbers.TICKS_FOR_TOWER_EASY_ESCAPE) * MyUtils.getMinWizardSpeed(MyStrategy.self);
		if (radiusEasyEscape < minDistance)
			return null;
		double radiusNotEscape = maxDistance - cd * MyUtils.getMinWizardSpeed(MyStrategy.self);
		if (radiusNotEscape < minDistance) {
			radiusNotEscape = minDistance;
		}
		return new CircleOfDanger(radiusNotEscape, radiusEasyEscape, building.getDamage(), building.getX(),
				building.getY(), building);
	}

	private static CircleOfDanger getWizardCirle(Wizard wizard, ProjectileType projectileType) {
		if (MyUtils.isPlayerCrashed(wizard))
			return null;

		if (MyStrategy.globalState.equals(MyGlobalState.GO_TO_RUNE)) {
			if (isIStrongerThen(wizard))
				return null;
		}

		Wizard self = MyStrategy.self;
		double distance = MyUtils.getDistance(wizard, self);
		boolean isClosestEnemy = isEnemyClosest(wizard, distance);
		double maxDistance = getMaxWizardDangerDistance(wizard, projectileType, isClosestEnemy);

		if (distance > maxDistance)
			return null;

		if (isClosestEnemy && canTakeRisk(wizard, maxDistance, distance)) {
			return null;
		}

		double cd = Math.max(wizard.getRemainingActionCooldownTicks(),
				wizard.getRemainingCooldownTicksByAction()[getActionOrdinalByProjectileType(projectileType)]);
		if (!isClosestEnemy
				|| (double) self.getLife() / (double) self.getMaxLife() < MyMagicNumbers.LIFE_LEVEL_FOR_WIZARD_ATTACK)
			cd *= 0.5;

		double radiusNotEscape = maxDistance - cd * MyUtils.getMinWizardSpeed(wizard);
		return new CircleOfDanger(radiusNotEscape, maxDistance, MyUtils.getProjetileDamage(projectileType),
				wizard.getX(), wizard.getY(), wizard);
	}

	private static int getActionOrdinalByProjectileType(ProjectileType type) {
		return ActionType.MAGIC_MISSILE.ordinal();
	}

	private static boolean isEnemyClosest(Wizard wizard, double distance) {

		for (Wizard enemy : MyStrategy.world.getWizards()) {
			if (!MyUtils.isUnitEnemy(enemy))
				continue;
			if (enemy.equals(wizard))
				continue;

			if (MyUtils.isPlayerCrashed(enemy))
				continue;
			double newDistance = MyUtils.getDistance(MyStrategy.self, enemy);
			if (newDistance < distance - MyMagicNumbers.DELTA)
				return false;
		}
		return true;
	}

	private static double getMaxWizardDangerDistance(Wizard wizard, ProjectileType projectileType,
			boolean isClosestEnemy) {
		double notDamage = wizard.getCastRange() + MyStrategy.game.getWizardRadius()
				+ MyUtils.getProjectileRadius(projectileType);
		if (isClosestEnemy)
			notDamage -= (wizard.getCastRange() / MyUtils.getProjetileSpeed(projectileType))
					* MyUtils.getMinWizardSpeed(MyStrategy.self) * 0.5;
		return notDamage;
	}

	private static boolean isIStrongerThen(Wizard wizard) {
		// TODO:improve
		return MyStrategy.self.getLife() >= wizard.getLife();
	}

	private static boolean canTakeRisk(Wizard wizard, double maxDistance, double distance) {
		Wizard self = MyStrategy.self;

		for (SkillType skill : wizard.getSkills()) {
			if (skill.equals(SkillType.FROST_BOLT)) {
				if (wizard.getMana() > MyStrategy.game.getFrostBoltManacost()
						- MyStrategy.game.getMagicMissileManacost() * 2)
					return false;

			}
		}

		if ((double) self.getLife() / (double) self.getMaxLife() < MyMagicNumbers.LIFE_LEVEL_FOR_WIZARD_ATTACK)
			return false;
		if (distance < getDistanceNotEscapeAttack(wizard))
			return false;

		if (canChase(wizard, ProjectileType.MAGIC_MISSILE))
			return false;

		if (MyAttackStrategy.getBestTargetFromBuildingList(MyStrategy.world.getBuildingsSet(),
				ProjectileType.MAGIC_MISSILE) != null)
			return false;

		int cdRangeSelf = Math.max(self.getRemainingActionCooldownTicks(),
				self.getRemainingCooldownTicksByAction()[ActionType.MAGIC_MISSILE.ordinal()]);
		int cdRangeEnemy = Math.max(wizard.getRemainingActionCooldownTicks(),
				wizard.getRemainingCooldownTicksByAction()[ActionType.MAGIC_MISSILE.ordinal()]);

		double maxRange = wizard.getCastRange() + MyStrategy.game.getMagicMissileRadius();
		for (Building building : MyStrategy.world.getBuildings()) {
			if (MyUtils.isUnitEnemy(building))
				continue;

			double distanceToBuilding = MyUtils.getDistance(wizard, building);
			if (distanceToBuilding - building.getRadius() < maxRange) {
				if (cdRangeEnemy < cdRangeSelf)
					wizard.toString();
				return true;
			}

		}
		if (cdRangeEnemy < cdRangeSelf)
			return false;

		return true;
	}

	private static double getDistanceNotEscapeAttack(Wizard wizard) {
		double escapeTime = 2.0 * (wizard.getRadius() + MyUtils.getProjectileRadius(ProjectileType.MAGIC_MISSILE))
				/ (MyUtils.getMaxWizardSpeed(wizard) + MyUtils.getMinWizardSpeed(wizard));
		double projectileDistance = MyUtils.getProjetileSpeed(ProjectileType.MAGIC_MISSILE) * escapeTime;
		double shift = escapeTime * (MyUtils.getMaxWizardSpeed(wizard) - MyUtils.getMinWizardSpeed(wizard)) * 0.5;
		double deltaAngle = wizard.getAngleTo(MyStrategy.self);
		double result = Math.sqrt(shift * shift + projectileDistance * projectileDistance
				- 2 * shift * projectileDistance * Math.cos(deltaAngle));
		return result;
	}

	private static boolean canChase(Wizard wizard, ProjectileType projectileType) {
		Wizard self = MyStrategy.self;
		double distance = MyUtils.getDistance(self, wizard);
		double angleToWizard = self.getAngleTo(wizard);

		int enemyCountNearWizard = 0;
		int teammateCountNearMe = 0;

		for (Wizard wizard2 : MyStrategy.world.getWizards()) {
			if (wizard.equals(wizard2) || wizard2.isMe())
				continue;

			if (MyUtils.isUnitEnemy(wizard2)) {
				double distance2 = MyUtils.getDistance(self, wizard2);
				double angle2 = self.getAngleTo(wizard2);

				if (MyUtils.normalizeAngle(angle2 - angleToWizard) > Math.PI / 3)
					continue;

				if (distance2 - wizard2.getCastRange() < MyMagicNumbers.DISTANCE_CLOSE_TO_ENEMY)
					enemyCountNearWizard++;
			} else {
				double distance2 = MyUtils.getDistance(wizard, wizard2);
				double angle2 = wizard.getAngleTo(wizard2);

				if (MyUtils.normalizeAngle(angle2 - wizard.getAngleTo(self)) > Math.PI / 3)
					continue;

				if (distance2 - wizard2.getCastRange() < MyMagicNumbers.DISTANCE_CLOSE_TO_ENEMY)
					teammateCountNearMe++;
			}
		}

		if (teammateCountNearMe > enemyCountNearWizard) {
			return false;
		}

		int minionCouner = 0;
		for (Minion minion : MyStrategy.world.getMinions()) {
			if (!minion.getFaction().equals(self.getFaction()))
				continue;

			double distanceToMinion = MyUtils.getDistance(self, minion);
			if (distanceToMinion > distance)
				continue;

			double angle = self.getAngleTo(minion);
			double angleWizardToMinion = MyUtils.normalizeAngle(angle - angleToWizard);
			if (Math.abs(angleWizardToMinion) > Math.PI / 2)
				continue;

			double strafeDistance = Math.cos(angleWizardToMinion) * distanceToMinion;
			if (strafeDistance > MyMagicNumbers.STRAFE_CREEP_DISTANCE_PREVENTED_CHASE)
				continue;

			minionCouner++;
		}

		if (minionCouner > 1)
			return false;

		if (wizard.getLife() + MyUtils.getProjetileDamage(projectileType) > self.getLife())
			return true;

		for (Wizard wizard2 : MyStrategy.world.getWizards()) {
			if (!wizard2.getFaction().equals(wizard.getFaction()))
				continue;

			if (wizard2.getId() == wizard.getId())
				continue;
			double distanceToSecondEnemy = MyUtils.getDistance(self, wizard2);
			double maxDistance = wizard.getCastRange() + MyStrategy.game.getWizardRadius()
					+ MyUtils.getProjectileRadius(projectileType) + MyUtils.getMaxWizardSpeed(wizard2);
			if (distanceToSecondEnemy < maxDistance)
				return true;
		}
		return false;
	}

	private static CircleOfDanger getFetishCirle(Minion minion) {
		double maxDistance = getFetishMaxDistance(minion);

		double distance = MyUtils.getDistance(minion, MyStrategy.self);

		if (distance > maxDistance)
			return null;

		double cd = minion.getRemainingActionCooldownTicks();

		double minDistance = MyStrategy.self.getRadius() + minion.getRadius();
		double radiusEasyEscape = maxDistance;
		if (radiusEasyEscape < minDistance)
			return null;
		double radiusNotEscape = maxDistance - cd * MyUtils.getMinWizardSpeed(MyStrategy.self);
		if (radiusNotEscape < minDistance) {
			radiusNotEscape = minDistance;
		}
		return new CircleOfDanger(radiusNotEscape, radiusEasyEscape, MyStrategy.game.getDartDirectDamage(),
				minion.getX(), minion.getY(), minion);
	}

	private static double getFetishMaxDistance(Minion minion) {
		double dodgeDistance = (MyStrategy.game.getFetishBlowdartAttackRange() / MyStrategy.game.getDartSpeed())
				* MyUtils.getMinWizardSpeed(MyStrategy.self) - MyStrategy.game.getDartRadius();
		double result = MyStrategy.game.getFetishBlowdartAttackRange() + MyUtils.getMaxWizardSpeed(MyStrategy.self)
				+ MyStrategy.self.getRadius() - dodgeDistance;

		for (Minion target : MyStrategy.world.getMinions()) {
			if (!MyUtils.isUnitEnemyTo(target, minion.getFaction()))
				continue;
			if (target.getLife() <= MyStrategy.game.getOrcWoodcutterDamage())
				continue;

			double distance = MyUtils.getDistance(minion, target) + MyUtils.getMaxWizardSpeed(MyStrategy.self)
					+ MyStrategy.self.getRadius() - target.getRadius();
			if (distance < result)
				result = distance;
		}

		return result;
	}

	private static CircleOfDanger getOrcCirle(Minion minion) {
		double radiusNotEscape = MyStrategy.game.getOrcWoodcutterAttackRange() + MyStrategy.self.getRadius()
				+ MyStrategy.game.getMinionSpeed();

		double radiusEasyEscape = MyStrategy.game.getStaffRange() + minion.getRadius()
				- MyUtils.getMinWizardSpeed(MyStrategy.self);

		if (MyUtils.getDistance(minion, MyStrategy.self) > radiusEasyEscape)
			return null;

		return new CircleOfDanger(radiusNotEscape, radiusEasyEscape, MyStrategy.game.getOrcWoodcutterDamage(),
				minion.getX(), minion.getY(), minion);
	}

	public static void makeMoveToFight() {
		List<LivingUnit> targets = new LinkedList<>();
		targets.addAll(Arrays.asList(MyStrategy.world.getMinions()));
		targets.addAll(Arrays.asList(MyStrategy.world.getWizards()));
		targets.addAll(MyStrategy.world.getBuildingsSet());

		LivingUnit closedUnit = null;
		Wizard self = MyStrategy.self;
		double minDistance = self.getCastRange();
		for (LivingUnit livingUnit : targets) {
			if (!MyUtils.isUnitEnemy(livingUnit)) {
				continue;
			}

			double distance = MyUtils.getDistance(self, livingUnit) - livingUnit.getRadius();

			if (distance < MyStrategy.game.getStaffRange() - MyUtils.getMaxWizardSpeed(self)) {
				if (!(livingUnit instanceof Building)
						|| !((Building) livingUnit).getType().equals(BuildingType.FACTION_BASE)) {
					return;
				}
			}
			if (distance < minDistance || closedUnit == null) {
				closedUnit = livingUnit;
				minDistance = distance;
			}

		}

		if (!(closedUnit instanceof Building) && minDistance > closedUnit.getRadius())
			minDistance += closedUnit.getRadius();// FIXME:magic

		if ((closedUnit instanceof Building) && ((Building) closedUnit).getType().equals(BuildingType.FACTION_BASE)) {
			MyLocalMovingStrategy.tryMoveToAngle(true, self.getAngleTo(3700, 300), MyLocalMoveType.ATTACK,
					MyUtils.getDistance(self, 3700, 300));
		} else {
			MyLocalMovingStrategy.tryMoveToAngle(true, self.getAngleTo(closedUnit), MyLocalMoveType.ATTACK,
					minDistance);
		}

	}

	private static class CircleOfDanger {
		/**
		 * radius with 100% chance get damage
		 */
		final double radiusNotEscape;
		/**
		 * radius in witch u can easy not get damage
		 */
		final double radiusEasyEscape;
		final int damage;
		final double x;
		final double y;
		final LivingUnit source;

		public CircleOfDanger(double radiusNotEscape, double radiusEasyEscape, int damage, double x, double y,
				LivingUnit source) {
			super();
			assert radiusNotEscape > 0;
			assert radiusEasyEscape > 0;
			assert radiusEasyEscape + MyMagicNumbers.DELTA > radiusNotEscape;
			this.radiusNotEscape = radiusNotEscape;
			this.radiusEasyEscape = radiusEasyEscape;
			this.damage = damage;
			this.x = x;
			this.y = y;
			this.source = source;
		}

		@Override
		public String toString() {
			return "CircleOfDanger [radiusNotEscape=" + radiusNotEscape + ", radiusEasyEscape=" + radiusEasyEscape
					+ ", damage=" + damage + ", x=" + x + ", y=" + y + "]";
		}

	}

}
