import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import model.ActionType;
import model.Building;
import model.Game;
import model.LivingUnit;
import model.Minion;
import model.MinionType;
import model.Move;
import model.Projectile;
import model.ProjectileType;
import model.Tree;
import model.Wizard;

public class MyAttackStrategy {

	public static boolean attackMove(LivingUnit bestTarget) {

		boolean isAttackMoveDone;
		boolean isTurnAllowed = true;
		Boolean makeDodgeMove = MyDodgeMoveStrategy.makeDodgeMove();
		if (makeDodgeMove != null) {
			if (makeDodgeMove) {
				isTurnAllowed = false;
			}
			isAttackMoveDone = true;
		} else {
			isAttackMoveDone = MyFightMovingStrategy.makeSafeMove();
		}

		if (!isAttackMoveDone)
			isAttackMoveDone = MyGlobalMoveStrategy.makeMoveToBaseDeff(bestTarget != null);

		if (bestTarget != null) {

			if (!isAttackMoveDone) {
				if (MyStrategy.globalState.equals(MyGlobalState.GO_TO_RUNE)) {
					MyLocalMovingStrategy.moveToWayPoint(MyGlobalMoveStrategy.getNextWaypoint(), true,
							MyLocalMoveType.WAYPOINT);
				} else {
					MyFightMovingStrategy.makeMoveToFight();
				}
			}

			isAttackMoveDone = true;

			if (isTurnAllowed) {
				MyStrategy.move.setTurn(getBestAngleToTurn(bestTarget));
			}

		}
		return isAttackMoveDone;
	}

	private static double getBestAngleToTurn(LivingUnit bestTarget) {
		Wizard self = MyStrategy.self;

		int cdAction = self.getRemainingActionCooldownTicks();

		int cdRange = self.getRemainingCooldownTicksByAction()[ActionType.MAGIC_MISSILE.ordinal()];
		if (bestTarget instanceof MyFifeballAttackStrategy.FireballTarget)
			cdRange = Math.min(cdRange, self.getRemainingCooldownTicksByAction()[ActionType.FIREBALL.ordinal()]);

		double angleToTarget = self.getAngleTo(bestTarget);
		if (Math.max(cdAction, cdRange) < 15)
			return angleToTarget;

		double distanceForStaff = MyUtils.getDistance(self, bestTarget) - MyStrategy.game.getStaffRange()
				- bestTarget.getRadius();

		if (distanceForStaff < 90)
			return angleToTarget;

		Wizard closest = null;
		double minDistance = 0;
		for (Wizard wizard : MyStrategy.world.getWizards()) {
			if (!MyUtils.isUnitEnemy(wizard))
				continue;
			double distance = MyUtils.getDistance(self, wizard);
			if (distance > wizard.getCastRange() + self.getRadius() + MyStrategy.game.getMagicMissileRadius())
				continue;

			if (closest == null || distance < minDistance) {
				closest = wizard;
				minDistance = distance;
			}
		}

		if (closest == null)
			return angleToTarget;

		if (bestTarget instanceof Wizard && bestTarget.getId() == closest.getId())
			if (angleToTarget > 0)
				return MyUtils.normalizeAngle(angleToTarget - Math.PI / 2);
			else
				return MyUtils.normalizeAngle(angleToTarget + Math.PI / 2);

		double angleToClosest = self.getAngleTo(closest);
		double diff = MyUtils.normalizeAngle(angleToTarget - angleToClosest);
		if (Math.abs(diff) > Math.PI / 2)
			return angleToTarget;

		if (diff > 0)
			return MyUtils.normalizeAngle(angleToClosest + Math.PI / 2);
		else
			return MyUtils.normalizeAngle(angleToClosest - Math.PI / 2);

	}

	private static void makeAttackAction(LivingUnit bestTarget) {
		Wizard self = MyStrategy.self;
		Game game = MyStrategy.game;
		Move move = MyStrategy.move;
		double angle = self.getAngleTo(bestTarget);
		if (self.getRemainingActionCooldownTicks() == 0) {
			Boolean staffNeeded = isStaffNeeded();
			boolean staffDone = false;
			if (self.getRemainingCooldownTicksByAction()[ActionType.STAFF.ordinal()] == 0) {
				if (self.getRemainingCooldownTicksByAction()[ActionType.STAFF.ordinal()] == 0 && staffNeeded != null) {
					if (staffNeeded || bestTarget instanceof Minion
							|| self.getMana() < MyStrategy.game.getMagicMissileManacost()) {
						move.setAction(ActionType.STAFF);
						staffDone = true;
					}
				}
			}
			if (!staffDone) {
				if (self.getRemainingCooldownTicksByAction()[ActionType.MAGIC_MISSILE.ordinal()] == 0) {
					if (StrictMath.abs(angle) < game.getStaffSector() / 2.0D) {

						move.setAction(ActionType.MAGIC_MISSILE);
						move.setCastAngle(angle);
						setMinCastRange(bestTarget, self, move, ProjectileType.MAGIC_MISSILE, angle);
						if (bestTarget instanceof Wizard) {
							if (dodgeMoveForBestTarget != null) {
								boolean isAngle = dodgeMoveForBestTarget.isAngle;
								MyDodgeEnemyStrategy.shootTo((Wizard) bestTarget, isAngle, move.getMinCastDistance(),
										ProjectileType.MAGIC_MISSILE);
							}
						}

					}
				} else if (self.getRemainingCooldownTicksByAction()[ActionType.STAFF.ordinal()] == 0) {
					if (staffNeeded != null) {
						move.setAction(ActionType.STAFF);
					}
				}
			}
		}
	}

	private static void setMinCastRange(LivingUnit bestTarget, Wizard self, Move move, ProjectileType projectileType,
			double castAngle) {
		double distance = MyUtils.getDistance(self, bestTarget);
		double minCastDistance;
		if (!(bestTarget instanceof Wizard)) {
			minCastDistance = distance - MyMagicNumbers.DELTA;
		} else {
			Wizard target = (Wizard) bestTarget;
			int ticks = (int) ((distance - MyMagicNumbers.DELTA) / MyUtils.getProjetileSpeed(projectileType)) + 1;// TODO:?
			double maxMove = MyUtils.getMaxWizardSpeed(target) * (double) ticks;
			double projectileRadius = MyUtils.getProjectileRadius(projectileType);
			minCastDistance = distance - maxMove + target.getRadius() + projectileRadius - MyMagicNumbers.DELTA;

			double maxMinionMove = MyStrategy.game.getMinionSpeed() * (double) ticks;
			for (Minion minion : MyStrategy.world.getMinions()) {
				double distanceToMinion = MyUtils.getDistance(self, minion);
				double dangerDistance = maxMinionMove + projectileRadius + minion.getRadius();
				if (distanceToMinion > distance || distanceToMinion < minCastDistance - dangerDistance)
					continue;

				double angle = MyUtils.normalizeAngle(self.getAngleTo(minion) - castAngle);
				if (Math.abs(angle) > Math.PI / 2)
					continue;

				double strafeDistance = Math.abs(Math.sin(angle)) * distanceToMinion;
				if (strafeDistance > dangerDistance - MyMagicNumbers.DELTA)
					continue;

				double forwardDistance = Math.abs(Math.cos(angle)) * distanceToMinion;
				double dangerForProjectile = forwardDistance
						+ Math.sqrt(dangerDistance * dangerDistance - strafeDistance * strafeDistance);
				if (!minion.getFaction().equals(self.getFaction())) {
					if (forwardDistance > distance - target.getRadius()) {
						dangerForProjectile = Math.min(dangerForProjectile,
								distance - target.getRadius() + projectileRadius + minion.getRadius());
					} else {
						dangerForProjectile = Math.min(dangerForProjectile,
								forwardDistance + projectileRadius + minion.getRadius());
					}
				}

				if (dangerForProjectile + MyMagicNumbers.DELTA > minCastDistance)
					minCastDistance = dangerForProjectile + MyMagicNumbers.DELTA;
			}
			if (MyStrategy.RENDER) {
				double angle = MyUtils.normalizeAngle(self.getAngle() + castAngle);
				double x = self.getX() + minCastDistance * Math.cos(angle);
				double y = self.getY() + minCastDistance * Math.sin(angle);
				MyStrategy.visualClient.circle(x, y, projectileRadius, new Color(0, 0, 1.0f));
			}

		}
		if (minCastDistance > self.getCastRange() - MyMagicNumbers.DELTA)
			minCastDistance = self.getCastRange() - MyMagicNumbers.DELTA;
		move.setMinCastDistance(minCastDistance);

	}

	/**
	 * 
	 * @return null - not needed<br>
	 *         true - need for tower or wizard<br>
	 *         false - need for minion
	 */
	private static Boolean isStaffNeeded() {
		List<LivingUnit> targets = new ArrayList<>();
		targets.addAll(MyStrategy.world.getBuildingsSet());
		targets.addAll(Arrays.asList(MyStrategy.world.getWizards()));
		targets.addAll(Arrays.asList(MyStrategy.world.getMinions()));

		for (LivingUnit target : targets) {
			if (!MyUtils.isUnitEnemy(target)) {
				continue;
			}
			double distance = MyUtils.getDistance(MyStrategy.self, target);
			if (distance < MyStrategy.game.getStaffRange() + target.getRadius()) {
				double angle = getBestAngleToTurn(target);
				if (Math.abs(angle) < MyStrategy.game.getStaffSector() / 2.0) {
					if (target instanceof Minion)
						return false;
					else
						return true;
				}
			}
		}

		return null;
	}

	public static LivingUnit getBestTarget() {

		Set<Building> buildingsList = MyStrategy.world.getBuildingsSet();
		List<Wizard> wizardsList = Arrays.asList(MyStrategy.world.getWizards());
		List<Minion> minionsList = Arrays.asList(MyStrategy.world.getMinions());

		LivingUnit fireballTarget = MyFifeballAttackStrategy.getFireballTarget(buildingsList, wizardsList, minionsList);
		if (fireballTarget != null) {
			MyFifeballAttackStrategy.makeFireballAction(fireballTarget);
			return fireballTarget;
		}

		LivingUnit staffTarget = getStaffTarget(buildingsList, wizardsList, minionsList);
		if (staffTarget != null) {
			makeAttackAction(staffTarget);
			return staffTarget;
		}

		LivingUnit magicMissileTarget = getMagicMissileTarget(buildingsList, wizardsList, minionsList);
		if (magicMissileTarget != null)
			makeAttackAction(magicMissileTarget);
		return magicMissileTarget;
	}

	private static LivingUnit getMagicMissileTarget(Set<Building> buildingsList, List<Wizard> wizardsList,
			List<Minion> minionsList) {
		LivingUnit building = MyAttackStrategy.getBestTargetFromBuildingList(buildingsList,
				ProjectileType.MAGIC_MISSILE);
		if (building != null)
			return building;

		LivingUnit bestTargetFromMinionList = MyAttackStrategy.getBestTargetFromMinionList(minionsList,
				ProjectileType.MAGIC_MISSILE);
		boolean isOnlyTarget = bestTargetFromMinionList == null;
		LivingUnit wizard = MyAttackStrategy.getBestTargetFromWizardList(wizardsList, ProjectileType.MAGIC_MISSILE,
				isOnlyTarget);
		if (wizard != null)
			return wizard;

		return bestTargetFromMinionList;
	}

	private static LivingUnit getStaffTarget(Set<Building> buildingsList, List<Wizard> wizardsList,
			List<Minion> minionsList) {
		Wizard self = MyStrategy.self;
		if (Math.max(self.getRemainingActionCooldownTicks(),
				self.getRemainingCooldownTicksByAction()[ActionType.STAFF.ordinal()]) < self
						.getRemainingCooldownTicksByAction()[ActionType.MAGIC_MISSILE.ordinal()]) {
			List<LivingUnit> targets = new ArrayList<>();
			targets.addAll(wizardsList);
			targets.addAll(buildingsList);
			targets.addAll(minionsList);

			for (LivingUnit target : targets) {
				if (!MyUtils.isUnitEnemy(target)) {
					continue;
				}
				double distance = MyUtils.getDistance(MyStrategy.self, target);
				if (distance < MyStrategy.game.getStaffRange() + target.getRadius()) {
					return target;
				}
			}
		}
		return null;
	}

	private static MyDodgeMoveStrategy.DodgeMove dodgeMoveForBestTarget = null;

	private static LivingUnit getBestTargetFromWizardList(List<Wizard> wizardsList, ProjectileType projectileType,
			boolean isOnlyTarget) {
		LivingUnit bestTarget = null;
		int lowestTargetHP = 100000;
		boolean bestTargetUsefull = false;
		Wizard self = MyStrategy.self;

		for (Wizard target : wizardsList) {
			if (!MyUtils.isUnitEnemy(target)) {
				continue;
			}

			double distance = MyUtils.getDistance(self, target);

			double projetileSpeed = MyUtils.getProjetileSpeed(projectileType);
			int ticks = (int) ((distance - MyMagicNumbers.DELTA) / projetileSpeed) + 1;
			double delta = (MyUtils.getMaxWizardSpeed(target) - MyUtils.getMinWizardSpeed(target)) * (double) ticks;

			if (distance - delta > self.getCastRange())
				continue;

			Wizard newTarget = getShiftedWizard(target, delta);
			double distanceToNew = MyUtils.getDistance(self, newTarget);
			if (distanceToNew > self.getCastRange())
				continue;

			double angleProjectile = MyUtils.normalizeAngle(self.getAngle() + self.getAngleTo(newTarget));

			MyProjectile newProjectile = getProjectileFrom(self, angleProjectile, projectileType);

			double lastPositionDistance = Math.min((double) ticks * projetileSpeed, self.getCastRange());
			double prevLastPositionDistance = (double) (ticks - 1) * projetileSpeed;
			MyDodgeMoveStrategy.DodgeMove dodgeMove = MyDodgeMoveStrategy.getBestDodgeMove(newProjectile,
					lastPositionDistance, prevLastPositionDistance, ticks, target);

			boolean isShootUsefull = isShootUsefull(target, dodgeMove);
			if (bestTarget != null) {
				if (bestTargetUsefull && !isShootUsefull)
					continue;
				if (bestTargetUsefull || !isShootUsefull) {
					if (lowestTargetHP < target.getLife())
						continue;
				}
			}
			lowestTargetHP = target.getLife();
			bestTarget = newTarget;
			dodgeMoveForBestTarget = dodgeMove;
			bestTargetUsefull = isShootUsefull;
		}
		if (!isOnlyTarget && !bestTargetUsefull)
			return null;

		return bestTarget;
	}

	private static boolean isShootUsefull(Wizard target, MyDodgeMoveStrategy.DodgeMove dodgeMove) {
		if (dodgeMove == null)
			return true;
		return !MyDodgeEnemyStrategy.isWizardWillEvade(target, dodgeMove.isAngle);
	}

	private static MyProjectile getProjectileFrom(Wizard self, double angleProjectile, ProjectileType projectileType) {
		Projectile projectile = new Projectile(-1, self.getX(), self.getY(), self.getSpeedX(), self.getSpeedY(),
				angleProjectile, self.getFaction(), MyUtils.getProjectileRadius(projectileType), projectileType,
				self.getId(), self.getId());

		MyProjectile myProjectile = MyProjectile.create(projectile, self.getCastRange(), true);

		return myProjectile;
	}

	private static Wizard getShiftedWizard(Wizard target, double delta) {

		double newX = target.getX() + delta * Math.cos(target.getAngle());
		double newY = target.getY() + delta * Math.sin(target.getAngle());

		return new Wizard(target.getId(), newX, newY, target.getSpeedX(), target.getSpeedY(), target.getAngle(),
				target.getFaction(), target.getRadius(), target.getLife(), target.getMaxLife(), target.getStatuses(),
				target.getOwnerPlayerId(), target.isMe(), target.getMana(), target.getMaxMana(),
				target.getVisionRange(), target.getCastRange(), target.getXp(), target.getLevel(), target.getSkills(),
				target.getRemainingActionCooldownTicks(), target.getRemainingCooldownTicksByAction(), target.isMaster(),
				target.getMessages());
	}

	public static LivingUnit getBestTargetFromBuildingList(Set<Building> buildingsList, ProjectileType projectileType) {
		LivingUnit bestTarget = null;
		int lowestTargetHP = 100000;

		for (Building target : buildingsList) {
			if (!MyUtils.isUnitEnemy(target)) {
				continue;
			}

			double distance = MyUtils.getDistance(MyStrategy.self, target);
			double range = MyStrategy.self.getCastRange() + target.getRadius()
					+ MyUtils.getProjectileRadius(projectileType);
			if (distance < range - MyMagicNumbers.DELTA) {
				if (!MyUtils.isFirstOnLane(target))
					continue;
				if (target.getLife() < lowestTargetHP) {
					bestTarget = target;
					lowestTargetHP = target.getLife();
				}
			}
		}

		return bestTarget;
	}

	static LivingUnit getBestTargetFromMinionList(List<Minion> targets, ProjectileType projectileType) {
		Minion bestTarget = null;
		int lowestTargetHP = 100000;

		double shift = (MyStrategy.self.getCastRange() / MyUtils.getProjetileSpeed(projectileType))
				* (MyStrategy.game.getMinionSpeed() / 2);
		for (Minion target : targets) {
			if (!MyUtils.isUnitEnemy(target)) {
				continue;
			}

			double distance = MyUtils.getDistance(MyStrategy.self, target);

			if (distance - shift > MyStrategy.self.getCastRange())
				continue;
			double newX = target.getX() + shift * Math.cos(target.getAngle());
			double newY = target.getY() + shift * Math.sin(target.getAngle());

			if (MyStrategy.RENDER) {
				MyStrategy.visualClient.circle(newX, newY, MyUtils.getProjectileRadius(projectileType),
						new Color(0.5F, 0.0F, 0.0F));

			}
			Minion newMinion = new Minion(target.getId(), newX, newY, target.getSpeedX(), target.getSpeedY(),
					target.getAngle(), target.getFaction(), target.getRadius(), target.getLife(), target.getMaxLife(),
					target.getStatuses(), target.getType(), target.getVisionRange(), target.getDamage(),
					target.getCooldownTicks(), target.getRemainingActionCooldownTicks());

			distance = MyUtils.getDistance(MyStrategy.self, newMinion);
			if (distance < MyStrategy.self.getCastRange() - MyMagicNumbers.DELTA) {
				if (bestTarget == null) {
					bestTarget = newMinion;
					lowestTargetHP = newMinion.getLife();
				} else if (newMinion.getType().equals(MinionType.FETISH_BLOWDART)
						&& !bestTarget.getType().equals(MinionType.FETISH_BLOWDART)) {
					bestTarget = newMinion;
					lowestTargetHP = newMinion.getLife();
				} else if (!newMinion.getType().equals(MinionType.FETISH_BLOWDART)
						&& bestTarget.getType().equals(MinionType.FETISH_BLOWDART)) {
					continue;
				} else if (newMinion.getLife() < lowestTargetHP) {
					bestTarget = newMinion;
					lowestTargetHP = newMinion.getLife();
				}
			}
		}

		return bestTarget;
	}

	public static void attackTree() {
		Wizard self = MyStrategy.self;

		if (self.getRemainingActionCooldownTicks() == 0) {
			if (self.getRemainingCooldownTicksByAction()[ActionType.STAFF.ordinal()] == 0)
				if (attackTreeByStaff())
					return;
			if (isEnemyNear())
				return;

			if (self.getRemainingCooldownTicksByAction()[ActionType.MAGIC_MISSILE.ordinal()] == 0
					&& self.getMana() == self.getMaxMana()) {
				attackTreeByMissile();
			}
		}

	}

	private static boolean isEnemyNear() {
		Set<Building> buildingsList = MyStrategy.world.getBuildingsSet();
		List<Wizard> wizardsList = Arrays.asList(MyStrategy.world.getWizards());
		List<Minion> minionsList = Arrays.asList(MyStrategy.world.getMinions());
		List<LivingUnit> targets = new ArrayList<>();
		targets.addAll(wizardsList);
		targets.addAll(buildingsList);
		targets.addAll(minionsList);

		Wizard self = MyStrategy.self;
		double minDistance = self.getCastRange()
				+ MyUtils.getMaxWizardSpeed(self) * MyStrategy.game.getMagicMissileCooldownTicks();
		for (LivingUnit unit : targets) {
			if (!MyUtils.isUnitEnemy(unit))
				continue;
			if (MyUtils.getDistance(self, unit) < minDistance)
				return true;
		}

		return false;
	}

	private static boolean attackTreeByMissile() {
		Wizard self = MyStrategy.self;
		for (Tree tree : MyStrategy.world.getTrees()) {
			double distance = MyUtils.getDistance(self, tree);
			double radius = MyStrategy.game.getMagicMissileRadius() + tree.getRadius() - MyMagicNumbers.DELTA;
			double castRange = self.getCastRange();
			if (distance > castRange + radius)
				continue;

			double deltaAngle = Math.asin(radius / distance);
			double angle = self.getAngleTo(tree);
			double sectorAngle = MyStrategy.game.getStaffSector() / 2.0;
			if (Math.abs(angle) > deltaAngle + sectorAngle)
				continue;

			if (Math.abs(angle) > sectorAngle && distance > castRange)
				continue;

			MyStrategy.move.setAction(ActionType.MAGIC_MISSILE);
			MyStrategy.move.setCastAngle(angle);
			MyStrategy.move.setMinCastDistance(castRange + MyMagicNumbers.DELTA);
			return true;

		}
		return false;
	}

	private static boolean attackTreeByStaff() {
		Wizard self = MyStrategy.self;
		for (Tree tree : MyStrategy.world.getTrees()) {
			if (MyUtils.getDistance(self, tree) < MyStrategy.game.getStaffRange() + tree.getRadius()
					- MyMagicNumbers.DELTA) {
				if (Math.abs(self.getAngleTo(tree)) < MyStrategy.game.getStaffSector() / 2.0) {
					MyStrategy.move.setAction(ActionType.STAFF);
					return true;
				}
			}
		}
		return false;
	}

}
