import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import model.ActionType;
import model.Building;
import model.Faction;
import model.Game;
import model.LivingUnit;
import model.Minion;
import model.Move;
import model.ProjectileType;
import model.SkillType;
import model.Status;
import model.Wizard;

public class MyFifeballAttackStrategy {

	static LivingUnit getFireballTarget(Set<Building> buildingsList, List<Wizard> wizardsList,
			List<Minion> minionsList) {

		boolean isHaveFireball = false;
		Wizard self = MyStrategy.self;
		for (SkillType a : self.getSkills()) {
			if (a.equals(SkillType.FIREBALL))
				isHaveFireball = true;
		}
		if (!isHaveFireball)
			return null;

		int cdFireball = self.getRemainingCooldownTicksByAction()[ActionType.FIREBALL.ordinal()];
		if (cdFireball >= self.getRemainingActionCooldownTicks() + MyStrategy.game.getWizardActionCooldownTicks())
			return null;

		Set<FireballTargetCircle> buildingsCircles = getBuildingCircles(buildingsList);
		Set<FireballTargetCircle> wizardCircles = getWizardCircles(wizardsList);
		Set<FireballTargetCircle> minionsCircles = getMinionCircles(minionsList);
		Set<FireballTargetCircle> importantTargets = new LinkedHashSet<>(buildingsCircles);
		importantTargets.addAll(wizardCircles);
		Set<Point> possiblePoints;
		if (!importantTargets.isEmpty()) {
			possiblePoints = getPossiblePoints(importantTargets);
		} else {
			possiblePoints = getPossiblePoints(minionsCircles);
		}

		Point bestPoint = getBestPoint(possiblePoints, importantTargets, minionsCircles);
		if (bestPoint == null)
			return null;
		return FireballTarget.create(bestPoint);
	}

	private static Point getBestPoint(Set<Point> possiblePoints, Set<FireballTargetCircle> importantTargets,
			Set<FireballTargetCircle> minionsCircles) {
		Point best = null;
		int maxImportant = 0;
		int maxMinions = 0;
		for (Point point : possiblePoints) {
			int imortant = 0;
			int minion = 0;
			for (FireballTargetCircle circle : importantTargets) {
				if (MyUtils.getDistance(circle.x, circle.y, point.x, point.y) < circle.radius)
					imortant++;
			}
			for (FireballTargetCircle circle : minionsCircles) {
				if (MyUtils.getDistance(circle.x, circle.y, point.x, point.y) < circle.radius)
					minion++;
			}

			if (best == null || imortant > maxImportant || (imortant == maxImportant && minion > maxMinions)) {
				best = point;
				maxImportant = imortant;
				maxMinions = minion;
			}
		}
		if (maxImportant + maxMinions == 0)
			return null;
		if (maxImportant == 0 && maxMinions == 1)
			return null;
		if (maxImportant == 1 && maxMinions == 0)
			if (MyStrategy.self.getMana() < MyStrategy.self.getMaxMana() / 2.0)
				return null;
		return best;
	}

	private static Set<Point> getPossiblePoints(Set<FireballTargetCircle> importantTargets) {
		Wizard self = MyStrategy.self;
		FireballTargetCircle selfCircle = new FireballTargetCircle(self.getX(), self.getY(), self.getCastRange(), self);
		Set<Point> result = new LinkedHashSet<>();
		for (FireballTargetCircle target : importantTargets) {
			result.add(new Point(target.x, target.y));
			Set<Point> pointsForTwoCirles = getPointsForTwoCircles(target, selfCircle);
			result.addAll(pointsForTwoCirles);
			for (FireballTargetCircle second : importantTargets) {
				if (second.equals(target))
					continue;
				Set<Point> points = getPointsForTwoCircles(target, second);
				if (points != null)
					result.addAll(points);
			}
		}
		removeBadPoints(result);
		return result;
	}

	private static void removeBadPoints(Set<Point> result) {
		Iterator<Point> iterator = result.iterator();
		Wizard self = MyStrategy.self;
		loop: while (iterator.hasNext()) {
			Point point = iterator.next();
			double distance = MyUtils.getDistance(self.getX(), self.getY(), point.x, point.y);
			if (distance > self.getCastRange() + MyMagicNumbers.DELTA) {
				iterator.remove();
				continue loop;
			}
			double range = MyStrategy.game.getFireballExplosionMinDamageRange()
					+ MyUtils.getMaxWizardSpeed(self) * (distance / MyUtils.getProjetileSpeed(ProjectileType.FIREBALL));
			if (distance < self.getRadius() + range) {
				iterator.remove();
				continue loop;
			}

			for (Wizard wizard : MyStrategy.world.getWizards()) {
				if (wizard.isMe() || !wizard.getFaction().equals(self.getFaction()))
					continue;

				double distanceFromWizard = MyUtils.getDistance(wizard, point.x, point.y);
				if (distanceFromWizard < wizard.getRadius() + range) {
					iterator.remove();
					continue loop;
				}
			}

		}

	}

	private static Set<Point> getPointsForTwoCircles(FireballTargetCircle first, FireballTargetCircle second) {
		double distance = MyUtils.getDistance(first.x, first.y, second.x, second.y);
		if (distance > first.radius + second.radius)
			return null;

		double deltaX = second.x - first.x;
		double deltaY = second.y - first.y;
		if (Math.abs(deltaX) < MyMagicNumbers.DELTA && Math.abs(deltaY) < MyMagicNumbers.DELTA)
			return null;

		if (distance + first.radius <= second.radius || distance + second.radius <= first.radius) {
			FireballTargetCircle big;
			FireballTargetCircle small;
			if (distance + first.radius < second.radius) {
				big = second;
				small = first;
			} else {
				big = first;
				small = second;
			}
			deltaX = big.x - small.x;
			deltaY = big.y - small.y;
			Point point1 = new Point(small.x + deltaX * small.radius / distance,
					small.y + deltaY * small.radius / distance);
			Point point2 = new Point(small.x - deltaX * small.radius / distance,
					small.y - deltaY * small.radius / distance);

			Set<Point> result = new LinkedHashSet<>();
			result.add(point1);
			result.add(point2);
			return result;
		}

		Point point1 = new Point(first.x + deltaX * first.radius / distance,
				first.y + deltaY * first.radius / distance);
		Point point2 = new Point(second.x - deltaX * second.radius / distance,
				first.y - deltaY * second.radius / distance);
		Point point3 = new Point((point1.x + point2.x) / 2, (point1.y + point2.y) / 2);

		double cosAngle = (distance * distance + first.radius * first.radius - second.radius * second.radius)
				/ (2.0 * distance * first.radius);
		assert Math.abs(cosAngle) <= 1.0 : cosAngle + " : " + distance + " : " + first.radius + " : " + second.radius;
		double deltaAngle = Math.acos(cosAngle);
		double angleFromFirstToSecond = Math.atan2(deltaY, deltaX);
		double x4 = first.x + Math.cos(MyUtils.normalizeAngle(angleFromFirstToSecond + deltaAngle)) * first.radius;
		double y4 = first.y + Math.sin(MyUtils.normalizeAngle(angleFromFirstToSecond + deltaAngle)) * first.radius;
		Point point4 = new Point(x4, y4);
		assert Math.abs(MyUtils.getDistance(x4, y4, second.x, second.y) - second.radius) < MyMagicNumbers.DELTA;

		double x5 = first.x + Math.cos(MyUtils.normalizeAngle(angleFromFirstToSecond - deltaAngle)) * first.radius;
		double y5 = first.y + Math.sin(MyUtils.normalizeAngle(angleFromFirstToSecond - deltaAngle)) * first.radius;
		Point point5 = new Point(x5, y5);
		assert Math.abs(MyUtils.getDistance(x5, y5, second.x, second.y) - second.radius) < MyMagicNumbers.DELTA;

		Set<Point> result = new LinkedHashSet<>();
		result.add(point1);
		result.add(point2);
		result.add(point3);
		result.add(point4);
		result.add(point5);

		return result;
	}

	private static Set<FireballTargetCircle> getMinionCircles(List<Minion> minionsList) {
		Wizard self = MyStrategy.self;
		Set<FireballTargetCircle> result = new LinkedHashSet<>();
		for (Minion building : minionsList) {
			if (!MyUtils.isUnitEnemy(building))
				continue;
			double distance = MyUtils.getDistance(self, building);
			if (distance > building.getRadius() + self.getCastRange()
					+ MyStrategy.game.getFireballExplosionMinDamageRange() + MyMagicNumbers.DELTA)
				continue;

			FireballTargetCircle circle = getCircleForMinion(building);
			if (MyUtils.getDistance(self, circle.x, circle.y) < self.getCastRange() + circle.radius)
				result.add(circle);

		}
		return result;
	}

	private static Set<FireballTargetCircle> getWizardCircles(List<Wizard> wizardsList) {
		Wizard self = MyStrategy.self;
		Set<FireballTargetCircle> result = new LinkedHashSet<>();
		for (Wizard building : wizardsList) {
			if (!MyUtils.isUnitEnemy(building))
				continue;
			double distance = MyUtils.getDistance(self, building);
			if (distance > building.getRadius() + self.getCastRange()
					+ MyStrategy.game.getFireballExplosionMinDamageRange() + MyMagicNumbers.DELTA)
				continue;

			FireballTargetCircle circle = getCircleForWizard(building);
			if (MyUtils.getDistance(self, circle.x, circle.y) < self.getCastRange() + circle.radius)
				result.add(circle);

		}
		return result;
	}

	private static Set<FireballTargetCircle> getBuildingCircles(Set<Building> buildingsList) {
		Wizard self = MyStrategy.self;
		Set<FireballTargetCircle> result = new LinkedHashSet<>();
		for (Building building : buildingsList) {
			if (!MyUtils.isUnitEnemy(building))
				continue;

			if (!MyUtils.isFirstOnLane(building))
				continue;

			double distance = MyUtils.getDistance(self, building);
			if (distance > building.getRadius() + self.getCastRange()
					+ MyStrategy.game.getFireballExplosionMinDamageRange() + MyMagicNumbers.DELTA)
				continue;

			FireballTargetCircle circle = getCircleForBuilding(building);
			if (MyUtils.getDistance(self, circle.x, circle.y) < self.getCastRange() + circle.radius)
				result.add(circle);

		}
		return result;
	}

	private static FireballTargetCircle getCircleForWizard(Wizard wizard) {
		double distance = MyUtils.getDistance(MyStrategy.self, wizard);
		double time = distance / MyUtils.getProjetileSpeed(ProjectileType.FIREBALL);
		double shift = time * (MyUtils.getMaxWizardSpeed(wizard) - MyUtils.getMinWizardSpeed(wizard)) / 2.0;
		double newX = wizard.getX() + shift * Math.cos(wizard.getAngle());
		double newY = wizard.getY() + shift * Math.sin(wizard.getAngle());
		double radiusMoving = time * (MyUtils.getMaxWizardSpeed(wizard) + MyUtils.getMinWizardSpeed(wizard)) / 2.0;
		double delta = time < 11 ? 0.2 : (time - 10) * 0.24;
		radiusMoving += delta;

		double radius = wizard.getRadius() + MyStrategy.game.getFireballExplosionMinDamageRange() - radiusMoving;

		return new FireballTargetCircle(newX, newY, radius - MyMagicNumbers.DELTA, wizard);
	}

	private static FireballTargetCircle getCircleForMinion(Minion minion) {
		double distance = MyUtils.getDistance(MyStrategy.self, minion);
		double time = distance / MyUtils.getProjetileSpeed(ProjectileType.FIREBALL);
		double radiusMoving = time * MyStrategy.game.getMinionSpeed() / 2.0;
		double newX = minion.getX() + radiusMoving * Math.cos(minion.getAngle());
		double newY = minion.getY() + radiusMoving * Math.sin(minion.getAngle());
		double radius = minion.getRadius() + MyStrategy.game.getFireballExplosionMinDamageRange() - radiusMoving;

		return new FireballTargetCircle(newX, newY, radius - MyMagicNumbers.DELTA, minion);
	}

	private static FireballTargetCircle getCircleForBuilding(Building building) {
		return new FireballTargetCircle(building.getX(), building.getY(),
				MyStrategy.game.getFireballExplosionMaxDamageRange() + building.getRadius() - MyMagicNumbers.DELTA,
				building);
	}

	static void makeFireballAction(LivingUnit fireballTarget) {
		Wizard self = MyStrategy.self;
		Game game = MyStrategy.game;
		double angle = self.getAngleTo(fireballTarget);
		if (self.getRemainingActionCooldownTicks() == 0) {
			if (self.getRemainingCooldownTicksByAction()[ActionType.FIREBALL.ordinal()] == 0) {
				if (self.getMana() >= game.getFireballManacost()) {
					if (Math.abs(angle) < game.getStaffSector() / 2.0) {
						Move move = MyStrategy.move;
						move.setAction(ActionType.FIREBALL);
						move.setCastAngle(angle);
						double distance = MyUtils.getDistance(self, fireballTarget);
						move.setMinCastDistance(distance - MyMagicNumbers.DELTA);
						move.setMaxCastDistance(distance);
					}
				}
			}
		}

	}

	private static class FireballTargetCircle {
		final double x;
		final double y;
		final double radius;
		@SuppressWarnings("unused")
		final LivingUnit target;

		public FireballTargetCircle(double x, double y, double radius, LivingUnit target) {
			super();
			assert radius > 0;
			this.x = x;
			this.y = y;
			this.radius = radius;
			this.target = target;
		}

	}

	private static class Point {
		final double x;
		final double y;

		public Point(double x, double y) {
			super();
			this.x = x;
			this.y = y;
		}

		@Override
		public int hashCode() {
			int result = 1;
			int intx = (int) (x * 100.0);
			int inty = (int) (y * 100.0);
			result += 31 * intx;
			result = result * 7 + 37 * inty;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Point other = (Point) obj;
			if (Math.abs(x - other.x) > MyMagicNumbers.DELTA)
				return false;
			if (Math.abs(y - other.y) > MyMagicNumbers.DELTA)
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "Point [x=" + x + ", y=" + y + "]";
		}
	}

	public static class FireballTarget extends LivingUnit {

		protected FireballTarget(long id, double x, double y, double speedX, double speedY, double angle,
				Faction faction, double radius, int life, int maxLife, Status[] statuses) {
			super(id, x, y, speedX, speedY, angle, faction, radius, life, maxLife, statuses);
		}

		public static FireballTarget create(Point point) {
			return new FireballTarget(-1, point.x, point.y, 0, 0, 0, Faction.OTHER, 5, 5, 5, new Status[] {});
		}
	}

}
