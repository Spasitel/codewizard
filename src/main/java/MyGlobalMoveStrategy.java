import java.awt.Color;
import java.util.EnumMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import model.Bonus;
import model.Faction;
import model.LaneType;
import model.LivingUnit;
import model.Minion;
import model.Player;
import model.Status;
import model.StatusType;
import model.Unit;
import model.Wizard;

public class MyGlobalMoveStrategy {

	private static final Map<LaneType, MyDataPoint2D[]> waypointsByLine = new EnumMap<>(LaneType.class);
	private static MyDataPoint2D[] waypoints = null;
	private static LaneType lane = null;
	private static final Map<RuneType, PointForRune[]> pointsForRune = new EnumMap<>(RuneType.class);
	private static Map<Integer, LaneType> lanesOfWizards = new LinkedHashMap<>();
	/**
	 * Shows that runes still on map or not
	 */
	public static Map<RuneType, Boolean> isRuneGot = new EnumMap<>(RuneType.class);
	private static RuneType rune = null;

	private static MyDataPoint2D nextWaypiont = null;
	private static MyDataPoint2D prevWaypiont = null;
	private static int nextWayPointSet = 0;
	private static int prevWayPointSet = 0;

	static {
		double mapSize = 4000.0;

		MyGlobalMoveStrategy.waypointsByLine.put(LaneType.MIDDLE,
				new MyDataPoint2D[] { new MyDataPoint2D(800.0D, mapSize - 800.0D),
						new MyDataPoint2D(1800.0D, mapSize - 1800.0D), new MyDataPoint2D(2200.0D, mapSize - 2200.0D),
						new MyDataPoint2D(3000.0D, mapSize - 3000.0D), new MyDataPoint2D(3725.0D, mapSize - 3725.0D) });

		MyGlobalMoveStrategy.waypointsByLine.put(LaneType.TOP,
				new MyDataPoint2D[] { new MyDataPoint2D(350.0D, 3200.0D), new MyDataPoint2D(265.0D, 1667.0D),
						new MyDataPoint2D(350.0D, 800.0D), new MyDataPoint2D(400.0D, 400.0D),
						new MyDataPoint2D(800.0D, 300.0D), new MyDataPoint2D(1500.0D, 300.0D),
						new MyDataPoint2D(2200.0D, 300.0D), new MyDataPoint2D(2800.0D, 300.0D),
						new MyDataPoint2D(3725.0D, 275.0D) });

		MyGlobalMoveStrategy.waypointsByLine.put(LaneType.BOTTOM,
				new MyDataPoint2D[] { new MyDataPoint2D(800.0D, 3650.0D), new MyDataPoint2D(1370.0D, 3735.0D),
						new MyDataPoint2D(3200.0D, 3650.0D), new MyDataPoint2D(3600.0D, 3600.0D),
						new MyDataPoint2D(3700.0D, 3200.0D), new MyDataPoint2D(3700.0D, 2500.0D),
						new MyDataPoint2D(3700.0D, 1800.0D), new MyDataPoint2D(3700.0D, 1100.0D),
						new MyDataPoint2D(3725.0D, 275.0D) });

		pointsForRune.put(RuneType.TOP,
				new PointForRune[] { new PointForRune(LaneType.TOP, false, new MyDataPoint2D(450, 800)),
						new PointForRune(LaneType.TOP, true, new MyDataPoint2D(800, 450)),
						new PointForRune(LaneType.MIDDLE, false, new MyDataPoint2D(1600, 1950)),
						new PointForRune(LaneType.MIDDLE, true, new MyDataPoint2D(1950, 1600)) });

		pointsForRune.put(RuneType.BOT,
				new PointForRune[] {
						new PointForRune(LaneType.BOTTOM, true, new MyDataPoint2D(mapSize - 450, mapSize - 800)),
						new PointForRune(LaneType.BOTTOM, false, new MyDataPoint2D(mapSize - 800, mapSize - 450)),
						new PointForRune(LaneType.MIDDLE, true, new MyDataPoint2D(mapSize - 1600, mapSize - 1950)),
						new PointForRune(LaneType.MIDDLE, false, new MyDataPoint2D(mapSize - 1950, mapSize - 1600)) });

	}

	public static void updateGlobalState(LivingUnit bestTarget) {

		switch (MyStrategy.globalState) {
		case GO_TO_LANE:
			if (needGoToRune(bestTarget))
				MyStrategy.globalState = MyGlobalState.GO_TO_RUNE;
			else if (bestTarget != null)
				MyStrategy.globalState = MyGlobalState.LANE;

			break;
		case LANE:
			if (needGoToRune(bestTarget))
				MyStrategy.globalState = MyGlobalState.GO_TO_RUNE;
			else if (bestTarget == null)
				MyStrategy.globalState = MyGlobalState.GO_TO_LANE;
			break;
		case GO_TO_RUNE:
			if (!needGoToRune(bestTarget)) {
				if (bestTarget == null)
					MyStrategy.globalState = MyGlobalState.GO_TO_LANE;
				else
					MyStrategy.globalState = MyGlobalState.LANE;
			}
			break;

		default:
			assert false;
			break;
		}

	}

	public static void makeGlobalStrategy(LivingUnit bestTarget) {
		setLanesOfWizards();
		updateRuneState();

		updateGlobalState(bestTarget);

		waypoints = chooseWaipointsToMove();

		if (MyStrategy.RENDER) {
			MyDataPoint2D prev = null;
			for (MyDataPoint2D waypoint : MyGlobalMoveStrategy.waypoints) {
				if (prev != null)
					MyStrategy.visualClient.line(prev.getX(), prev.getY(), waypoint.getX(), waypoint.getY(),
							new Color(0, 0.75f, 0));
				prev = waypoint;
			}
		}

	}

	public static MyDataPoint2D getNextWaypoint() {
		MyDataPoint2D newNextWaypoint = getNewNextWaypoint(true);
		if (MyStrategy.finalMode && LaneType.MIDDLE.equals(lane)
				&& !MyStrategy.globalState.equals(MyGlobalState.GO_TO_RUNE)) {
			newNextWaypoint = getNextMiddleWaypoint(newNextWaypoint);
		}
		if (nextWaypiont != null
				&& MyStrategy.world.getTickIndex() - nextWayPointSet < MyMagicNumbers.DELAY_TO_CHANGE_WAYPOINTS) {
			if (!newNextWaypoint.equals(nextWaypiont))
				newNextWaypoint.toString();
			return nextWaypiont;
		} else {
			if (!newNextWaypoint.equals(nextWaypiont)) {
				nextWaypiont = newNextWaypoint;
				nextWayPointSet = MyStrategy.world.getTickIndex();
			}
			return newNextWaypoint;
		}
	}

	private static MyDataPoint2D getNewNextWaypoint(boolean isNeedToWathchEnemy) {
		Wizard self = MyStrategy.self;
		if (MyStrategy.globalState.equals(MyGlobalState.GO_TO_RUNE)) {
			if (isOnExtraRiver(self))
				return waypoints[waypoints.length - 1];
			return waypoints[0];
		}
		for (int waypointIndex = 0; waypointIndex < MyGlobalMoveStrategy.waypoints.length - 1; ++waypointIndex) {
			MyDataPoint2D waypoint = MyGlobalMoveStrategy.waypoints[waypointIndex];
			if (MyUtils.getDistance(self, waypoint.getX(), waypoint.getY()) < MyMagicNumbers.WAYPOINT_RADIUS) {
				return MyGlobalMoveStrategy.waypoints[waypointIndex + 1];
			}
		}
		LaneType laneByPossition = getLaneByPossition(self);
		if (isNeedToWathchEnemy && laneByPossition != null && laneByPossition.equals(lane)) {
			double selfPositionOnLane = getPositionOnLane(self, laneByPossition, true);
			double positionOnLane = getPositionOnLane(laneByPossition);
			if (positionOnLane > -1 && positionOnLane > selfPositionOnLane + 0.2)// FIXME
				return getNewPreviousWaypoint();
		}
		switch (MyGlobalMoveStrategy.lane) {
		case MIDDLE:
			if (isOnBase(self))
				return MyGlobalMoveStrategy.waypoints[0];
			if (isOnRiver(self)) {
				double positionOnLane = getPositionOnLane(LaneType.MIDDLE);
				if (positionOnLane > -1 && positionOnLane > 0.5)
					return MyGlobalMoveStrategy.waypoints[1];
				else
					return MyGlobalMoveStrategy.waypoints[2];
			}

			if (!LaneType.MIDDLE.equals(laneByPossition)) {
				MyDataPoint2D closest = null;
				double minDistance = 0;
				for (MyDataPoint2D point2d : MyGlobalMoveStrategy.waypoints) {
					double distance = MyUtils.getDistance(self, point2d.getX(), point2d.getY());
					if (closest == null || minDistance > distance) {
						closest = point2d;
						minDistance = distance;
					}
				}
				return closest;
			}
			for (MyDataPoint2D waypoint : MyGlobalMoveStrategy.waypoints) {
				if (self.getX() - self.getY() < waypoint.getX() - waypoint.getY())
					return waypoint;
			}
			return MyGlobalMoveStrategy.waypoints[MyGlobalMoveStrategy.waypoints.length - 1];
		case TOP:
			if (isOnBase(self))
				return MyGlobalMoveStrategy.waypoints[0];
			if (isOnRiver(self)) {
				double positionOnLane = getPositionOnLane(LaneType.TOP);
				if (positionOnLane > -1 && positionOnLane > 0.5)
					return MyGlobalMoveStrategy.waypoints[2];
				else
					return MyGlobalMoveStrategy.waypoints[4];
			}
			if (!LaneType.TOP.equals(laneByPossition)) {
				MyDataPoint2D closest = null;
				double minDistance = 0;
				for (MyDataPoint2D point2d : MyGlobalMoveStrategy.waypoints) {
					double distance = MyUtils.getDistance(self, point2d.getX(), point2d.getY());
					if (closest == null || minDistance > distance) {
						closest = point2d;
						minDistance = distance;
					}
				}
				return closest;
			}
			if (self.getY() > MyGlobalMoveStrategy.waypoints[2].getY()) {
				for (int waypointIndex = 0; waypointIndex < 2; ++waypointIndex) {
					MyDataPoint2D waypoint = MyGlobalMoveStrategy.waypoints[waypointIndex];
					if (self.getY() > waypoint.getY()) {
						return waypoint;
					}
				}
				return MyGlobalMoveStrategy.waypoints[2];
			} else {
				for (int waypointIndex = 3; waypointIndex < MyGlobalMoveStrategy.waypoints.length; ++waypointIndex) {
					MyDataPoint2D waypoint = MyGlobalMoveStrategy.waypoints[waypointIndex];
					if (self.getX() < waypoint.getX()) {
						return waypoint;
					}
				}
				return MyGlobalMoveStrategy.waypoints[MyGlobalMoveStrategy.waypoints.length - 1];
			}
		case BOTTOM:
			if (isOnBase(self))
				return MyGlobalMoveStrategy.waypoints[0];
			if (isOnRiver(self)) {
				double positionOnLane = getPositionOnLane(LaneType.BOTTOM);
				if (positionOnLane > -1 && positionOnLane > 0.5)
					return MyGlobalMoveStrategy.waypoints[2];
				else
					return MyGlobalMoveStrategy.waypoints[4];
			}
			if (!LaneType.BOTTOM.equals(laneByPossition)) {
				MyDataPoint2D closest = null;
				double minDistance = 0;
				for (MyDataPoint2D point2d : MyGlobalMoveStrategy.waypoints) {
					double distance = MyUtils.getDistance(self, point2d.getX(), point2d.getY());
					if (closest == null || minDistance > distance) {
						closest = point2d;
						minDistance = distance;
					}
				}
				return closest;
			}

			if (self.getX() < MyGlobalMoveStrategy.waypoints[2].getX()) {
				for (int waypointIndex = 0; waypointIndex < 2; ++waypointIndex) {
					MyDataPoint2D waypoint = MyGlobalMoveStrategy.waypoints[waypointIndex];
					if (self.getX() < waypoint.getX()) {
						return waypoint;
					}
				}
				return MyGlobalMoveStrategy.waypoints[2];
			} else {
				for (int waypointIndex = 3; waypointIndex < MyGlobalMoveStrategy.waypoints.length; ++waypointIndex) {
					MyDataPoint2D waypoint = MyGlobalMoveStrategy.waypoints[waypointIndex];
					if (self.getY() > waypoint.getY()) {
						return waypoint;
					}
				}
				return MyGlobalMoveStrategy.waypoints[MyGlobalMoveStrategy.waypoints.length - 1];
			}
		default:
			assert false;
			return null;
		}
	}

	private static MyDataPoint2D getNextMiddleWaypoint(MyDataPoint2D nextMiddleWaypoint) {
		Wizard self = MyStrategy.self;
		int teammatesOnMiddle = 0;
		int myNumber = 0;
		int startId = 1;
		if (self.getId() > 5)
			startId = 6;
		for (int i = startId; i < startId + 5; i++) {
			if (lanesOfWizards.containsKey(i) && lanesOfWizards.get(i).equals(LaneType.MIDDLE)) {
				teammatesOnMiddle++;
				if (i < self.getId())
					myNumber++;
			}
		}

		if (teammatesOnMiddle > 0) {

			int mul = myNumber * 2 > teammatesOnMiddle ? (myNumber + 1) * 2 - teammatesOnMiddle - 1
					: myNumber * 2 - teammatesOnMiddle - 1;
			double shift = MyMagicNumbers.DELTA_WAYPOINTS_MIDDLE * 0.5 * (double) mul;
			return new MyDataPoint2D(nextMiddleWaypoint.getX() + shift, nextMiddleWaypoint.getY() + shift);
		}

		return nextMiddleWaypoint;

	}

	public static MyDataPoint2D getPreviousWaypoint() {
		MyDataPoint2D newPrevWaypoint = getNewPreviousWaypoint();
		if (prevWaypiont != null
				&& MyStrategy.world.getTickIndex() - prevWayPointSet < MyMagicNumbers.DELAY_TO_CHANGE_WAYPOINTS) {
			if (!newPrevWaypoint.equals(prevWaypiont))
				newPrevWaypoint.toString();
			return prevWaypiont;
		} else {
			if (!newPrevWaypoint.equals(prevWaypiont)) {
				prevWaypiont = newPrevWaypoint;
				prevWayPointSet = MyStrategy.world.getTickIndex();
			}
			return newPrevWaypoint;
		}

	}

	private static MyDataPoint2D getNewPreviousWaypoint() {
		if (MyStrategy.globalState.equals(MyGlobalState.GO_TO_RUNE)) {
			if (isOnExtraRiver(MyStrategy.self))
				return waypoints[waypoints.length - 1];
			return waypoints[0];
		}

		if (isOnRiver(MyStrategy.self) && !LaneType.MIDDLE.equals(MyGlobalMoveStrategy.lane))
			return MyGlobalMoveStrategy.waypoints[2];
		MyDataPoint2D firstWaypoint = MyGlobalMoveStrategy.waypoints[0];
		MyDataPoint2D nextWaypoint = getNewNextWaypoint(false);

		for (int waypointIndex = MyGlobalMoveStrategy.waypoints.length - 1; waypointIndex > 0; --waypointIndex) {
			if (MyGlobalMoveStrategy.waypoints[waypointIndex].equals(nextWaypoint))
				return MyGlobalMoveStrategy.waypoints[waypointIndex - 1];
		}

		return firstWaypoint;
	}

	public static LaneType getLaneByPossition(Unit wizard) {
		double x = wizard.getX();
		double y = wizard.getY();
		double mapSize = MyStrategy.game.getMapSize();
		double tileSize = mapSize / 10.0;
		double delta = MyMagicNumbers.DELTA_LINE_IN_FOREST;

		if (isOnBase(wizard))
			return null;
		if (isOnEnemyBase(wizard))
			return null;

		if (x < tileSize + delta)
			return LaneType.TOP;
		if (y < tileSize + delta)
			return LaneType.TOP;
		if (x < 2 * tileSize && y < 2 * tileSize && x + y < 3 * tileSize + delta)
			return LaneType.TOP;

		if (x > mapSize - tileSize - delta)
			return LaneType.BOTTOM;
		if (y > mapSize - tileSize - delta)
			return LaneType.BOTTOM;
		if (x > mapSize - 2 * tileSize && y > mapSize - 2 * tileSize && x + y > mapSize * 2 - 3 * tileSize - delta)
			return LaneType.BOTTOM;

		if (x + y > mapSize - tileSize - delta && x + y < mapSize + tileSize + delta)
			return LaneType.MIDDLE;

		return null;
	}

	private static MyDataPoint2D[] chooseWaipointsToMove() {
		Wizard self = MyStrategy.self;
		if (MyStrategy.globalState.equals(MyGlobalState.GO_TO_LANE)
				|| MyStrategy.globalState.equals(MyGlobalState.LANE)) {

			if (lane == null) {
				if (MyStrategy.finalMode) {
					chooseStartLanes(self);
				} else {
					lane = getBestLineToMove();
				}
			}
			changeLineIfNeeded();

			return MyGlobalMoveStrategy.waypointsByLine.get(MyGlobalMoveStrategy.lane);
		} else {
			return getRuneWaypoints(rune, self);
		}
	}

	private static void chooseStartLanes(Wizard self) {
		String enemyName = "";
		for (Player pl : MyStrategy.world.getPlayers()) {
			if (!pl.getFaction().equals(MyStrategy.self.getFaction())) {
				enemyName = pl.getName();
				break;
			}
		}

		if (enemyName.equals("Antmsu") || enemyName.equals("Commandos") || enemyName.equals("ud1")
				|| enemyName.equals("NighTurs") || enemyName.equals("WildCat") || enemyName.equals("mustang")
				|| enemyName.equals("mortido")) {
			choose140(self);
		} else if (enemyName.equals("r2d2") || enemyName.equals("GreenTea") || enemyName.equals("tyamgin")) {
			choose122(self);
		} else

			choose131(self);
	}

	private static void choose131(Wizard self) {
		switch ((int) (self.getId() % 5)) {
		case 1:
			MyGlobalMoveStrategy.lane = LaneType.TOP;
			break;
		case 2:
		case 3:
		case 4:
			MyGlobalMoveStrategy.lane = LaneType.MIDDLE;
			break;
		case 0:
			MyGlobalMoveStrategy.lane = LaneType.BOTTOM;
			assert testFastPush();
			break;

		default:
			break;
		}
	}

	private static void choose140(Wizard self) {
		switch ((int) (self.getId() % 5)) {
		case 1:
		case 2:
		case 3:
		case 4:
			MyGlobalMoveStrategy.lane = LaneType.MIDDLE;
			break;
		case 0:
			MyGlobalMoveStrategy.lane = LaneType.BOTTOM;
			break;

		default:
			break;
		}
	}

	private static void choose122(Wizard self) {
		switch ((int) (self.getId() % 5)) {
		case 1:
		case 2:
			MyGlobalMoveStrategy.lane = LaneType.TOP;
			break;
		case 3:
		case 4:
			MyGlobalMoveStrategy.lane = LaneType.MIDDLE;
			break;
		case 0:
			MyGlobalMoveStrategy.lane = LaneType.BOTTOM;
			break;

		default:
			break;
		}
	}

	private static void changeLineIfNeeded() {
		if (MyStrategy.isFastPush != null && MyStrategy.isFastPush)
			return;
		Wizard self = MyStrategy.self;
		Set<LaneType> types = new LinkedHashSet<>();
		boolean notStart = MyStrategy.world.getTickIndex() >= 500;
		if ((isOnBase(self) && notStart) || (isCloseToBase() && (!MyStrategy.finalMode || notStart))) {
			types.add(LaneType.MIDDLE);
			types.add(LaneType.TOP);
			types.add(LaneType.BOTTOM);
		} else {
			if (MyUtils.getDistance(self, RuneType.BOT.waypoint.getX(),
					RuneType.BOT.waypoint.getY()) < MyMagicNumbers.DISTANCE_TO_CHANGE_LANE_ON_RUNE) {
				types.add(LaneType.MIDDLE);
				types.add(LaneType.BOTTOM);
			} else if (MyUtils.getDistance(self, RuneType.TOP.waypoint.getX(),
					RuneType.TOP.waypoint.getY()) < MyMagicNumbers.DISTANCE_TO_CHANGE_LANE_ON_RUNE) {
				types.add(LaneType.MIDDLE);
				types.add(LaneType.TOP);
			} else {
				return;
			}
		}
		if (types.isEmpty())
			return;
		lane = getBestLaneFromList(types);
	}

	private static boolean isCloseToBase() {
		Wizard self = MyStrategy.self;
		LaneType laneType = getLaneByPossition(self);
		if (laneType == null)
			return false;
		if (!laneType.equals(lane))
			return false;

		double lanePosition = getPositionOnLane(self, laneType, true);
		if (lanePosition > 0.85)
			return true;
		return false;
	}

	private static LaneType getBestLaneFromList(Set<LaneType> types) {
		LaneType best = null;
		double bestScore = 0;
		for (LaneType laneType : types) {
			double score = getScoreForLane(laneType);
			if (best == null || score > bestScore) {
				best = laneType;
				bestScore = score;
			}
		}
		return best;
	}

	private static double getScoreForLane(LaneType laneType) {

		int enemyCount = 0;
		int teammateCount = 0;
		long id = MyStrategy.self.getId();
		int sumEnemy = 0;
		for (Entry<Integer, LaneType> entry : lanesOfWizards.entrySet()) {
			int key = entry.getKey();
			if (key == id)
				continue;
			boolean enemy = (key <= 5 && id > 5) || (key > 5 && id <= 5);
			if (enemy)
				sumEnemy++;
			if (entry.getValue().equals(laneType))
				if (enemy)
					enemyCount++;
				else
					teammateCount++;
		}

		if (enemyCount - teammateCount > 1)
			return 1000;

		if (teammateCount > enemyCount && sumEnemy == 5)
			return 0;

		int closeMinionCount = 0;
		for (Minion minion : MyStrategy.world.getMinions()) {
			if (!MyUtils.isUnitEnemy(minion))
				continue;

			if (!laneType.equals(getLaneByPossition(minion)))
				continue;

			double distance = MyUtils.getDistance(minion, 0, MyStrategy.game.getMapSize());
			if (distance < MyMagicNumbers.DISTANCE_CLOSE_TO_BASE)
				closeMinionCount++;

		}

		double positionOnLane = getPositionOnLane(laneType);
		double result = positionOnLane > -1 ? 10 * positionOnLane : 0;
		if (closeMinionCount > 0)
			return result + closeMinionCount * 50;
		if (teammateCount == 0)
			result += 75;
		else if (teammateCount == 1)
			result += 25;

		if (teammateCount < enemyCount)
			result += 10;

		return result;
	}

	/**
	 * 1 - on my base, 0 - on enemy base
	 * 
	 * @param closest
	 * @param laneType
	 * @param b
	 * @return
	 */
	private static double getPositionOnLane(LaneType laneType) {
		if (MyStrategy.world.getTickIndex() < 1200)
			return 0.5;
		double minDistance = 0;
		Minion closest = null;
		double maxDistanceTeammate = 0;
		Minion closestTeammate = null;
		for (Minion minion : MyStrategy.world.getMinions()) {

			if (!laneType.equals(getLaneByPossition(minion)))
				continue;

			double distance = MyUtils.getDistance(minion, 0, MyStrategy.game.getMapSize());
			if (MyUtils.isUnitEnemy(minion) && !minion.getFaction().equals(Faction.NEUTRAL)) {
				if (closest == null || distance < minDistance) {
					minDistance = distance;
					closest = minion;
				}
			} else if (MyStrategy.self.getFaction().equals(minion.getFaction())) {
				if (closestTeammate == null || distance > maxDistanceTeammate) {
					maxDistanceTeammate = distance;
					closestTeammate = minion;
				}
			}
		}

		if (closest != null)
			return getPositionOnLane(closest, laneType, true);

		if (closestTeammate != null)
			return getPositionOnLane(closestTeammate, laneType, false);

		return -5;
	}

	/**
	 * 1 - on my base, 0 - on enemy base
	 * 
	 * @param closest
	 * @param laneType
	 * @param b
	 * @return
	 */
	public static double getPositionOnLane(LivingUnit closest, LaneType laneType, boolean isEnemy) {
		double mapSize = MyStrategy.game.getMapSize();
		if (LaneType.MIDDLE.equals(laneType)) {
			double delta = 600;
			double distance = MyUtils.getDistance(closest, mapSize - delta, delta);
			double maxDistance = MyUtils.getDistance(delta, mapSize - delta, mapSize - delta, delta);

			if (!isEnemy) {
				distance -= MyStrategy.game.getMinionVisionRange();
				distance = distance < 0 ? 0 : distance;
			}

			double result = distance / maxDistance;
			if (result > 1.0)
				return 1.0;
			else
				return result;
		} else {
			double midX;
			double midY;
			if (LaneType.TOP.equals(laneType)) {
				midX = 400.0;
				midY = 400.0;
			} else {
				midX = mapSize - 400.0;
				midY = mapSize - 400.0;
			}

			double maxDistance = MyUtils.getDistance(400, 400, 200, 3200);
			double distance = MyUtils.getDistance(closest, midX, midY);
			double delta = distance / maxDistance;
			if (delta > 1.0)
				delta = 1.0;
			delta *= 0.5;
			double result;
			if (closest.getX() - closest.getY() > 0)
				result = 0.5 - delta;
			else
				result = 0.5 + delta;

			if (!isEnemy)
				result -= MyStrategy.game.getMinionVisionRange() * 0.5 / maxDistance;
			result = result < 0 ? 0 : result;
			return result;

		}
	}

	private static boolean testFastPush() {
		// lane = LaneType.MIDDLE;
		return true;
	}

	private static LaneType getBestLineToMove() {

		if (MyStrategy.world.getTickIndex() < 500) {
			return LaneType.MIDDLE;
		}
		LaneInfo infoTop = new LaneInfo(LaneType.TOP, 0, 0);
		LaneInfo infoBot = new LaneInfo(LaneType.BOTTOM, 0, 0);
		LaneInfo infoMid = new LaneInfo(LaneType.MIDDLE, 0, 0);

		Set<LaneInfo> infos = new LinkedHashSet<>();
		infos.add(infoMid);
		infos.add(infoTop);
		infos.add(infoBot);

		for (Entry<Integer, LaneType> entry : lanesOfWizards.entrySet()) {
			int key = entry.getKey();
			long id = MyStrategy.self.getId();
			boolean enemy = (key <= 5 && id > 5) || (key > 5 && id <= 5);
			switch (entry.getValue()) {
			case TOP:
				if (!enemy) {
					infoTop.teammates++;
					if (infoTop.teammates > 1)
						infos.remove(infoTop);
				} else {
					infoTop.enemy++;
				}
				break;
			case MIDDLE:
				if (!enemy) {
					infoMid.teammates++;
					if (infoMid.teammates > 1)
						infos.remove(infoMid);
				} else {
					infoMid.enemy++;
				}
				break;
			case BOTTOM:
				if (!enemy) {
					infoBot.teammates++;
					if (infoBot.teammates > 1)
						infos.remove(infoBot);
				} else {
					infoBot.enemy++;
				}
				break;
			}
		}

		assert (infos.size() > 0);
		if (infos.size() == 1)
			return infos.iterator().next().type;

		// min teammates
		int minTeammates = 3;
		for (LaneInfo info : infos) {
			if (info.teammates < minTeammates)
				minTeammates = info.teammates;
		}
		Iterator<LaneInfo> iterator = infos.iterator();
		while (iterator.hasNext()) {
			LaneInfo info = iterator.next();
			if (info.teammates > minTeammates)
				iterator.remove();
		}
		assert (infos.size() > 0);
		if (infos.size() == 1)
			return infos.iterator().next().type;

		// max enemy
		int maxEnemy = 0;
		for (LaneInfo info : infos) {
			if (info.enemy > maxEnemy)
				maxEnemy = info.enemy;
		}
		iterator = infos.iterator();
		while (iterator.hasNext()) {
			LaneInfo info = iterator.next();
			if (info.enemy < maxEnemy)
				iterator.remove();
		}
		assert (infos.size() > 0);
		// TODO:listen messages, problem lane
		return infos.iterator().next().type;

	}

	private static void setLanesOfWizards() {
		for (Wizard wizard : MyStrategy.world.getWizards()) {
			if (!wizard.isMe()) {
				LaneType type = getLaneByPossition(wizard);
				if (type != null)
					lanesOfWizards.put((int) wizard.getOwnerPlayerId(), type);
				assert lanesOfWizards.size() < 10;
			}
		}

	}

	private static void updateRuneState() {
		if (MyStrategy.world.getTickIndex() < MyStrategy.game.getBonusAppearanceIntervalTicks()) {
			isRuneGot.put(RuneType.TOP, true);
			isRuneGot.put(RuneType.BOT, true);
			return;
		}

		Bonus[] bonuses = MyStrategy.world.getBonuses();
		if (bonuses.length == 2) {
			isRuneGot.put(RuneType.TOP, false);
			isRuneGot.put(RuneType.BOT, false);
			return;
		}
		if (getTimeToNextRune() == 0) {
			isRuneGot.put(RuneType.TOP, false);
			isRuneGot.put(RuneType.BOT, false);
		}
		for (LivingUnit unit : MyStrategy.world.getMinions()) {
			if (unit.getFaction().equals(MyStrategy.self.getFaction())) {
				if (MyUtils.getDistance(unit, RuneType.TOP.waypoint.getX(),
						RuneType.TOP.waypoint.getY()) < MyStrategy.game.getMinionVisionRange())
					isRuneGot.put(RuneType.TOP, true);
				if (MyUtils.getDistance(unit, RuneType.BOT.waypoint.getX(),
						RuneType.BOT.waypoint.getY()) < MyStrategy.game.getMinionVisionRange())
					isRuneGot.put(RuneType.BOT, true);
			}
		}
		for (Wizard unit : MyStrategy.world.getWizards()) {
			if (unit.getFaction().equals(MyStrategy.self.getFaction())) {
				if (MyUtils.getDistance(unit, RuneType.TOP.waypoint.getX(), RuneType.TOP.waypoint.getY()) < unit
						.getVisionRange())
					isRuneGot.put(RuneType.TOP, true);
				if (MyUtils.getDistance(unit, RuneType.BOT.waypoint.getX(), RuneType.BOT.waypoint.getY()) < unit
						.getVisionRange())
					isRuneGot.put(RuneType.BOT, true);
			}
		}

		for (Bonus bonus : bonuses) {
			if (Math.abs(bonus.getX() - RuneType.BOT.waypoint.getX()) < MyMagicNumbers.DELTA
					&& Math.abs(bonus.getY() - RuneType.BOT.waypoint.getY()) < MyMagicNumbers.DELTA) {
				isRuneGot.put(RuneType.BOT, false);
			} else {
				assert Math.abs(bonus.getX() - RuneType.TOP.waypoint.getX()) < MyMagicNumbers.DELTA
						&& Math.abs(bonus.getY() - RuneType.TOP.waypoint.getY()) < MyMagicNumbers.DELTA;
				isRuneGot.put(RuneType.TOP, false);
			}
		}

		// TODO: look at enemy statuses
	}

	private static boolean isOnRiver(Unit wizard) {
		if (getLaneByPossition(wizard) != null)
			return false;
		return isOnExtraRiver(wizard);
	}

	private static boolean isOnExtraRiver(Unit wizard) {
		double x = wizard.getX();
		double y = wizard.getY();
		double mapSize = MyStrategy.game.getMapSize();
		double tileSize = mapSize / 10.0;
		double delta = MyMagicNumbers.DELTA_LINE_IN_FOREST;
		if (x - y > -tileSize - delta && x - y < tileSize + delta)
			return true;
		return false;
	}

	private static boolean isOnBase(Unit wizard) {
		double x = wizard.getX();
		double y = wizard.getY();
		double mapSize = MyStrategy.game.getMapSize();
		double tileSize = mapSize / 10.0;

		if (x < 2 * tileSize && y > mapSize - 2 * tileSize && x - y < -mapSize + 3 * tileSize)
			return true;
		return false;
	}

	private static boolean isOnEnemyBase(Unit wizard) {
		double mapSize = MyStrategy.game.getMapSize();
		double x = mapSize - wizard.getX();
		double y = mapSize - wizard.getY();
		double tileSize = mapSize / 10.0;

		if (x < 2 * tileSize && y > mapSize - 2 * tileSize && x - y < -mapSize + 3 * tileSize)
			return true;
		return false;
	}

	/**
	 * Decides go to rune
	 * 
	 * @param bestTarget
	 * 
	 * @return
	 */
	private static boolean needGoToRune(LivingUnit bestTarget) {
		if (MyUtils.getDistance(MyStrategy.self, MyStrategy.game.getMapSize(),
				0) < MyMagicNumbers.DISTANCE_CLOSE_TO_BASE)
			return false;

		if (!isTimeToGoToRune())
			return false;

		if (!canGoToRune())
			return false;

		return true;
	}

	/**
	 * check if path to rune is available
	 * 
	 * @return
	 */
	private static boolean canGoToRune() {
		if (MyStrategy.isFastPush != null && MyStrategy.isFastPush)
			return false;

		if (MyStrategy.self.getX() - MyStrategy.self.getY() > -500)
			return true;

		// TODO: improve
		return false;
	}

	/**
	 * Check time go to next rune or if rune now available
	 * 
	 * @return
	 */
	private static boolean isTimeToGoToRune() {

		Wizard self = MyStrategy.self;
		RuneType newRune = isTimeToGoToRune(self);
		if (newRune != null && MyStrategy.finalMode) {
			int estimateSelf = estimateTimeToGoToRune(newRune, self);
			for (Wizard wizard : MyStrategy.world.getWizards()) {
				if (wizard.isMe() || !wizard.getFaction().equals(self.getFaction()))
					continue;

				RuneType type = isTimeToGoToRune(wizard);
				if (newRune.equals(type)) {
					int esimateWizard = estimateTimeToGoToRune(newRune, wizard);
					if (esimateWizard < estimateSelf) {
						rune = null;
						return false;
					}
				}

			}
		}
		rune = newRune;
		return newRune != null;
	}

	private static RuneType isTimeToGoToRune(Wizard wizard) {
		LaneType lane = getLaneByPossition(wizard);

		if (lane == null) {
			if (isOnBase(wizard))
				return null;
			if (isOnRiver(wizard)) {
				if (wizard.getX() + wizard.getY() > MyStrategy.game.getMapSize()) {
					if (isTimeToGoRune(RuneType.BOT, wizard)) {
						return RuneType.BOT;
					}
				} else {
					if (isTimeToGoRune(RuneType.TOP, wizard)) {
						return RuneType.TOP;
					}
				}

			}
		} else if (lane.equals(LaneType.MIDDLE)) {
			if (isTimeToGoRune(RuneType.TOP, wizard) || isTimeToGoRune(RuneType.BOT, wizard)) {
				RuneType newRune = null;
				if (isTimeToGoRune(RuneType.TOP, wizard) && isTimeToGoRune(RuneType.BOT, wizard)) {
					if (isBetterToGoBotFromMid(wizard)) {
						newRune = RuneType.BOT;
					} else {
						newRune = RuneType.TOP;
					}
				} else if (isTimeToGoRune(RuneType.TOP, wizard)) {
					newRune = RuneType.TOP;
				} else {
					newRune = RuneType.BOT;
				}
				return newRune;
			}
		} else if (lane.equals(LaneType.BOTTOM)) {
			if (isTimeToGoRune(RuneType.BOT, wizard)) {
				return RuneType.BOT;
			}
		} else {
			if (isTimeToGoRune(RuneType.TOP, wizard)) {
				return RuneType.TOP;
			}
		}
		return null;
	}

	private static boolean isBetterToGoBotFromMid(Wizard wizard) {
		Set<LaneType> types = new LinkedHashSet<>();
		types.add(LaneType.MIDDLE);
		types.add(LaneType.TOP);
		types.add(LaneType.BOTTOM);
		LaneType bestLane = getBestLaneFromList(types);
		if (LaneType.TOP.equals(bestLane))
			return false;
		else if (LaneType.BOTTOM.equals(bestLane))
			return true;
		return wizard.getX() + wizard.getY() > MyStrategy.game.getMapSize();
	}

	private static boolean isTimeToGoRune(RuneType runeType, Wizard wizard) {
		if (!isRuneGot.get(runeType))
			return true;
		if (MyStrategy.world.getTickIndex() > MyStrategy.world.getTickCount()
				- MyStrategy.game.getBonusAppearanceIntervalTicks() + 1)
			return false;
		int timeToNextRune = getTimeToNextRune();
		if (timeToNextRune > MyMagicNumbers.MAX_TIME_TO_GO_TO_RUNE)
			return false;
		int estimate = estimateTimeToGoToRune(runeType, wizard);
		if (estimate + MyMagicNumbers.EXTRA_TIME_TO_GET_RUNE > timeToNextRune)
			return true;

		return false;
	}

	public static int getTimeToNextRune() {
		int interval = MyStrategy.game.getBonusAppearanceIntervalTicks();
		int timeToNextRune = interval - (MyStrategy.world.getTickIndex() - 2) % interval - 2;
		return timeToNextRune;
	}

	private static int estimateTimeToGoToRune(RuneType runeType, Wizard wizard) {
		MyDataPoint2D[] ways = getRuneWaypoints(runeType, wizard);

		assert ways.length > 0;
		if (ways.length == 1 && !isOnExtraRiver(wizard)) {
			assert false;
			return MyMagicNumbers.MAX_TIME_TO_GO_TO_RUNE;
		}

		double distance = 0;
		MyDataPoint2D prev = new MyDataPoint2D(wizard.getX(), wizard.getY());
		for (MyDataPoint2D dataPoint2D : ways) {
			distance += MyUtils.getDistance(prev.getX(), prev.getY(), dataPoint2D.getX(), dataPoint2D.getY());
			prev = dataPoint2D;
		}

		return timeToGoDistance(distance, wizard);
	}

	private static MyDataPoint2D[] getRuneWaypoints(RuneType runeType, Wizard wizard) {
		LaneType laneType = getLaneByPossition(wizard);
		MyDataPoint2D[] ways;
		if (isOnExtraRiver(wizard) || laneType == null) {
			ways = new MyDataPoint2D[] { runeType.waypoint };
		} else {
			MyDataPoint2D bestWayPoint = null;
			double minDistance = 0;
			for (PointForRune pointForRune : pointsForRune.get(runeType)) {
				if (pointForRune.laneType.equals(laneType)) {
					double distanceToPoint = MyUtils.getDistance(wizard, pointForRune.point.getX(),
							pointForRune.point.getY());
					if (bestWayPoint == null || distanceToPoint < minDistance) {
						bestWayPoint = pointForRune.point;
						minDistance = distanceToPoint;
					}
				}
			}

			if (bestWayPoint == null) {
				assert false;
				ways = new MyDataPoint2D[] { runeType.waypoint };
			}

			ways = new MyDataPoint2D[] { bestWayPoint, runeType.waypoint };
		}
		return ways;
	}

	private static int timeToGoDistance(double distance, Wizard wizard) {
		double selfSpeedFactor = MyUtils.getSelfMultiplySpeedFactor(wizard);
		double maxSpeed = MyStrategy.game.getWizardForwardSpeed();

		for (Status status : wizard.getStatuses()) {
			if (status.getType().equals(StatusType.HASTENED)) {
				double hastedFactor = selfSpeedFactor + MyStrategy.game.getHastenedMovementBonusFactor();
				double distanceUnderHaste = (double) (status.getRemainingDurationTicks()) * maxSpeed * hastedFactor;
				if (distanceUnderHaste > distance) {
					return (int) (distance / (maxSpeed * hastedFactor));
				} else {
					return status.getRemainingDurationTicks()
							+ (int) ((distance - distanceUnderHaste) / (maxSpeed * selfSpeedFactor));
				}
			}
		}
		return (int) (distance / (maxSpeed * selfSpeedFactor));
	}

	public static enum RuneType {
		TOP(1200, 1200), BOT(2800, 2800);

		final MyDataPoint2D waypoint;

		private RuneType(double x, double y) {
			waypoint = new MyDataPoint2D(x, y);
		}

	}

	private static class PointForRune {
		final LaneType laneType;
		@SuppressWarnings("unused")
		final boolean isForward;
		final MyDataPoint2D point;

		public PointForRune(LaneType laneType, boolean isForward, MyDataPoint2D point) {
			super();
			this.laneType = laneType;
			this.isForward = isForward;
			this.point = point;
		}

	}

	private static class LaneInfo {
		final LaneType type;
		int teammates;
		int enemy;

		// TODO: position, death of teammates
		public LaneInfo(LaneType type, int teammates, int enemy) {
			super();
			this.type = type;
			this.teammates = teammates;
			this.enemy = enemy;
		}

	}

	public static Boolean isFastPushMod() {
		LaneType laneSelf = lane;
		Boolean selfPush = isLaneForPush(laneSelf);
		if (selfPush == null || !selfPush)
			return selfPush;

		Wizard self = MyStrategy.self;
		for (Wizard wizard : MyStrategy.world.getWizards()) {
			if (wizard.isMe() || !wizard.getFaction().equals(self.getFaction()))
				continue;

			if (!lanesOfWizards.containsKey((int) wizard.getId()))
				continue;// FIXME

			Boolean isWizardPush = isLaneForPush(lanesOfWizards.get((int) wizard.getId()));
			assert isWizardPush != null;
			if (isWizardPush == null || !isWizardPush)
				continue;
			if (wizard.getSkills().length > 0)
				return false;
			if (wizard.getXp() > self.getXp())
				return false;

			if (wizard.getXp() == self.getXp()) {
				if (wizard.getId() < self.getId())
					return false;
			}
		}
		return true;

	}

	private static Boolean isLaneForPush(LaneType laneSelf) {
		if (laneSelf == null)
			return null;
		int start = 1;
		if (MyStrategy.self.getId() <= 5)
			start = 6;
		for (int i = start; i < start + 5; i++) {
			if (!lanesOfWizards.containsKey(i))
				return null;
			if (lanesOfWizards.get(i).equals(laneSelf))
				return false;
		}
		return true;
	}

	public static boolean makeMoveToBaseDeff(boolean fightMove) {
		Wizard self = MyStrategy.self;
		if (!isOnBase(self)) {
			LaneType laneByPossition = getLaneByPossition(self);
			if (laneByPossition == null)
				return false;

			if (getPositionOnLane(self, laneByPossition, true) < MyMagicNumbers.LANE_POSITION_TO_SAFE_MOVE)
				return false;
		}
		LivingUnit bestTarget = null;
		for (Wizard wizard : MyStrategy.world.getWizards()) {
			if (!MyUtils.isUnitEnemy(wizard))
				continue;

			if (isOnBase(wizard))
				bestTarget = wizard;
		}

		if (bestTarget == null) {
			boolean isNeedDeff = false;
			for (Minion minion : MyStrategy.world.getMinions()) {
				if (!MyUtils.isUnitEnemy(minion) || minion.getFaction().equals(Faction.NEUTRAL))
					continue;

				if (!isOnBase(minion))
					continue;
				if (bestTarget == null)
					bestTarget = minion;
				else {
					isNeedDeff = true;
					double dis1 = MyUtils.getDistance(bestTarget, 400, 3600);
					double dis2 = MyUtils.getDistance(minion, 400, 3600);
					if (dis2 < dis1)
						bestTarget = minion;
				}
			}
			if (!isNeedDeff)
				return false;
		}

		MyLocalMovingStrategy.tryMoveToAngle(fightMove, MyStrategy.self.getAngleTo(bestTarget), MyLocalMoveType.SAFE,
				MyMagicNumbers.DISTANCE_FOR_SAFE_MOVE);

		return true;
	}

}
