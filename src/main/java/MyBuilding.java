import model.Building;
import model.BuildingType;
import model.Faction;
import model.Status;

public class MyBuilding extends Building {

	public MyBuilding(long id, double x, double y, double speedX, double speedY, double angle, Faction faction,
			double radius, int life, int maxLife, Status[] statuses, BuildingType type, double visionRange,
			double attackRange, int damage, int cooldownTicks, int remainingActionCooldownTicks) {
		super(id, x, y, speedX, speedY, angle, faction, radius, life, maxLife, statuses, type, visionRange, attackRange,
				damage, cooldownTicks, remainingActionCooldownTicks);
		this.remainingActionCooldownTicks = remainingActionCooldownTicks;
	}

	public MyBuilding(Building building) {
		super(building.getId(), building.getX(), building.getY(), building.getSpeedX(), building.getSpeedY(),
				building.getAngle(), building.getFaction(), building.getRadius(), building.getLife(),
				building.getMaxLife(), building.getStatuses(), building.getType(), building.getVisionRange(),
				building.getAttackRange(), building.getDamage(), building.getCooldownTicks(),
				building.getRemainingActionCooldownTicks());
		this.remainingActionCooldownTicks = building.getRemainingActionCooldownTicks();
	}

	private int remainingActionCooldownTicks;

	@Override
	public int getRemainingActionCooldownTicks() {
		return remainingActionCooldownTicks;
	}

	public void decrementRemainingActionCooldownTicks() {
		if (remainingActionCooldownTicks > 0)
			remainingActionCooldownTicks--;
	}

	@Override
	public String toString() {
		return "MyBuilding [remainingActionCooldownTicks=" + remainingActionCooldownTicks + ", getType()=" + getType()
				+ ", getAttackRange()=" + getAttackRange() + ", getDamage()=" + getDamage() + ", getLife()=" + getLife()
				+ ", getRadius()=" + getRadius() + ", getId()=" + getId() + ", getX()=" + getX() + ", getY()=" + getY()
				+ ", getFaction()=" + getFaction() + "]";
	}

}
