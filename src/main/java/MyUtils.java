import model.Building;
import model.BuildingType;
import model.Faction;
import model.LaneType;
import model.LivingUnit;
import model.Minion;
import model.Player;
import model.ProjectileType;
import model.SkillType;
import model.Status;
import model.StatusType;
import model.Unit;
import model.Wizard;

public class MyUtils {
	public static double normalizeAngle(double angle) {
		double result = angle;
		if (angle > Math.PI)
			result = angle - 2 * Math.PI;
		else if (angle < -Math.PI)
			result = angle + 2 * Math.PI;
		assert result >= -Math.PI && result <= Math.PI : result;
		return result;
	}

	public static double getDistance(Unit from, Unit to) {
		if (from.equals(to))
			return 0;
		return Math.sqrt((from.getX() - to.getX()) * (from.getX() - to.getX())
				+ (from.getY() - to.getY()) * (from.getY() - to.getY()));
	}

	public static double getDistance(Unit from, double x, double y) {
		double y2 = from.getY();
		double x2 = from.getX();
		return getDistance(x, y, x2, y2);
	}

	public static double getDistance(double x, double y, double x2, double y2) {
		return Math.sqrt((x2 - x) * (x2 - x) + (y2 - y) * (y2 - y));
	}

	public static boolean isUnitEnemy(LivingUnit livingUnit) {
		Faction faction = MyStrategy.self.getFaction();
		return isUnitEnemyTo(livingUnit, faction);
	}

	public static boolean isUnitEnemyTo(LivingUnit livingUnit, Faction faction) {
		if (livingUnit.getFaction().equals(faction))
			return false;
		if (livingUnit.getFaction().equals(Faction.NEUTRAL)) {
			if (livingUnit instanceof Minion) {
				Minion minion = (Minion) livingUnit;

				if (Math.abs(minion.getSpeedX()) < MyMagicNumbers.DELTA
						&& Math.abs(minion.getSpeedY()) < MyMagicNumbers.DELTA
						&& minion.getRemainingActionCooldownTicks() == 0 && minion.getLife() == minion.getMaxLife())
					return false;// TODO: check angle speed

			} else {
				assert false;
			}
		}
		return true;
	}

	public static Faction getOppositeFaction(Faction faction) {
		assert (faction.equals(Faction.ACADEMY) || faction.equals(Faction.RENEGADES));
		if (faction.equals(Faction.ACADEMY))
			return Faction.RENEGADES;
		else
			return Faction.ACADEMY;
	}

	public static double getProjetileSpeed(ProjectileType type) {
		switch (type) {
		case DART:
			return MyStrategy.game.getDartSpeed();
		case FIREBALL:
			return MyStrategy.game.getFireballSpeed();
		case FROST_BOLT:
			return MyStrategy.game.getFrostBoltSpeed();
		default:
			assert false;
		case MAGIC_MISSILE:
			return MyStrategy.game.getMagicMissileSpeed();
		}
	}

	public static double getProjectileRadius(ProjectileType type) {
		switch (type) {
		case DART:
			return MyStrategy.game.getDartRadius();
		case FIREBALL:
			return MyStrategy.game.getFireballExplosionMinDamageRange();
		case FROST_BOLT:
			return MyStrategy.game.getFrostBoltRadius();
		default:
			assert false;
		case MAGIC_MISSILE:
			return MyStrategy.game.getMagicMissileRadius();
		}
	}

	public static int getProjetileDamage(ProjectileType type) {
		switch (type) {
		case DART:
			return MyStrategy.game.getDartDirectDamage();
		case FIREBALL:
			return MyStrategy.game.getFireballExplosionMaxDamage();
		case FROST_BOLT:
			return MyStrategy.game.getFrostBoltDirectDamage();
		default:
			assert false;
		case MAGIC_MISSILE:
			return MyStrategy.game.getMagicMissileDirectDamage();
		}
	}

	public static double getMaxWizardSpeed(Wizard wizard) {
		return MyStrategy.game.getWizardForwardSpeed() * getMultiplySpeedFactor(wizard);
	}

	public static double getMinWizardSpeed(Wizard wizard) {
		return MyStrategy.game.getWizardBackwardSpeed() * getMultiplySpeedFactor(wizard);
	}

	public static double getMultiplySpeedFactor(Wizard wizard) {

		if (isPlayerCrashed(wizard))
			return 0;

		boolean passive1 = false;
		boolean passive2 = false;
		boolean aura1 = false;
		boolean aura2 = false;
		boolean hasted = false;

		for (SkillType skillType : wizard.getSkills()) {
			if (skillType.equals(SkillType.MOVEMENT_BONUS_FACTOR_PASSIVE_1))
				passive1 = true;
			if (skillType.equals(SkillType.MOVEMENT_BONUS_FACTOR_PASSIVE_2))
				passive2 = true;
			if (skillType.equals(SkillType.MOVEMENT_BONUS_FACTOR_AURA_1))
				aura1 = true;
			if (skillType.equals(SkillType.MOVEMENT_BONUS_FACTOR_AURA_2))
				aura2 = true;
		}

		for (Status status : wizard.getStatuses()) {
			if (status.getType().equals(StatusType.HASTENED))
				hasted = true;
			if (status.getType().equals(StatusType.FROZEN))
				return 0.0;
		}

		if (!aura1 || !aura2) {
			for (Wizard other : MyStrategy.world.getWizards()) {
				if (!other.getFaction().equals(wizard.getFaction()))
					continue;
				double distance = getDistance(wizard, other);

				if (distance < MyStrategy.game.getWizardRadius())
					continue;
				if (distance > MyStrategy.game.getAuraSkillRange())
					continue;
				for (SkillType skillType : other.getSkills()) {
					if (skillType.equals(SkillType.MOVEMENT_BONUS_FACTOR_AURA_1))
						aura1 = true;
					if (skillType.equals(SkillType.MOVEMENT_BONUS_FACTOR_AURA_2))
						aura2 = true;
				}
			}
		}

		double result = 1.0;
		if (passive1)
			result += MyStrategy.game.getMovementBonusFactorPerSkillLevel();
		if (passive2)
			result += MyStrategy.game.getMovementBonusFactorPerSkillLevel();
		if (aura1)
			result += MyStrategy.game.getMovementBonusFactorPerSkillLevel();
		if (aura2)
			result += MyStrategy.game.getMovementBonusFactorPerSkillLevel();
		if (hasted)
			result += MyStrategy.game.getHastenedMovementBonusFactor();

		return result;
	}

	public static boolean isPlayerCrashed(Wizard wizard) {
		for (Player player : MyStrategy.world.getPlayers()) {
			if (player.getId() == wizard.getOwnerPlayerId() && player.isStrategyCrashed())
				return true;
		}
		return false;
	}

	public static double getSelfMultiplySpeedFactor(Wizard wizard) {
		double result = 1.0;
		for (SkillType skillType : wizard.getSkills()) {
			switch (skillType) {
			case MOVEMENT_BONUS_FACTOR_AURA_1:
			case MOVEMENT_BONUS_FACTOR_AURA_2:
			case MOVEMENT_BONUS_FACTOR_PASSIVE_1:
			case MOVEMENT_BONUS_FACTOR_PASSIVE_2:
				result += MyStrategy.game.getMovementBonusFactorPerSkillLevel();
				break;
			default:
				break;
			}
		}
		return result;
	}

	public static boolean isFirstOnLane(Building target) {
		assert !target.getFaction().equals(MyStrategy.self.getFaction());
		if (target.getType().equals(BuildingType.FACTION_BASE)) {
			int remaindTowers2 = 0;
			for (Building building : MyStrategy.world.getBuildingsSet()) {
				if (!isUnitEnemy(building))
					continue;
				if (target.getType().equals(BuildingType.FACTION_BASE))
					continue;
				if (!MyUtils.isFirstTower(building))
					remaindTowers2++;
			}
			if (remaindTowers2 == 3)
				return false;
		} else {
			if (MyUtils.isFirstTower(target))
				return true;
			LaneType lane = MyGlobalMoveStrategy.getLaneByPossition(target);
			assert lane != null;
			for (Building building : MyStrategy.world.getBuildingsSet()) {
				if (!isUnitEnemy(building))
					continue;
				if (lane.equals(MyGlobalMoveStrategy.getLaneByPossition(building)) && MyUtils.isFirstTower(building))
					return false;
			}
		}

		return true;
	}

	private static boolean isFirstTower(Building target) {
		double x;
		double y;
		if (target.getFaction().equals(MyStrategy.self.getFaction())) {
			x = target.getX();
			y = MyStrategy.game.getMapSize() - target.getY();
		} else {
			x = MyStrategy.game.getMapSize() - target.getX();
			y = target.getY();
		}
		double distance = Math.sqrt(x * x + y * y);

		return distance > 1950.0;// FIXME
	}

	public static double getAngleFromTriangle(double opposite, double adjacent1, double adjacent2) {
		double cosA = (adjacent2 * adjacent2 + adjacent1 * adjacent1 - opposite * opposite)
				/ (2 * adjacent2 * adjacent1);
		assert Math.abs(cosA) <= 1.0 : adjacent2 + " : " + adjacent1 + " : " + opposite;
		if (cosA > 1.0)
			cosA = 1.0;
		else if (cosA < -1.0)
			cosA = -1.0;
		double diffAngle = Math.acos(cosA);
		return diffAngle;
	}

}
