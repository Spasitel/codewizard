import model.Game;

public class MyMagicNumbers {

	public static final int STRAFE_CREEP_DISTANCE_PREVENTED_CHASE = 350;
	public static final double DANGER_LEVEL_TO_CONSIDER = 0.3;
	public static final double DANGER_LEVEL_TO_PANIC = 0.7;
	public static final int TICKS_FOR_TOWER_EASY_ESCAPE = 10;
	public static final double LIFE_LEVEL_FOR_BUILDING_ATTACK = 0.36;
	public static final double LIFE_LEVEL_FOR_WIZARD_ATTACK = 0.5;
	public static final double DELTA_LINE_IN_FOREST = 50.0;
	public static final double WAYPOINT_RADIUS = 100.0D;
	public static final double DISTANCE_FOR_SAFE_MOVE = 400.0;
	public static final double DISTANCE_FOR_WAYPIONT_MOVE = 600.0;
	public static final double DELTA = Math.pow(10, -7);
	public static final int MAX_TIME_TO_GO_TO_RUNE = 1000;
	public static final int EXTRA_TIME_TO_GET_RUNE = 50;
	public static final int DELAY_TO_CHANGE_WAYPOINTS = 5;
	public static final double HP_LEVEL_FOR_IGNORE_FETISH = 0.8;
	public static final double HP_LEVEL_FOR_IGNORE_FETISH_FASTPUSH = 0.6;
	public static final double DELTA_WAYPOINTS_MIDDLE = 60;
	public static final double DISTANCE_CLOSE_TO_BASE = 1850;
	public static final double DISTANCE_TO_CHANGE_LANE_ON_RUNE = 400.0;
	public static final double DISTANCE_CLOSE_TO_ENEMY = 100.0;
	public static final double LANE_POSITION_TO_SAFE_MOVE = 0.65;

	public static double WIZARD_MAX_CAST_RANGE = 0;

	private static boolean init = false;

	public static void init(Game game) {
		if (!init) {
			WIZARD_MAX_CAST_RANGE = MyStrategy.game.getWizardCastRange() + 100.0;

			init = true;
		}
	}

}
